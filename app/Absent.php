<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Absent extends Model
{
    protected $table = 'absents';
    protected $fillable = ['student_id','teacher_id','employee_id','status','type','date','hours_home','hours_come','for_who','status_come'];

    public function student()
    {
        return $this->hasOne('App\Student','id','student_id');
    }

    public function teacher()
    {
        return $this->hasOne('App\Teacher','id','teacher_id');
    }

    public function employee()
    {
        return $this->hasOne('App\Employee','id','employee_id');
    }
}
