<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'articles';
    protected $fillable = ['id','title','content','image','category_content_id'];

    public function category()
    {
    	return $this->hasOne('App\categoryType','id','category_content_id');
    }
}
