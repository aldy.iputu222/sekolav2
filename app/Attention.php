<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Teacher;
use App\User;
class Attention extends Model
{
    protected $fillable = [
        'id',
        'title',
        'content',
        'important',
        'from_id',
        'from'
    ];

    public function from_attention()
    {
        if($this->from == "admin")
        {
            return $this->hasOne('App\User','id','from_id');
        }else{
            return $this->hasOne('App\Teacher','id','from_id');
        }
    }

    public function teacher()
    {
        return $this->hasOne('App\Teacher','id','from_id');
    }

    public function attention_filter()
    {
        return $this->hasMany('App\AttentionFilter');
    }
}
