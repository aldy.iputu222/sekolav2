<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ClassGroup;
class AttentionFilter extends Model
{
    protected $fillable = [
        'attention_id',
        'class_group_id',
        'for_who',
        'type',
    ];

    public function attention()
    {
        return $this->belongsTo('App\Attention','attention_id','id')->with('teacher');
    }

    public function classgroup()
    {
        return $this->belongsTo('App\ClassGroup','class_group_id','id');
    }
}
