<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attitude extends Model
{
    protected $guarded =[];
    protected $fillable = ['name','type','code','point'];

}
