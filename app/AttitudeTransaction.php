<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttitudeTransaction extends Model
{
    protected $guarded = [];

    public function student(){
    	return $this->hasOne('App\Student','id','user_id');
    }

    public function attitude(){
    	return $this->hasOne('App\Attitude','id','attitude_id');
    }

    public function teacher(){
    	return $this->hasOne('App\Teacher','id','teacher_id');
    }
}
