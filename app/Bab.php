<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bab extends Model
{
    protected $table = 'babs';
    protected $fillable = ['subject_id','bab'];
    
}
