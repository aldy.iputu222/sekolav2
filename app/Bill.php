<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bill extends Model
{
    protected $fillable = ['name', 'content', 'total'];

    public function bill_filters()
    {
        return $this->hasMany('App\BillFilter','bill_id','id');
    }
}
