<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillFilter extends Model
{
    protected $fillable = [
        'bill_id',
        'class_group_id',
    ];

    public function bill()
    {
        return $this->hasOne('App\Bill','id','bill_id');
    }

    public function class_group()
    {
        return $this->hasOne('App\ClassGroup','id','class_group_id');
    }
}
