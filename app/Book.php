<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = [
        'title',
        'description',
        'author_id',
        'publisher_id',
        'publication_year',
        'pages',
        'book_category_id',
        'cover_book'
    ];

    public function book_copy()
    {
        return $this->hasMany('App\BookSingle','book_id','id');
    }

    public function book_was_borrowed()
    {
        return $this->hasMany('App\BookSingle','book_id','id')->where('status','available');
    }

    public function author()
    {
        return $this->hasOne('App\Author','id','author_id');
    }
    public function bookCategory()
    {
        return $this->hasOne('App\BookCategory','id','book_category_id');
    }

    public function publisher()
    {
        return $this->hasOne('App\Publisher','id','publisher_id');
    }
}
