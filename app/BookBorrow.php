<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookBorrow extends Model
{
    protected $fillable = [
        'user_id',
        'student_id',
        'status',
        'borrow_type_id',
        'borrow_at',
        'returned_at',
        'late',
    ];

    public function student()
    {
        return $this->hasOne('App\Student','id','student_id');
    }

    public function user()
    {
        return $this->hasOne('App\User','id','user_id');
    }

    public function borrow_type()
    {
        return $this->hasOne('App\BorrowType','id','borrow_type_id');
    }
}
