<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookSingle extends Model
{
    protected $fillable = [
        'book_id',
        'qrcode',
        'barcode',
        'status',
        'date_in'
    ];

    public function book()
    {
        return $this->hasOne('App\Book','id','book_id');
    }
}
