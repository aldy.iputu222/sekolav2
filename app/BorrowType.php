<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BorrowType extends Model
{
    protected $fillable = [
        'name',
        'number_date',
        'expired_price'
    ];
}
