<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassGroup extends Model
{
    protected $fillable = [
        'name',
        'grade',
        'major_id',
        'school_year_id',
        'status'
    ];
    
    public function school_year()
    {
        return $this->hasOne('App\SchoolYear','id','school_year_id');
    }

    public function students()
    {
        return $this->hasMany('App\Student','class_group_id','id');
    }
}
