<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Student;
use App\Absent;
class AbsentCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'absent:checking';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mengecek Absensi Hari ini';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
    }
}
