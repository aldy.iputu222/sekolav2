<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = ['name','content','type','file','status','teacher_id','matpel_id'];

    public function teacher()
    {
    	return $this->hasOne('App\Teacher','id','teacher_id');
    }

    public function subject()
    {
    	return $this->hasOne('App\Subject','id','matpel_id');
    }
}
