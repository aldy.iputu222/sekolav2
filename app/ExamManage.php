<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamManage extends Model
{
    protected $table = 'exam_manages';
    protected $fillable = [
        'exam_type_id',
        'subject_id',
        'major_id',
        'grade',
        'kkm',
        'time',
        'description',
        'created_by',
        'token'
    ];

    public function exam_type()
    {
        return $this->hasOne('App\ExamType','id','exam_type_id');
    }

    public function subject()
    {
        return $this->hasOne('App\Subject','id','subject_id');
    }

    public function major()
    {
        return $this->hasOne('App\Major','id','major_id');
    }

    public function bab(){
        return $this->hasOne('App\Bab','id','bab_id');
    }

    
}
