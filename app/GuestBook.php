<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GuestBook extends Model
{
    protected $fillable = [
        'name',
        'address',
        'requirement',
        'visit_as',
        'phone'
    ];
}
