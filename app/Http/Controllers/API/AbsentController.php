<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Major;
use App\ClassGroup;
use App\Student;
use App\Absent;
use App\Notification;
use App\ParentRelate;
use App\SchoolYear;
use Carbon\Carbon;
class AbsentController extends Controller
{
   
    public function store(Request $request)
    {
        $qrcode = explode('-',$request->id);
        if($qrcode[0] == "student"){
            if(Student::find($qrcode[1])->count() > 0){
                $absent = Absent::where('student_id',$qrcode[1])
                ->whereRaw("DATE(created_at) = CURDATE()")
                ->where('type','Machine');

                if($absent->count() > 0)
                {
                    if($absent->first()->hours_home == null)
                    {
                        if(date("H") >= 15)
                        {
                            $absent->update([
                                'hours_home'=>date("H:i"),
                            ]);

                            $notification = new Notification();
                            foreach(ParentRelate::where('student_id',$qrcode[1])->get() as $parent_relate)
                            {
                                $notification->send("parentid".$parent_relate->parent_id,$parent_relate->student->name." Sudah Pulang ","Pukul : ".date("H:i"));        
                            }

                            $response = [
                                'status' =>'success',
                                'message'=>'was-scanned',
                                'data'   =>Student::where('id',$qrcode[1])->first(),
                            ];
                            
                        }else{
                            $response = [
                                'status'=>'fail',
                                'message'=>'too-early',
                            ];
                        }
                    }else{
                        $response = [
                            'status'=>'fail',
                            'message'=>'was-scanned',
                        ];
                    }
                }else{
                    if($absent->count() <= 0)
                    {
                        if((date("H") <= 6) && (date("i") <= 50))
                        {
                            Absent::create([
                                'for_who'=>'student',
                                'student_id' => $qrcode[1],
                                'status' => "H",
                                'type' => "Machine",
                                'date' => date("Y-m-d"),
                                'hours_come' => date("H:i"),
                                'status_come'=>'on_time'
                            ]);
                        }else{
                            Absent::create([
                                'for_who'=>'student',
                                'student_id' => $qrcode[1],
                                'status' => "H",
                                'type' => "Machine",
                                'date' => date("Y-m-d"),
                                'hours_come' => date("H:i"),
                                'status_come'=>'late'
                            ]);
                        }

                        $notification = new Notification();
                        foreach(ParentRelate::where('student_id',$qrcode[1])->get() as $parent_relate)
                        {
                            $notification->send("parentid".$parent_relate->parent_id,$parent_relate->student->name." Sudah Sampai ","Pukul : ".date("H:i"));        
                        }

                        $response = [
                            'status' =>'success',
                            'message'=>'was-scanned',
                            'data'   =>Student::where('id',$qrcode[1])->first(),
                        ];
                    }else{
                        $response = [
                            'status'=>'fail',
                            'message'=>'was-scanned',
                        ];
                    }
                }

            }else{
                $response = [
                    'status'=>'fail',
                    'message'=>'student-not-found',
                ];
            }
        }else{
            $response = [
                'status'=>'fail',
                'message'=>'qrcode-not-valid',
            ];
        }
        
        return response()->json($response,200);
    }


    public function major_class($id_year,$id_class)
    {
         $kelas = ClassGroup::where('major_id',$id_class)
        ->where('school_year_id',$id_year)->where('status','active')->get();

        return response()->json(['code'=>'200','kelas'=>$kelas]);
    }

    public function student_absent($student_id)
    {
        $student = Student::find($student_id);

        $school_year = $student->school_year;
        
        $array = [];

        $array_month = [
            '06','07','08','09','10','11','12',
            '01','02','03','04','05','06','07','08','09','10','11','12',
            '01','02','03','04','05','06',
        ];

        for($i = $school_year->first_year;$i<= $school_year->last_year;$i++)
        {
            foreach($array_month as $number_month)
            {
                if($number_month == "12")
                {
                    $array[] = [
                        'month_number'=>$number_month,
                        'month'=>$this->translate_month($number_month),
                        'year'=>$i,
                    ];
                    $i++;
                }else{
                    $array[] = [
                        'month_number'=>$number_month,
                        'month'=>$this->translate_month($number_month),
                        'year'=>$i,
                    ];
                }
                
            }
        }

        return response()->json([
            'today'=>Absent::where('student_id',$student->id)->whereRaw("DATE(date) = CURDATE()")->first(),
            'month'=>$this->translate_month(date("m")),
            'sick_count'=>Absent::where('student_id',$student->id)->where('status',"S")->count(),
            'permit_count'=>Absent::where('student_id',$student->id)->where('status',"I")->count(),
            'alfa_count'=>Absent::where('student_id',$student->id)->where('status',"A")->count(),
            'months_in_year'=>$array,
        ]);
    }

    public function absent_month(Request $request)
    {
        $data = [];
        if($request->as_who == "student")
        {
            $data =  Absent::where('student_id',$request->student_id)
            ->whereRaw('YEAR(date) = '.$request->year)
            ->whereRaw('MONTH(date) = '.$request->month)
            ->latest()->get();
        }

        return response()->json([
            'data'=>$data,
        ],200);
    }

    public function translate_month($param)
    {
        switch ($param) {
            case '01':
                return "Januari";
                break;
            case '02':
                return "Februari";
                break;
            case '03':
                return "Maret";
                break;
            case '04':
                return "April";
                break;
            case '05':
                return "Mei";
                break;
            case '06':
                return "Juni";
                break;
            case '07':
                return "Juli";
                break;
            case '08':
                return "Agustus";
                break;
            case '09':
                return "September";
                break;
            case '10':
                return "Oktober";
                break;
            case '11':
                return "November";
                break;
            case '12':
                return "Desember";
                break;
        }
    }

    public function test()
    {
        $years = SchoolYear::where('status','active')->get();
        foreach($years as $year)
        {
            foreach($year->classgroup as $classgroup)
            {
                foreach($classgroup->students as $student)
                {
                    if($student->absent_today == null)
                    {
                        Absent::create([
                            'for_who'=>'student',
                            'student_id' => $student->id,
                            'status' => "A",
                            'type' => "Machine",
                            'date' => date("Y-m-d"),
                        ]);
                    }
                }
            }
        }
    }
}
