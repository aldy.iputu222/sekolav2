<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Article;
class ArticleController extends Controller
{
    public function index()
    {
        return response()->json(
            Article::latest()->with('category')->paginate(7)
        );
    }
}
