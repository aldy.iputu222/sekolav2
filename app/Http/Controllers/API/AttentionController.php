<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Student;
use App\AttentionFilter;
use App\StudentParent;
class AttentionController extends Controller
{
    public function attention_data($for,$id)
    {
        if($for == "student" || $for == "parent")
        {
            if($for == "student")
            {
                $student   =  Student::where('id',$id)->first();
                $attention = AttentionFilter::orWhere(function($query) use ($student){
                    $query->where('class_group_id',$student->class_group_id)
                    ->where('for_who','student');
                })
                ->orWhere(function($query){
                    $query->where('type','all')
                    ->where('for_who','student');
                })
                ->with('attention')->latest()->paginate();
            }else{
                $students = StudentParent::find($id)->parent_relate;
                $classgroup = [];

                foreach($students as $student)
                {
                    $classgroup[] = $student->student->class_group_id;
                }

                $attention = AttentionFilter::orWhere(function($query) use ($classgroup){
                    $query->whereIn('class_group_id',$classgroup)
                    ->where('for_who','parent');
                })
                ->orWhere(function($query){
                    $query->where('type','all')
                    ->where('for_who','parent');
                })
                ->with('attention')->latest()->paginate();

            }
        }
        else{
            $attention = ['data'=>''];    
        }

        return response()->json(
            $attention 
        ,200);
    }

    public function atteion_parent(Request $request)
    {
        
    }
}
