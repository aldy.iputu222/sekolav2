<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AttitudeTransaction;
class AttitudeController extends Controller
{
    public function student($student_id)
    {
        $reward = 0;
        $punishment = 0;
        foreach(AttitudeTransaction::where('user_id',$student_id)->get() as $field)
        {
            if($field->attitude->type == "punishment")
            {
                $punishment += $field->attitude->point;
            }else{
                $reward += $field->attitude->point;
            }
        }

        return response()->json(
            [
                'reward_count'=>$reward,
                'punishment_count'=>$punishment,
                'data'=>AttitudeTransaction::where('user_id',$student_id)
                ->with('attitude')
                ->with('student')
                ->with('teacher')
                ->latest()->paginate(),
            ]
        );
    }
}
