<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Bill;
use App\BillFilter;
use App\Student;
use Log;
use App\Payment;
use App\ParentRelate;
use App\Notification;

use Veritrans_Config;
use Veritrans_Snap;
use Veritrans_Notification;
class BillController extends Controller
{
   protected $request;
   private $notification;


   public function __construct(Request $request)
   {
        $this->request = $request;
        Veritrans_Config::$serverKey = config('services.midtrans.serverKey');
        Veritrans_Config::$isProduction = config('services.midtrans.isProduction');
        Veritrans_Config::$isSanitized = config('services.midtrans.isSanitized');
        Veritrans_Config::$is3ds = config('services.midtrans.is3ds');
        $this->notification = new Notification();
    }

    public function student_bill($id)
    {
        $student = Student::find($id);
        $data    = BillFilter::where('class_group_id',$student->class_group_id)
        ->with('bill')->latest()->get();

        $array = [];

        foreach($data as $single)
        {
          $single['payment_status'] = Payment::where('student_id',$id)->where('bill_id',$single->bill_id)->where('status','settlement')->count();
          $array['data'][] = $single;
        }

        return $array;

        return response()->json($array,200);
    }

    public function charge(Request $request)
    {
        $server_key = 'SB-Mid-server-U322waITZmI1HW-TdTWLZ0fg';
        $is_production = false;

        $api_url = $is_production ? 
        'https://app.midtrans.com/snap/v1/transactions' : 
        'https://app.sandbox.midtrans.com/snap/v1/transactions';


        
        if( !strpos($_SERVER['REQUEST_URI'], '/charge') ) {
        http_response_code(404); 
        echo "wrong path, make sure it's `/charge`"; exit();
        }
        
        if( $_SERVER['REQUEST_METHOD'] !== 'POST'){
        http_response_code(404);
        echo "Page not found or wrong HTTP request method is used"; exit();
        }
        
        $request_body = file_get_contents('php://input');

        // return Log::info(Student::find($request->customer_details['billing_address']['phone']));

        Payment::create([
            'bill_id'=>$request->item_details[0]['id'],
            'student_parent_id'=>$request->user_id,
            'student_id'=>$request->customer_details['billing_address']['phone'],
            'class_group_id'=>Student::find($request->customer_details['billing_address']['phone'])->class_group_id,
            'order_id'=>$request->transaction_details['order_id'],
            'status'=>'pending',
        ]);

        header('Content-Type: application/json');
        $charge_result = $this->chargeAPI($api_url, $server_key, $request_body);
        http_response_code($charge_result['http_code']);
        echo $charge_result['body'];
    }

    function chargeAPI($api_url, $server_key, $request_body){
        $ch = curl_init();
        $curl_options = array(
            CURLOPT_URL => $api_url,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'Accept: application/json',
            'Authorization: Basic ' . base64_encode($server_key . ':')
            ),
            CURLOPT_POSTFIELDS => $request_body
        );
        curl_setopt_array($ch, $curl_options);
        $result = array(
            'body' => curl_exec($ch),
            'http_code' => curl_getinfo($ch, CURLINFO_HTTP_CODE),
        );
        return $result;
    }
    
    public function charge_notification(Request $request)
    {
        // $notif = new Veritrans_Notification();
        $notif = $request;
        $transaction = $notif->transaction_status;
        $type        = $notif->payment_type;
        $orderId     = $notif->order_id;
        $fraud       = $notif->fraud_status;
        $payment     = Payment::where('order_id',$orderId)->first();

        // return $payment->bill;
 
          if ($transaction == 'capture') {
 
            if ($type == 'credit_card') {
 
              if($fraud == 'challenge') {
                $payment->setPending();
              } else {
                $payment->setSuccess();
                foreach(ParentRelate::where('student_id',$payment->student_id)->get() as $value)
                {
                  $this->notification->send("parentid".$value->parent_id,$request->title,"Info keuangan");
                }
                
              }
 
            }
 
          } elseif ($transaction == 'settlement') {
            $payment->setSuccess();
            foreach(ParentRelate::where('student_id',$payment->student_id)->get() as $value)
            {
              $this->notification->send("parentid".$value->parent_id,"Pembayaran ".$payment->bill->name." sukses di bayar","Info keuangan");
            }

            return "okey";
 
          } elseif($transaction == 'pending'){
            $payment->setPending();
 
          } elseif ($transaction == 'deny') {
            $payment->setFailed();
 
          } elseif ($transaction == 'expire') {
            $payment->setExpired();
 
          } elseif ($transaction == 'cancel') {
            $payment->setFailed();
 
          }
    }
}
