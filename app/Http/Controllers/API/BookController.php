<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Book;
use App\BookSingle;
use App\TemporaryBorrow;

class BookController extends Controller
{
    public function book_status()
    {
        return response()->json([
            'book'=>Book::latest()->get(),
        ],200);
    }

    public function data($id)
    {
        return response()->json([
            'book_single'=>BookSingle::where('book_id',$id)->where('status','available')->get(),
        ],200);
    }

    public function load_current_temporary($id)
    {
        $data_array  = [];
        $data_real   = [];
        $array       = [];
        $data = TemporaryBorrow::where('student_id',$id)->latest()->get();
        foreach($data as $value)
        {
            $data_real  = $value;
            $data_array = $value->book->book;
            $array[] = [
                'book_data'=>$data_array,
                'data'     =>$data_real,
            ];
        }
        
        return response()->json(
            $array
        ,200);
    }

    public function show()
    {
        return response()->json([
            'book'=>Book::latest()->get(),
        ],200);
    }
}
