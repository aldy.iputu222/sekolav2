<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TemporaryBorrow;
class BorrowController extends Controller
{
    public function get_books($student_id)
    {
        return response()->json([
            'data'=>TemporaryBorrow::where('student_id',$student_id)->with('book_single')->latest()->get()
        ]
        ,200);
        
    }
}
