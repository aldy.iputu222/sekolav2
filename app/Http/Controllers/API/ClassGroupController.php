<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\ClassGroup;
use App\Http\Controllers\Controller;

class ClassGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function by_schoolyear(Request $request)
    {
        return response()->json(
            ClassGroup::where([
                'school_year_id' =>$request->school_year_id,
                'status'         =>'active'
            ])->latest()->get(),
            200
        );
    }

    public function get_class($id)
    {
        $kelas = ClassGroup::where('major_id',$id)
        ->where('school_year_id',1)->get();

        return response()->json(['code'=>'200','kelas'=>$kelas]);
    }
}
