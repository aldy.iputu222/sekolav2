<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GuestBook;
class GuestBookController extends Controller
{
    public function index()
    {
        return response()->json(GuestBook::latest()->paginate(15),200);
    }

    public function today()
    {
        return response()->json(GuestBook::whereRaw('DATE(created_at) = CURDATE()')->latest()->paginate(15),200);
    }

    public function store(Request $request)
    {
        $store = GuestBook::create($request->all());
        if($store)
        {
            return response()->json([
                'status'=>'success',
                'data'=>$store,
            ],200);
        }else{
            return response()->json([
                'status'=>'failed',
            ],201);
        }
    }
}
