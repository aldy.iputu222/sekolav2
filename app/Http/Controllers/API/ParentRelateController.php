<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\ParentRelate;
use App\StudentParent;
class ParentRelateController extends Controller
{
    public function relate(Request $request)
    {
        $check = ParentRelate::where($request->all())->count();
        if($check > 0)
        {
            return response()->json([
                'status'=>'was-added',
            ]);
        }else{
            ParentRelate::create($request->all());

            return response()->json([
                'data'=>StudentParent::where('id',$request->parent_id)->with('parent_relate')->first(),
                'status'=>'success',
            ]);
        }
    }
}
