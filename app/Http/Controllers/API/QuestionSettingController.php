<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Bab;
use App\Http\Controllers\Controller;

class QuestionSettingController extends Controller
{
    public function by_subjectid(Request $request)
    {
        return response()->json(
            Bab::where([
                'subject_id' =>$request->subject_id,
            ])->latest()->get(),
            200
        );
    }
}
