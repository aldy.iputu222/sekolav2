<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Schedule;
use App\Student;
class ScheduleController extends Controller
{
    public function detail($student_id)
    {
        $student  = Student::where('id',$student_id)->first();
        $schedule = Schedule::where('class_id',$student->class_group_id);
        return response()->json([
            'senin'=>Schedule::where('class_id',$student->class_group_id)->where('hari','senin')
            ->with('subject')
            ->with('teacher')
            ->orderBy('jam_ke','ASC')->get(),
            'selasa'=>Schedule::where('class_id',$student->class_group_id)->where('hari','selasa')
            ->with('subject')
            ->with('teacher')
            ->orderBy('jam_ke','ASC')->get(),
            'rabu'=>Schedule::where('class_id',$student->class_group_id)->where('hari','rabu')
            ->with('subject')
            ->with('teacher')
            ->orderBy('jam_ke','ASC')->get(),
            'kamis'=>Schedule::where('class_id',$student->class_group_id)->where('hari','kamis')
            ->with('subject')
            ->with('teacher')
            ->orderBy('jam_ke','ASC')->get(),
            'jumat'=>Schedule::where('class_id',$student->class_group_id)->where('hari','jumat')
            ->with('subject')
            ->with('teacher')
            ->orderBy('jam_ke','ASC')->get(),
        ]);
    }
}
