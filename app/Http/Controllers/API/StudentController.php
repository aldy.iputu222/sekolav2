<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Student;
use Hash;

class StudentController extends Controller
{
    public function index($school_year_id, $class_group_id)
    {
        $data = Student::latest()->where('school_year_id', $school_year_id)->where('class_group_id', $class_group_id)->get();
        return response()->json($data,200);
    }

    public function do_borrow(Request $request)
    {
        return Student::where([
            'school_year_id'=>$request->school_year_id,
            'class_group_id'=>$request->class_group_id,
        ])->orderBy('name')->get();
    }

    public function login(Request $request)
    {
        $obj = Student::where('nis',$request->nis);
        if($obj->count() > 0)
        {
            if (Hash::check($request->password, $obj->first()->password)) {
                $response = [
                    'status'=>'success',
                    'data'  =>$obj->first(),
                ];
            }else{
                $response = [
                    'status'=>'fail',
                ];
            }
        }else{
            $response = [
                'status'=>'fail',
            ];
        }

        return response()->json($response);
    }

    public function check(Request $request)
    {
        $qrcode = explode('-',$request->id);
        if($qrcode[0] == "student"){
            $student = Student::where('id',$qrcode[1]);
            if($student->count() > 0){
                $response = [
                    'data'  =>$student->with('classgroup')->first(),
                    'status'=>'found',
                ];
            }else{
                $response = [
                    'status'=>'not-found',
                ];
            }
        }else{
            $response = [
                'status'=>'fail',
                'message'=>'qrcode-not-valid',
            ];
        }

        return response()->json($response,200);
    }

    public function friends($id)
    {
        $student       = Student::where('id',$id)->first();
        $student_class = Student::where('class_group_id',$student->class_group_id)
        ->whereNotIn('id',[$id])
        ->with('classgroup')
        ->get();

        return response()->json(
            Student::where('status','aktif')->orderBy('name','ASC')->paginate(15)
            ,200);
    }

    public function by_nis($nis)
    {
        $student = Student::where('nis',$nis);
        if($student->count() > 0)
        {
            return response()->json([
                'data'=>$student->first(),
            ]);
        }else{
            return response()->json([
                'data'=>'',
            ]);
        }
    }
}
