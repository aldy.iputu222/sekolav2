<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Teacher;
use Hash;
class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Teacher::with('subject')->latest()->paginate(10),200);
    }

    public function login(Request $request)
    {
        $obj = Teacher::where('nip',$request->nip);
        if($obj->count() > 0)
        {
            if (Hash::check($request->password, $obj->first()->password)) {
                $response = [
                    'status'=>'success',
                    'data'  =>$obj->first(),
                ];
            }else{
                $response = [
                    'status'=>'fail',
                ];
            }
        }else{
            $response = [
                'status'=>'fail',
            ];
        }

        return response()->json($response);
    }
}