<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\TemporaryBorrow;
use App\BookSingle;
use App\Http\Controllers\Controller;
class TemporaryBorrowController extends Controller
{
    public function store(Request $request)
    {
        
        $checking = BookSingle::where([
            'id'=>$request->book_single_id,
        ]);

        if($checking->count() > 0)
        {
            if($checking->first()->status == "borrow")
            {
                return response()->json([
                    'message'=>'Buku sedang di pinjam',
                    'status'=>'unavailable',
                ]);
            }else{
                // return $request->all();
                BookSingle::where('id',$request->book_single_id)->update(['status'=>'borrow']);
                TemporaryBorrow::create($request->all());
                return response()->json(['status'=>'success'],200);
            }
        }else{
            return response()->json([
                'message'=>'Buku tidak tersedia',
                'status'=>'not-found',
            ]);
        }
    }

    public function delete($id)
    {
        BookSingle::where('id',$id)->update(['status'=>'available']);
        TemporaryBorrow::where('id',$id)->delete();;
        return response()->json(['status'=>'success','message'=>'Berhasil'],200);
    }
}
