<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Student;
use App\Uks;
class UksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $qrcode = explode('-',$request->id);
        if($qrcode[0] == "student"){
            if(Student::find($qrcode[1])->count() > 0){
                if(Uks::where('student_id',$qrcode[1])->whereRaw("DATE(created_at) = CURDATE()")->count() <= 0){
                    Uks::create([
                        'student_id'=>$qrcode[1],
                        'description'=>$request->description,
                    ]);

                    $response = [
                        'status'=>'success',
                        'message'=>'was-scanned',
                    ];

                    $notification = new Notification();
                    $notification->send("parent-by_id-".$qrcode[1],"Anak Anda Sakit","Berikut deskripsi sakit...");

                }else{
                    $response = [
                        'status'=>'fail',
                        'message'=>'was-scanned',
                    ];
                }
            }else{
                $response = [
                    'status'=>'fail',
                    'message'=>'student-not-found',
                ];
            }
        }else{
            $response = [
                'status'=>'fail',
                'message'=>'qrcode-not-valid',
            ];
        }

        return response()->json($response,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
