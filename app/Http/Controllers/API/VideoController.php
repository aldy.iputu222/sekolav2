<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Video;
class VideoController extends Controller
{
    
    public function index()
    {
        return response()->json(
            Video::latest()->paginate()
        ,200);
    }


}
