<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Major;
use App\Absent;
use App\Student;
use App\ClassGroup;
use App\SchoolYear;
use Carbon\Carbon;
use Auth;

class AbsentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tahun_pelajarans = SchoolYear::where('status','active')->get();
        $jurusans = Major::all();
        return view('pages.absent.index',['tahun_pelajarans'=>$tahun_pelajarans,'jurusans'=>$jurusans]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $tahun_pelajarans = SchoolYear::where('status','active')->get();
        $jurusans = Major::all();
        return view('pages.absent.create',['tahun_pelajarans'=>$tahun_pelajarans,'jurusans'=>$jurusans]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $count_students = Student::where([
            ['school_year_id',$request->tahun_pelajaran],
            ['class_group_id',$request->kelas],
            ]
        )->count();

        
        for ($i=1; $i <= $count_students ; $i++) {
             $string = explode("-",$request->status[$i]);
             $datas[] = [
                'student_id' => $string[0],
                'status' => $string[1]
             ];
        }

        foreach ($datas as $data) {
            $absent = Absent::create([
                'student_id' => $data['student_id'],
                'status' => $data['status'],
                'type' => 'Manual',
                'date' => $request->tgl_absen
            ]);
        }
        
        return redirect(route('absent.create'))->with('message','Menambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function input_absen(Request $request)
    {
        
        // return $request->all();
        $major = Major::where('id',$request->jurusan)->first();
        $class = ClassGroup::where('id',$request->kelas)->first();
        $school_year = SchoolYear::where('id',$request->tahun_pelajaran)->first();
        $student_id = $class->students->first()->id;
        $count = Absent::where([
            'student_id'=>$student_id,
            'date'=>$request->date,
        ])->count();

        // return $count;

        if($count > 0)
        {
            return back()->with('info','Absen Sudah di Input');
        }else{
            $students = Student::where([
                ['school_year_id',$request->tahun_pelajaran],
                ['class_group_id',$request->kelas],
                ]
            )->get()->sortBy('name');
    
            $school_year_id = $request->tahun_pelajaran;
            $class_group_id = $request->kelas;
    
            return view('pages.absent.v_input',['students'=>$students,'school_year_id'=>$school_year_id,'class_group_id'=>$class_group_id,'major'=>$major,'class'=>$class,'schoolyear'=>$school_year,'date'=>$request->date]);
        }
    }

    public function data_absen(Request $request)
    {
        $major = Major::where('id',$request->jurusan)->first();
        $class = ClassGroup::where('id',$request->kelas)->first();
        $school_year = SchoolYear::where('id',$request->tahun_pelajaran)->first();

        $students = Student::where([
            ['school_year_id',$request->tahun_pelajaran],
            ['class_group_id',$request->kelas],
            ]
        )->get()->sortBy('name');
        
        if (count($students) > 0) {
        foreach ($students as $student) {
            $count_i = Absent::where('student_id',$student['id'])
                    ->where('status','I')
                    ->where('date',$request->date)
                    ->count();
            $count_a = Absent::where('student_id',$student['id'])
                    ->where('status','A')
                    ->where('date',$request->date)
                    ->count();
            $count_s = Absent::where('student_id',$student['id'])
                    ->where('status','S')
                    ->where('date',$request->date)
                    ->count();        

            $data_students[] = [
                'id' => $student['id'],
                'nis' => $student['nis'],
                'name' => $student['name'],
                'gender' => $student['gender'],
                'count_i' => $count_i,
                'count_a' => $count_a,
                'count_s' => $count_s
            ];
            }
            
        }else{
            $data_students = [];
        }
        
 
       return view('pages.absent.show',[
           'students'=>$data_students,
           'major'=>$major,
           'class'=>$class,
           'schoolyear'=>$school_year,
           'date'=>$request->date,
        ]);
        
    }


    public function manual_month()
    {
        $manual_month =
             array([
            'id' => '01',
            'name' => 'Januari'
            ],
            [
            'id' => '02',
            'name' => 'Febuary'
            ],
            [
            'id' => '03',
            'name' => 'Maret'
            ],
            [
            'id' => '04',
            'name' => 'April'
            ],
            [
            'id' => '05',
            'name' => 'Mei'
            ],
            [
            'id' => '06',
            'name' => 'Juni'
            ],
            [
            'id' => '07',
            'name' => 'Juli'
            ],
            [
            'id' => '08',
            'name' => 'Agustus'
            ],
            [
            'id' => '09',
            'name' => 'September'
            ],
            [
            'id' => '10',
            'name' => 'Oktober'
            ],
            [
            'id' => '11',
            'name' => 'November'
            ],
            [
            'id' => '12',
            'name' => 'Desember'
            ]);

        return $manual_month;
    }

    public function student_index()
    {
        $month = Carbon::now()->format('m');
      
        $count_i = Absent::whereMonth('date',$month)
        ->where('student_id',Auth::guard('web_student')->user()->id)
        ->where('type','Machine')
        ->where('status','I')
        ->count();

        $count_a = Absent::whereMonth('date',$month)
        ->where('student_id',Auth::guard('web_student')->user()->id)
        ->where('type','Machine')
        ->where('status','A')
        ->count();

        $count_s = Absent::whereMonth('date',$month)
        ->where('student_id',Auth::guard('web_student')->user()->id)
        ->where('type','Machine')
        ->where('status','S')
        ->count();

        $report = Absent::whereMonth('date',$month)
        ->where('student_id',Auth::guard('web_student')->user()->id)
        ->where('type','Machine')
        ->get();


        return view('pages.absent.student_index',['count_i'=>$count_i,'count_a'=>$count_a,'count_s'=>$count_s,'report'=>$report,'months'=>$this->manual_month()]);
    }


    public function student_filter(Request $request)
    {   $month = explode('-',$request->month);

        $count_i = Absent::whereMonth('date',$request->month)
        ->where('student_id',Auth::guard('web_student')->user()->id)
        ->where('type','Machine')
        ->where('status','I')
        ->count();

        $count_a = Absent::whereMonth('date',$request->month)
        ->where('student_id',Auth::guard('web_student')->user()->id)
        ->where('type','Machine')
        ->where('status','A')
        ->count();

        $count_s = Absent::whereMonth('date',$request->month)
        ->where('student_id',Auth::guard('web_student')->user()->id)
        ->where('type','Machine')
        ->where('status','S')
        ->count();

        $report = Absent::whereMonth('date',$request->month)
        ->where('student_id',Auth::guard('web_student')->user()->id)
        ->where('type','Machine')
        ->get();

        return view('pages.absent.student_filter',['count_i'=>$count_i,'count_a'=>$count_a,'count_s'=>$count_s,'report'=>$report,'months'=>$this->manual_month(),'this_month'=>$month[1]]);
    }


}
