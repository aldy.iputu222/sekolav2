<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\categoryType;
use Storage;
use Hash;
use DNS1D;
use DNS2D;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $article = Article::latest()->get();
        return view('pages.web-profile.article.index',compact('article'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $kategori = categoryType::all();
        return view('pages.web-profile.article.create',compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'=>'required',
            'content'=>'required',
            'image'=>'required',
            'category_content_id'=>'required',
        ]);

        $do = new Article($request->all());
        $do->image = $request->file('image')->store('web_profile/article');
        $do->save();
            
        return redirect()->route('article.index')->withMessage("Artikel Berhasil ditambahkan");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        $file = $article->image;
        $tmp = explode('.',$file);
        $ext = end($tmp);
        return view('pages.web-profile.article.show',[
            'data'=>$article,
            'ext'=>$ext,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        $file = $article->image;
        $tmp = explode('.',$file);
        $ext = end($tmp);
        return view('pages.web-profile.article.edit', [
            'data'=>$article,
            'kategori' => categoryType::all(),
            'ext'=>$ext,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        $this->validate($request, [
            'title'=>'required',
            'content'=>'required',
            'category_content_id'=>'required',
        ]);
        
        if ($request->hasFile('image')) 
        {
            $object = Article::find($article->id);
            Storage::delete($object->image);
            $object->image = $request->file('image')->store('web_profile/article');
            $object->save();
        } else {
            Article::find($article->id)->update($request->except('image','_token','_method'));
        }

        return redirect()->route('article.index')->with('message','Update Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        unlink(public_path('app/').$article->image);
        $article->delete();
        return redirect()->route('article.index')->with('message','Menghapus');
    }
}
