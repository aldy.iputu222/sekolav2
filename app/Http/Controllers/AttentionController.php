<?php

namespace App\Http\Controllers;

use App\Attention;
use App\AttentionFilter;
use App\ClassGroup;
use Illuminate\Http\Request;
use Auth;
use App\SchoolYear;
use App\Notification;
class AttentionController extends Controller
{

    private $notification;
    public function __construct()
    {
        $this->notification = new Notification();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::guard('web')->check())
        {
            $data = Attention::where('from','admin')->latest()->get();
        }else if(Auth::guard('web_teacher')->check()){
            $data = Attention::where([
                'from'=>'teacher',
                'from_id'=>Auth::guard('web_teacher')->user()->id,
            ])->latest()->get();
        }

        return view('pages.attention.index',[
            'data'=>$data,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = SchoolYear::where('status','active')->latest()->get();
        $array = [];
        foreach($data as $val)
        {
            foreach(ClassGroup::where(['school_year_id'=>$val->id,'status'=>'active'])->with('school_year')->latest()->get() as $field)
            {
                $array[] = $field;
            }
        }

        return view('pages.attention.create',[
            'classgroup'=>$array,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required',
            'content'=>'required',
            'for_who'=>'required',
            'type'=>'required',
            'class_group_filter'
        ]);

        $attention = new Attention($request->except('class_group_filter'));
        if(Auth::guard('web')->check())
        {
            $attention->from_id = Auth::user()->id;
            $attention->from    = "admin";
            $from = Auth::user()->name;
        }else if(Auth::guard('web_teacher')->check())
        {
            $attention->from_id = Auth::guard('web_teacher')->user()->id;
            $attention->from    = "teacher";   
            $from = Auth::guard('web_teacher')->user()->name;
        }

        // return $attention;
        $attention->save();
        $id = $attention->id;

        if($request->for_who == "student" && 
        $request->type    == "class"){
            if(count($request->class_group_filter) > 0){
                foreach($request->class_group_filter as $data){
                    AttentionFilter::create([
                        'class_group_id'=>$data,
                        'attention_id'=>$id,
                        'for_who'=>$request->for_who,
                        'type'=>$request->type,
                    ]);
                }

                $topics = $request->for_who."-classgroup-".$data;
            }
        }
        else if($request->for_who == "parent" && 
        $request->type    == "class"){
            if(count($request->class_group_filter) > 0){
                foreach($request->class_group_filter as $data){
                    AttentionFilter::create([
                        'class_group_id'=>$data,
                        'attention_id'=>$id,
                        'for_who'=>$request->for_who,
                        'type'=>$request->type,
                    ]);
                }

                $topics = $request->for_who."-classgroup-".$data;
            }
        }else{
            AttentionFilter::create([
                'attention_id'=>$id,
                'for_who'=>$request->for_who,
                'type'=>$request->type,
            ]);
            
            $topics = $request->for_who;
        }

        $this->notification->send($topics,$request->title,"Pengumuman dari ".$from);

        return redirect()->route('attention.index')->with('message','Menambahkan');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Attention  $attention
     * @return \Illuminate\Http\Response
     */
    public function show(Attention $attention)
    {
        return view('pages.attention.show',[
            'data'=>$attention,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Attention  $attention
     * @return \Illuminate\Http\Response
     */
    public function edit(Attention $attention)
    {
        if($attention->attention_filter[0]->type == "all")
        {
            $before = ClassGroup::where('status','active')->get();
        }else{
            $array = [];
            foreach($attention->attention_filter as $field)
            {
                $array[] = $field->class_group_id;
            }

            $class     = ClassGroup::where('status','active')->whereIn('id',$array)->latest()->get();
            $class_not = ClassGroup::where('status','active')->whereNotIn('id',$array)->latest()->get();

            $before = [];
            foreach($class as $field)
            {
                $field['is_selected'] = "selected";
                $before[] = $field;
            }

            foreach($class_not as $field)
            {
                $field['is_selected'] = "";
                $before[] = $field;
            }
        }
        
        return view('pages.attention.edit',[
            'data'=>$attention,
            'classgroup'=>$before,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Attention  $attention
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Attention $attention)
    {
        $do_update = Attention::find($attention->id);
        $do_update->title     = $request->title;
        $do_update->content   = $request->content;
        $do_update->important = $request->important;
        $do_update->save();

        // AttentionFilter::where('attention_id',$attention->id)->delete();

        if($request->type == "class")
        {
            $intArray = array_map(
                function($value) { return (int)$value; },
                $request->class_group_filter
            );
            $intArray2 = array_map(
                function($value) { return (int)$value; },
                $attention->attention_filter()->pluck('class_group_id')->toArray()
            );
    
            $check_min   = array_diff($intArray2,$intArray);
            $check_plus  = array_diff($intArray,$intArray2);
    
            foreach($check_plus as $num)
            {
                AttentionFilter::create([
                    'class_group_id'=>$num,
                    'attention_id'=>$attention->id,
                    'for_who'=>$request->for_who,
                    'type'=>$request->type,
                ]);
            }
            
            AttentionFilter::whereIn('class_group_id',$check_min)->delete();

        }else{
            AttentionFilter::create([
                'attention_id'=>$attention->id,
                'for_who'=>$request->for_who,
                'type'=>$request->type,
            ]);
        }

        return redirect()->route('attention.index')->with('message','Berhasil Mengubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Attention  $attention
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attention $attention)
    {
        $attention->delete();
        AttentionFilter::where('attention_id',$attention->id)->delete();
        return back()->with('message','Menghapus');
    }

    public function student_attention()
    {
        $classgroup =  Auth::guard('web_student')->user()->class_group_id;
        // return AttentionFilter::where('class_group_id',$classgroup)->with('attention')->latest()->paginate(5);
        return view('pages.attention.student_index',[
            'data'=>AttentionFilter::where('class_group_id',$classgroup)->orWhere('type','all')->with('attention')->latest()->paginate(5),
        ]);
    }
}
