<?php

namespace App\Http\Controllers;

use App\AttentionFilter;
use Illuminate\Http\Request;

class AttentionFilterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AttentionFilter  $attentionFilter
     * @return \Illuminate\Http\Response
     */
    public function show(AttentionFilter $attentionFilter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AttentionFilter  $attentionFilter
     * @return \Illuminate\Http\Response
     */
    public function edit(AttentionFilter $attentionFilter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AttentionFilter  $attentionFilter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AttentionFilter $attentionFilter)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AttentionFilter  $attentionFilter
     * @return \Illuminate\Http\Response
     */
    public function destroy(AttentionFilter $attentionFilter)
    {
        //
    }
}
