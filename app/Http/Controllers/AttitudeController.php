<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Attitude;
use Illuminate\Support\Str;


class AttitudeController extends Controller
{
    
    public function index()
    {
        return view('pages.attitude.index',['data'=> Attitude::all()]);
        $data = Attitude::orderBy('code','asc')->get();
        return view('pages.attitude.index',['data'=>$data]);
    }

    public function code($type)
    {
        if ($type == "reward") {
            $count = Attitude::where('type','reward')->count();
            $first_letter = "R";
            if ($count > 0) {
                $get_latest = Attitude::where('type','reward')->latest()->first();
                $number = substr($get_latest->code,1) + 1;
            }else{
                $number = $count + 1;
            }
            $code = $first_letter.$number;
        }else
        {
            $count = Attitude::where('type','punishment')->count();
            $first_letter = "P";
            if ($count > 0) {
                $get_latest = Attitude::where('type','punishment')->latest()->first();
                $number = substr($get_latest->code,1) + 1;
            }else{
                $number = $count + 1;
            }
            $code = $first_letter.$number;
        }

        return $code;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()

    {

    

        return view('pages.attitude.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[

            'name'=>'required',
            'type'=>'required',
            'code'=>'required',
        ]);

        $att = new Attitude($request->all());
        $att->save();


        $do = new Attitude($request->all());
        $do->code = $this->code($request->type);
        $do->save();

        return redirect()->route('attitude.index')->withMessage('Reward/Punishment Berhasil Di Tambahkan');

    }

    /**
     * Display the specified resource.
     *

     * @param  \App\Attitude  $attitude
     * @return \Illuminate\Http\Response
     */
    public function show(Attitude $attitude)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *

     * @param  \App\Attitude  $attitude
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Attitude::find($id);
        return view('pages.attitude.edit',['data'=>$data]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request

     * @param  \App\Attitude  $attitude

     * @param  int  $id

     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         $this->validate($request,[
            'name'=>'required',
            'type'=>'required',
            'code'=>'required',
        ]);

        Attitude::where('id',$id)->update($request->except('_token','_method'));

        return redirect()->route('attitude.index')->withMessage('Attitude Updated');

    }

    /**
     * Remove the specified resource from storage.
     *

     * @param  \App\Attitude  $attitude
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $do = Attitude::find($id);
        $do->delete();

        return back()->withMessage('Reward/Punishment Berhasil Di Hapus');

    }
}
