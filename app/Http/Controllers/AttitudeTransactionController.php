<?php

namespace App\Http\Controllers;
use Auth;
use App\AttitudeTransaction;
use App\Student;
use App\Attitude;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ParentRelate;
use App\Notification;
class AttitudeTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.rekap.index',['data'=>AttitudeTransaction::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ybs = Student::all();
        $att = Attitude::all();
        return View('pages.rekap.create',compact('att','ybs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'user_id'=>'required',
            'attitude_id'=>'required',
        ]);

        $att = new AttitudeTransaction($request->all());
        $att->teacher_id = Auth::guard('web_teacher')->user()->id;
        $att->save();

        $notification = new Notification();
        foreach(ParentRelate::where('student_id',$request->user_id)->get() as $parent_relate)
        {
            if($att->attitude->type == "reward")
            {
                $text = " Menerima Reward di Sekolah";
            }else{
                $text = " Melakukan Pelanggaran";
            }

            $notification->send("parentid".$parent_relate->parent_id,$parent_relate->student->name.$text,"Sikap Siswa");        
        }

        return redirect()->route('attitudeTransaction.index')->withMessage('Rekap Telah Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AttitudeTransaction  $attitudeTransaction
     * @return \Illuminate\Http\Response
     */
    public function show(AttitudeTransaction $attitudeTransaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AttitudeTransaction  $attitudeTransaction
     * @return \Illuminate\Http\Response
     */
    public function edit(AttitudeTransaction $attitudeTransaction)
    {
        $data = $attitudeTransaction;
        return view('pages.rekap.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AttitudeTransaction  $attitudeTransaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AttitudeTransaction $attitudeTransaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AttitudeTransaction  $attitudeTransaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(AttitudeTransaction $attitudeTransaction)
    {
        $attitudeTransaction->delete();
        return back()->withMessage('Terhapus');
    }
}
