<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Banner;
use Storage;
class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Banner::latest()->get();
        return view('pages.banner.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.banner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'subname'=>'required',
            'status'=>'required',
            'image'=>'required|file|mimes:jpg,png,jpeg',
        ]);

        $object = new Banner($request->all());
        $object->image = $request->file('image')->store('banner');
        $object->save();

        return redirect()->route('banner.index')->withMessage("Banner Berhasil ditambahkan");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Banner $banner)
    {
        return view('pages.banner.edit',['data'=>$banner]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Banner $banner)
    {
       $this->validate($request, [
            'name'=>'required',
            'subname'=>'required',
        ]);
        
        if ($request->hasFile('image')) 
        {
            $object = Banner::find($banner->id);
            Storage::delete($object->image);
            $object->image = $request->file('image')->store('banner');
            $object->save();
        } else {
            Banner::find($banner->id)->update($request->except('image','_token','_method'));
        }
        return redirect()->route('banner.index')->withMessage('Mengubah Banner');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Banner::destroy($id);

        return back()->withMessage('Menghapus Banner');
    }
}
