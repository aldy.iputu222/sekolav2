<?php

namespace App\Http\Controllers;

use App\bill;
use App\BillFilter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ClassGroup;
class BillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $data = Bill::latest()->get();
        return view('pages.bill.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.bill.create',[
            'classgroups'=>ClassGroup::where('status','active')->latest()->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'content'=>'required',
            'total'=>'required',
            'class_group_filter'=>'required'
        ]);

        $bill = Bill::create($request->except('class_group_filter'));

        for($i = 0;$i < count($request->class_group_filter);$i++)
        {
            BillFilter::create([
                'bill_id'=>$bill->id,
                'class_group_id'=>$request->class_group_filter[$i],
            ]);
        }
        
        return redirect()->route('bill.index')->with('message', 'Berhasil diSimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\bill  $bill
     * @return \Illuminate\Http\Response
     */
    public function show(bill $bill)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\bill  $bill
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Bill::find($id);
        $array = [];
        foreach($data->bill_filters as $field)
        {
            $array[] = $field->class_group_id;
        }

        $class     = ClassGroup::where('status','active')->whereIn('id',$array)->latest()->get();
        $class_not = ClassGroup::where('status','active')->whereNotIn('id',$array)->latest()->get();

        $before = [];
        foreach($class as $field)
        {
            $field['is_selected'] = "selected";
            $before[] = $field;
        }

        foreach($class_not as $field)
        {
            $field['is_selected'] = "";
            $before[] = $field;
        }
        return view('pages.bill.edit', [
            'data'=>$data,
            'classgroups'=>$before,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\bill  $bill
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $bill = Bill::find($id);
        $intArray = array_map(
            function($value) { return (int)$value; },
            $request->class_group_filter
        );
        $intArray2 = array_map(
            function($value) { return (int)$value; },
            $bill->bill_filters()->pluck('class_group_id')->toArray()
        );

        $check_min   = array_diff($intArray2,$intArray);
        $check_plus  = array_diff($intArray,$intArray2);

        foreach($check_plus as $num)
        {
            BillFilter::create([
                'bill_id'=>$id,
                'class_group_id'=>$num,
            ]);
        }
        
        BillFilter::whereIn('class_group_id',$check_min)->delete();
        


        Bill::where('id', $id)->update([
            'name' => $request->name,
            'content' => $request->content,
            'total' => $request->total
        ]);
        return redirect()->route('bill.index')->with('message', 'Data Berhasil Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\bill  $bill
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bill = Bill::find($id);
        $bill->bill_filters()->delete();
        $bill->delete();
        return redirect()->route('bill.index')->with('message','Data diHapus');
    }
}
