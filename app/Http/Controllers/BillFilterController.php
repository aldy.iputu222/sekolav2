<?php

namespace App\Http\Controllers;

use App\BillFilter;
use Illuminate\Http\Request;

class BillFilterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BillFilter  $billFilter
     * @return \Illuminate\Http\Response
     */
    public function show(BillFilter $billFilter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BillFilter  $billFilter
     * @return \Illuminate\Http\Response
     */
    public function edit(BillFilter $billFilter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BillFilter  $billFilter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BillFilter $billFilter)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BillFilter  $billFilter
     * @return \Illuminate\Http\Response
     */
    public function destroy(BillFilter $billFilter)
    {
        //
    }
}
