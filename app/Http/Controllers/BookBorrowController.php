<?php

namespace App\Http\Controllers;

use App\BookBorrow;
use App\SchoolYear;
use App\ClassGroup;
use App\BorrowType;
use App\Student;
use App\TemporaryBorrow;
use App\BookSingle;
use Illuminate\Http\Request;
use Auth;
use App\Book;

class BookBorrowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->route('book_borrow.create');
        // return view('pages.book_borrow.index',[
        //     'data'=>BookBorrow::latest()->get(),
        // ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.book_borrow.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Student::where('nis',$request->nis)->count() > 0)
        {
            return redirect()->route('book_borrow.next',$request->nis);
        }else{
            return back()->with('info','NIS Tidak di temukan!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BookBorrow  $bookBorrow
     * @return \Illuminate\Http\Response
     */
    public function show(BookBorrow $bookBorrow)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BookBorrow  $bookBorrow
     * @return \Illuminate\Http\Response
     */
    public function edit(BookBorrow $bookBorrow)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BookBorrow  $bookBorrow
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BookBorrow $bookBorrow)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BookBorrow  $bookBorrow
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $temp = TemporaryBorrow::where('book_single_id',$id)->first();
        if($temp->count() > 0)
        {
            
            BookSingle::where('id',$temp->book_single_id)->update(['status'=>'available']);
            TemporaryBorrow::where('book_single_id',$id)->update(['status'=>'returned']);
            return response()->json([
                'status'=>'success',
                'message'=>'Peminjaman di batalkan'
            ]);
        }else{
            return response()->json([
                'status'=>'not-available',
                'message'=>'Tidak tersedia'
            ]);
        }
    }

    public function next($nis)
    {
        return view('pages.book_borrow.next',[
            'data'=>Student::where('nis',$nis)->first(),
            'book'=>Book::latest()->get(),
        ]);
    }
}
