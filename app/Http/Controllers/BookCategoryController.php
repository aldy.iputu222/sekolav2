<?php

namespace App\Http\Controllers;

use App\BookCategory;
use Illuminate\Http\Request;

class BookCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.book_category.index',['data'=>BookCategory::latest()->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.book_category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|unique:book_categories'
        ]);

        BookCategory::create($request->all());

        return redirect()->route('book_category.index')->with('message','Menambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BookCategory  $bookCategory
     * @return \Illuminate\Http\Response
     */
    public function show(BookCategory $bookCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BookCategory  $bookCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(BookCategory $bookCategory)
    {
        return view('pages.book_category.edit',['data'=>$bookCategory]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BookCategory  $bookCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BookCategory $bookCategory)
    {
        $this->validate($request,[
            'name'=>'required'
        ]);

        BookCategory::find($bookCategory->id)->update($request->except('_token','_method'));
        return redirect()->route('book_category.index')->with('message','Mengubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BookCategory  $bookCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(BookCategory $bookCategory)
    {
        $bookCategory->delete();   
        return redirect()->route('book_category.index')->with('message','Menambahkan');
    }
}
