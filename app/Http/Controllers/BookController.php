<?php

namespace App\Http\Controllers;

use App\Book;
use App\Author;
use App\Publisher;
use App\BookCategory;
use App\BookSingle;
use Storage;
use DNS1D;
use DNS2D;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.book.index',[
            'data'=>Book::latest()->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.book.create',[
            'authors'        =>Author::latest()->get(),
            'publishers'     =>Publisher::latest()->get(), 
            'book_categories'=>BookCategory::latest()->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required',
            'description'=>'required',
            'author_id'=>'required',
            'publisher_id'=>'required',
            'publication_year'=>'required',
            'pages'=>'required',
            'book_category_id'=>'required',
            'cover_book'=>'file'
        ]);

        if($request->hasFile('cover_book'))
        {
            $book = new Book($request->except('copy_count'));
            $book->cover_book = $request->file('cover_book')->store('cover_book');
            $book->save();
            $id = $book->id;
        }else{
            $id = Book::create($request->except('cover_book','copy_count'))->id;           
        }

        for($i = 1;$i<= $request->copy_count;$i++)
        {
            $book_id = BookSingle::create([
                'book_id'=>$id,
                'status' =>'available',
                'date_in'=>date("Y-m-d"),
            ])->id;

            BookSingle::find($book_id)->update([
                'qrcode'=>$book_id,
                'barcode'=>$book_id
            ]);

            Storage::disk('local_bar')
            ->put($book_id.'.jpg',
            base64_decode(DNS1D::getBarcodePNG($book_id, "C39+")));
            Storage::disk('local_qr')
            ->put($book_id.'.jpg',
            base64_decode(DNS2D::getBarcodePNG($book_id, "QRCODE",10,10)));
        } 

        return redirect()->route('book.index')->with('message','Menambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        return view('pages.book.show',[
            'data'=>$book,
            'book_copy'=>BookSingle::where('book_id',$book->id)->paginate(4),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        //
    }
}
