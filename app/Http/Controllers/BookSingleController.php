<?php

namespace App\Http\Controllers;

use App\BookSingle;
use Illuminate\Http\Request;

class BookSingleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BookSingle  $bookSingle
     * @return \Illuminate\Http\Response
     */
    public function show(BookSingle $bookSingle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BookSingle  $bookSingle
     * @return \Illuminate\Http\Response
     */
    public function edit(BookSingle $bookSingle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BookSingle  $bookSingle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BookSingle $bookSingle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BookSingle  $bookSingle
     * @return \Illuminate\Http\Response
     */
    public function destroy(BookSingle $bookSingle)
    {
        //
    }
}
