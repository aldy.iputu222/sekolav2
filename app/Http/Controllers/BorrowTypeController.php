<?php

namespace App\Http\Controllers;

use App\BorrowType;
use Illuminate\Http\Request;

class BorrowTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.borrow_type.index',[
            'data'=>BorrowType::latest()->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.borrow_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'number_date'=>'required',
            'expired_price'=>'required'
        ]);

        BorrowType::create($request->all());
        return redirect()->route('borrow_type.index')->with('message','Menambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BorrowType  $borrowType
     * @return \Illuminate\Http\Response
     */
    public function show(BorrowType $borrowType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BorrowType  $borrowType
     * @return \Illuminate\Http\Response
     */
    public function edit(BorrowType $borrowType)
    {
        return view('pages.borrow_type.edit',['data'=>$borrowType]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BorrowType  $borrowType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BorrowType $borrowType)
    {
        $this->validate($request,[
            'name'=>'required',
            'number_date'=>'required',
            'expired_price'=>'required'
        ]);

        $borrowType->update($request->except('_token','_method'));
        return redirect()->route('borrow_type.index')->with('message','Mengubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BorrowType  $borrowType
     * @return \Illuminate\Http\Response
     */
    public function destroy(BorrowType $borrowType)
    {
        $borrowType->delete();
        return back()->with('message','Menghapus');
    }
}
