<?php

namespace App\Http\Controllers;

use App\ClassGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Major;
use App\SchoolYear;

class ClassGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.classgroup.index', [
            'data'=>ClassGroup::latest()->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.classgroup.create', [
            'data'=>Major::all(),
            'school_years'=>SchoolYear::where('status','active')->latest()->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'majorid'=>'required',
            'class_count'=>'required',
        ]);

        for($i = 1;$i <= $request->class_count;$i++)
        {
            $major  = Major::where('id', $request->majorid)->first();
            $array  = ['XII','XI','X'];
            foreach($array as $value)
            {
                // dd($value);
                $className = $major->alias . ' ' . $value . '-' . $i;
                if($value != "X")
                {
                    // return $value;
                    ClassGroup::create([
                        'name'     => $className,
                        'grade'    => $value,
                        'major_id' => $request->majorid,
                        'school_year_id'=>$request->school_year_id,
                        'status'   => 'not-active'
                    ]);
                }else{
                    ClassGroup::create([
                        'name'     => $className,
                        'grade'    => $value,
                        'major_id' => $request->majorid,
                        'school_year_id'=>$request->school_year_id,
                        'status'   => 'active'
                    ]);
                }
            }
        }

        return redirect()->route('classgroup.index')->with('message', 'Membuat');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ClassGroup  $classGroup
     * @return \Illuminate\Http\Response
     */
    public function show(ClassGroup $data)
    {
        return view('pages.classgroup.show', [
            'data'=>$data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ClassGroup  $classGroup
     * @return \Illuminate\Http\Response
     */
    public function edit(ClassGroup $data)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ClassGroup  $classGroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClassGroup $classGroup)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ClassGroup  $classGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data  = ClassGroup::findOrFail($id);
        $class = explode('-', $data->name);

        $latest      = ClassGroup::latest()->where(['major_id' => $data->major_id, 'grade' => $data->grade])->first();
        $classLatest = explode('-', $latest->name);

        if($class == $classLatest){
            ClassGroup::destroy($data->id);
            return back()->with('message', 'Menghapus');

        }else{
            return back()->with('error', 'Hanya bisa menghapus data paling terakhir');
        }
        
    }
}
