<?php

namespace App\Http\Controllers;

use App\Document;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Teacher;
use App\Subject;
use Storage;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $document = Document::latest()->get();
        return view('pages.document.index',compact('document'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teacher = Teacher::latest()->get();
        $matpel = Subject::latest()->get();
        return view('pages.document.create',compact('teacher','matpel'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string',
            'content' => 'required|string',
            'file' => 'required|max:2048',
            'type' => 'required',
            'status' => 'required',
        ]);



        if ($request->type == "embed") {
            $file = new Document($request->all());
            $file->save();   
        } else {
            $file = new Document($request->all());
            $file->file = $request->file('file')->store('web_profile/file');
            $file->save();
        }

        return redirect()->route('document.index')->with('message', 'File berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $details = Document::where('id',$id)->first();
        $extension = $details->file;
        $tmp       = explode('.',$extension);
        $ext       = end($tmp);
        return view('pages.document.show', compact('details','ext'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function edit(Document $document)
    {
         $file = $document->image;
            $tmp = explode('.',$file);
            $ext = end($tmp);
            return view('pages.document.edit', [
                'edit'=>$document,
                'kategori' => Document::all(),
                'ext'=>$ext,
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Document $document)
    {
         $this->validate($request, [
            'name'=>'required',
            'content'=>'required',
        ]);
        
        if ($request->hasFile('file')) 
        {
            $object = Document::find($document->id);
            Storage::delete($object->file);
            $object->file = $request->file('file')->store('web_profile/file');
             $object->name = $request->input('name');
             $object->content = $request->input('content');
            $object->save();
        } else {
            Document::find($document->id)->update($request->except('teacher_id','matpel_id','file','_token','_method'));
        }

        return redirect()->route('document.index')->with('message','Update Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function destroy(Document $document)
    {
         if ($document->type == "local") {
            unlink(public_path('app/').$document->file);
            $document->delete();
        } else {
            $document->delete();
        }
        return redirect()->route('document.index')->with('message','Menghapus');
    }
}
