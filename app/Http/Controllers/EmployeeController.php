<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;
class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.employee.index',['data'=>Employee::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.employee.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request,[
            'name'=>'required',
            'email'=>'required',
            'address'=>'required',
            'as'=>'required',
            'phone'=>'required',
            'photo'=>'required|file|mimes:jpg,png,jpeg',
        ]);

        $object = new Employee($request->all());
        $object->password = Hash::make($request->name);
        $object->photo = $request->file('photo')->store('employee');
        $object->save();

        return redirect()->route('employee.index')->withMessage("Emloyee Berhasil ditambahkan");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();
        return back()->withMessage("Employee Terhapus");
    }

    public function login(Request $request){
         $email = $request->input('email');
        
         $password = $request->input('password');


         $hashedPassword = Employee::where('email', $email)->first();



        $employee = Employee::where('email',$request->email)->first();
         if ($hashedPassword && Hash::check($password, $hashedPassword->password)) {
                $data = ['status'=>'success','code'=>'200','data'=> $employee];
            }else{
                $data = ['status'=>'failed','code'=>'201'];
            }
            
        return response()->json($data);
    }
}
