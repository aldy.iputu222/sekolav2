<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Eskul;
use Storage;

class EskulController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $data = Eskul::latest()->get();
        return view('pages.web-profile.eskul.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.web-profile.eskul.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request,[
            'name'=>'required',
            'image'=>'required|file|mimes:jpg,png,jpeg',
            'description'=>'required'
        ]);

        $object = new Eskul($request->all());
        $object->image = $request->file('image')->store('eskul');
        $object->save();


        return back()->with('message','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Eskul $eskul)
    {
        return view('pages.web-profile.eskul.edit',['data'=>$eskul]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Eskul $eskul)
    {
         $this->validate($request, [
            'name'=>'required',
            'image'=>'file|mimes:jpg,png,jpeg',
            'description'=>'required'
        ]);
        
        if ($request->hasFile('image')) 
        {
            $object = Eskul::find($eskul->id);
            Storage::delete($object->image);
            $object->image = $request->file('image')->store('eskul');
            $object->save();
        } else {
            Eskul::find($eskul->id)->update($request->except('image','_token','_method'));
        }

        return redirect()->route('eskul.index')->with('message','Update Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Eskul::destroy($id);
        return back()->withMessage('Menghapus Esktrakurikuler');
    }
}
