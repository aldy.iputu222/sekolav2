<?php

namespace App\Http\Controllers;

use App\ExamManage;
use App\ExamType;
use App\QuestionBank;
use App\Subject;
use App\Major;
use App\Bab;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ExamManageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        
        
        return view('pages.exam_manage.index',[
            'exam_manage'=>ExamManage::get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.exam_manage.create',[
            'exam_type'=>ExamType::latest()->get(),
            'subject'=>Subject::latest()->get(),
            'major'=>Major::latest()->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'exam_type_id'=>'required',
            'subject_id'=>'required',
            'major_id'=>'required',
            'grade'=>'required',
            'kkm'=>'required',
            'time'=>'required',
        ]);
        
        $get_alias = Subject::where(['id'=>$request->subject_id])->first();
        $alias = $get_alias->alias;
        $string = str_random($get_alias->id + 10);
        $characters = $alias;
        $pin = mt_rand(7,15)
              .mt_rand(8,13)
              .$characters[rand(0, strlen($characters) - 1)];
              $string = str_shuffle($pin);

        $create = ExamManage::create([
            'exam_type_id'=>$request->exam_type_id,
            'subject_id'  =>$request->subject_id,
            'major_id'    =>$request->major_id,
            'grade'       =>$request->grade,
            'kkm'         =>$request->kkm,
            'time'        =>$request->time,
            'description' =>$request->description,
            'created_by'  =>Auth::guard('web_teacher')->user()->id,
            'token'       =>$alias.$string
        ]);

       

        return redirect()->route('exam_manage.index')->withMessage("Setting Ulangan Berhasil");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ExamManage  $examManage
     * @return \Illuminate\Http\Response
     */
    public function show(ExamManage $examManage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ExamManage  $examManage
     * @return \Illuminate\Http\Response
     */
    public function edit(ExamManage $examManage)
    {
        return view('pages.exam_manage.edit',[
            'data'=>$examManage,
            'exam_type'=>ExamType::all(),
            'subject'=>Subject::all(),
            'major'=>Major::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ExamManage  $examManage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExamManage $examManage)
    {
        $this->validate($request, [
            'exam_type_id'=>'required',
            'subject_id'=>'required',
            'major_id'=>'required',
            'grade'=>'required',
            'kkm'=>'required',
            'time'=>'required',
            'description'=>'required'
        ]);
        
            $object = ExamManage::find($examManage->id);
            $object->save();
            ExamManage::find($examManage->id)->update($request->except('_token','_method'));

        return redirect()->route('exam_manage.index')->with('message','Update Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ExamManage  $examManage
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExamManage $examManage)
    {
        //
    }

    public function choose_question($id,$subject_id)
    {
        // $data  = ExamManage::where(['id'=>$id])->first();
        // $data2 = QuestionBank::where(['subject_id'=>$id])->get();
        $babs   = Bab::where(['subject_id'=>$subject_id])->get();
        $data = QuestionBank::where(['subject_id'=>$subject_id])->get();
        foreach($babs as $val){
            $bab[] = [
                "id" => $val->id,
                "name" => $val->bab
            ];
            // $level1 = QuestionBank::where('difficulty','Mudah')->where('bab_id',$id_bab)->first();
            // $level2 = QuestionBank::where('difficulty','Sedang')->where('bab_id',$id_bab)->first();
            // $level3 = QuestionBank::where('difficulty','Sulit')->where('bab_id',$id_bab)->first();
        }
        dd($bab);
        return view('pages.exam_manage.choose_question',['']);
    }
}
