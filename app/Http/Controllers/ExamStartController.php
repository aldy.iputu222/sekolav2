<?php

namespace App\Http\Controllers;

use App\ExamStart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExamStartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cek  = ExamStart::where('id',$request->id)->count();
        
        if($cek == 1){
            $go = ExamStart::where('id', $request->id)->update([
                'time' => request('time'),
                'status' => request('status')
            ]);
            
        }else{
            $go = ExamStart::create([
                'user_id' => request('user_id'),
                'exam_id' => request('exam_id'),
                'exam_id' => request('exam_id'),
                'time' => request('time'),
                'status' => request('status')
            ]);
        }

        if ($go) {
            $response = ['code'=>200,'status'=>$request->time];
        }else{
            $response = ['code'=>201,'status'=>'error'];
        }
        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ExamStart  $examStart
     * @return \Illuminate\Http\Response
     */
    public function show(ExamStart $examStart)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ExamStart  $examStart
     * @return \Illuminate\Http\Response
     */
    public function edit(ExamStart $examStart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ExamStart  $examStart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExamStart $examStart)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ExamStart  $examStart
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExamStart $examStart)
    {
        //
    }
}
