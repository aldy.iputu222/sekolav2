<?php

namespace App\Http\Controllers;

use App\ParentRelate;
use Illuminate\Http\Request;

class ParentRelateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ParentRelate  $parentRelate
     * @return \Illuminate\Http\Response
     */
    public function show(ParentRelate $parentRelate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ParentRelate  $parentRelate
     * @return \Illuminate\Http\Response
     */
    public function edit(ParentRelate $parentRelate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ParentRelate  $parentRelate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ParentRelate $parentRelate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ParentRelate  $parentRelate
     * @return \Illuminate\Http\Response
     */
    public function destroy(ParentRelate $parentRelate)
    {
        //
    }
}
