<?php

namespace App\Http\Controllers;

use App\Prestation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;
class PrestationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.web-profile.prestation.index',['data'=>Prestation::latest()->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.web-profile.prestation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string',
            'image' => 'required|file|mimes:jpg,png,jpeg',
            'description' => 'required|string',
            'position' => 'required|numeric',
        ]);

        $prestation = new Prestation($request->all());
        $prestation->image = $request->file('image')->store('prestation');
        $prestation->save();

        return redirect()->route('prestation.index')->with('message','Prestasi Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Prestation  $prestation
     * @return \Illuminate\Http\Response
     */
    public function show(Prestation $prestation)
    {
        return view('pages.web-profile.prestation.detail',['data'=>$prestation]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Prestation  $prestation
     * @return \Illuminate\Http\Response
     */
    public function edit(Prestation $prestation)
    {
        return view('pages.web-profile.prestation.edit',['data'=>$prestation]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Prestation  $prestation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Prestation $prestation)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'image' => 'file|mimes:jpg,png,jpeg',
            'description' => 'required',
            'position' => 'required|numeric',
        ]);
        
        if ($request->hasFile('image')) 
        {
            $object = Prestation::find($prestation->id);
            Storage::delete($object->image);
            $object->image = $request->file('image')->store('prestation');
            $object->save();
        } else {
            Prestation::find($prestation->id)->update($request->except('image'));
        }

        return redirect()->route('prestation.index')->with('message','Update Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Prestation  $prestation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Prestation $prestation)
    {
        unlink(public_path('app/').$prestation->image);
        $prestation->delete();
        return redirect()->route('prestation.index')->with('message','Menghapus');
    }
}
