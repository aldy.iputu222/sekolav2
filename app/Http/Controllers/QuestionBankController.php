<?php

namespace App\Http\Controllers;

use App\QuestionBank;
use App\Subject;
use App\Question;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
class QuestionBankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.question_bank_setting.index',[
            'questionbank'=>QuestionBank::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.question_bank_setting.create',[
            'subject'=>Subject::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'subject_id'=>'required',
            'bab_id'=>'required',
            'difficulty'=>'required',
            'question_total'=>'required',
        ]);

        $question_total = $request->question_total;
        $subject_id     = $request->subject_id;
        $bab_id         = $request->bab_id;
        $difficulty     = $request->difficulty;
        $sisa_bobot     = 100 - $question_total;    

        session(['session_question_total'=>$question_total]);
        session(['session_subject_id'=>$subject_id]);
        session(['session_bab_id'=>$bab_id]);
        session(['session_difficulty'=>$difficulty]);
        session(['session_sisa_bobot'=>$sisa_bobot]);
        $do = new QuestionBank;
        $do->subject_id     = $request->subject_id;
        $do->bab_id         = $request->bab_id;
        $do->difficulty     = $request->difficulty;
        $do->question_type  = $request->question_type;
        $do->question_total = $request->question_total;
        $do->created_by     = Auth::guard('web_teacher')->user()->id;
        $question_type      = $request->question_type;
        session(['session_question_type'=>$question_type]);
        $do->save();
        return redirect()->route('teacher.setting')->withMessage("Setting Berhasil");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\QuestionBank  $questionBank
     * @return \Illuminate\Http\Response
     */
    public function show(QuestionBank $questionBank)
    {
        //
    }

    public function addSetting(){
        $question_total = session::get('session_question_total');
        $question_type  = session::get('session_question_type');
        // dd($question_total);
        return view('pages.question_bank_setting.questioncreate',[
            'question_total'=>$question_total,
            'question_type'=>$question_type,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\QuestionBank  $questionBank
     * @return \Illuminate\Http\Response
     */
    public function edit(QuestionBank $questionBank)
    {
        //
    }

    public function addQuestion(Request $request){
        $question_total = session::get('session_question_total');
        $question_type  = session::get('session_question_type');
        if($question_type == "PG"){
            $question_type  = session::get('session_question_type');
            $question_total = session::get('session_question_total');
            $subject_id     = session::get('session_subject_id');
            $bab_id         = session::get('session_bab_id');
            $difficulty     = session::get('session_difficulty');
            if($question_total >= 1){
                $re = Question::create([
                    'subject_id'      => $subject_id,
                    'bab_id'          => $bab_id,
                    'level'           => $difficulty,
                    'question_type'   => $question_type,
                    'question'        => $request->question,
                    'answer_a'        => $request->j_a,
                    'answer_b'        => $request->j_b,
                    'answer_c'        => $request->j_c,
                    'answer_d'        => $request->j_d,
                    'answer_e'        => $request->j_e,
                    'true_answer'     => $request->true_answer,
                    'bobot'           => 1,
                    'created_by'      => Auth::guard('web_teacher')->user()->id
                ]);

                $question_total = session::get('session_question_total');
                session::forget('session_question_total');
                $question_total = $question_total-1;
                session(['session_question_total'=>$question_total]);
                return back()->withMessage('Soal Terinput!');
             }else{
                return redirect()->route('qbank.index')->withMessage('Input Soal Sudah Terlengkapi!');
             }
        }else if($question_type == "ESSAY"){
            $question_type  = session::get('session_question_type');
            $question_total = session::get('session_question_total');
            $subject_id     = session::get('session_subject_id');
            $bab_id         = session::get('session_bab_id');
            $difficulty     = session::get('session_difficulty');
            if($question_total >= 1){
                Question::create([
                    'subject_id'      => $subject_id,
                    'bab_id'          => $bab_id,
                    'level'           => $difficulty,
                    'question_type'   => $question_type,
                    'question'        => $request->question,
                    'bobot'           => 1,
                    'created_by'      => Auth::guard('web_teacher')->user()->id
                ]);

                $question_total = session::get('session_question_total');
                $sisa_bobot      = session::get('session_sisa_bobot');
                session::forget('session_question_total');
                session::forget('session_sisa_bobot');
                $question_total = $question_total-1;
                $min_bobot      = $sisa_bobot - $request->bobot;
                session(['session_akhir_bobot'=>$min_bobot]);
                session(['session_question_total'=>$question_total]);
                return back()->withMessage('Soal Terinput!');
            }else{
                return redirect()->route('qbank.index')->withMessage('Input Soal Sudah Terlengkapi!');
            }
        }else if($question_type == "LISTENING"){
            $question_type  = session::get('session_question_type');
            $question_total = session::get('session_question_total');
            $subject_id     = session::get('session_subject_id');
            $bab_id         = session::get('session_bab_id');
            $difficulty     = session::get('session_difficulty');
            if($question_total >= 1){
                $question = new Question;
                $question->subject_id    = $subject_id;
                $question->bab_id        = $bab_id;
                $question->level         = $difficulty;
                $question->question_type = $question_type;
                $question->question      = $request->question;
                $question->audio         = $request->file('file')->store('paperless/audio');
                $question->answer_a      = $request->j_a;
                $question->answer_b      = $request->j_b;
                $question->answer_c      = $request->j_c;
                $question->answer_d      = $request->j_d;
                $question->answer_e      = $request->j_e;
                $question->true_answer   = $request->true_answer;
                $question->bobot         = 1;
                $question->created_by    = Auth::guard('web_teacher')->user()->id;
                
                $question->save();
                $question_total = session::get('session_question_total');
                session::forget('session_question_total');
                $question_total = $question_total-1;
                session(['session_question_total'=>$question_total]);
                return back()->withMessage('Soal Terinput!');
            }else{
                return redirect()->route('qbank.index')->withMessage('Input Soal Sudah Terlengkapi!');
            }
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\QuestionBank  $questionBank
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QuestionBank $questionBank)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\QuestionBank  $questionBank
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuestionBank $questionBank)
    {
        //
    }
}
