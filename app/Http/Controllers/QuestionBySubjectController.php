<?php

namespace App\Http\Controllers;

use App\QuestionBySubject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuestionBySubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\QuestionBySubject  $questionBySubject
     * @return \Illuminate\Http\Response
     */
    public function show(QuestionBySubject $questionBySubject)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\QuestionBySubject  $questionBySubject
     * @return \Illuminate\Http\Response
     */
    public function edit(QuestionBySubject $questionBySubject)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\QuestionBySubject  $questionBySubject
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QuestionBySubject $questionBySubject)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\QuestionBySubject  $questionBySubject
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuestionBySubject $questionBySubject)
    {
        //
    }
}
