<?php

namespace App\Http\Controllers;

use App\Schedule as Jadwal;
use Illuminate\Http\Request;
use App\Teacher;
use App\Subject;
use App\ClassGroup;
use App\SchoolYear;
use App\Major;
use DB;
class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jadwal = Jadwal::all();
        $jurusan = Major::latest()->get();
        return view('pages.jadwal.index',compact('jadwal','jurusan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teacher = Teacher::latest()->get();
        $class = ClassGroup::where('status','active')->latest()->get();
        $subject = Subject::latest()->get();
        $tahun = SchoolYear::latest()->get();
        return view('pages.jadwal.create',compact('teacher','class','subject','tahun'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'hari'=>'required',
            'jam'=>'required',
            'jam_ke'=>'required',
            'matpel_id'=>'required',
            'teacher_id'=>'required',
            'class_id'=>'required',
            'tahun_id'=>'required',
            ]);

        $jadwal = new Jadwal($request->all());
        $jadwal->save();

        return redirect()->route('schedule.index')->with('message','jadwal berhasil di tambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Jadwal  $jadwal
     * @return \Illuminate\Http\Response
     */
    public function show(Jadwal $jadwal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Jadwal  $jadwal
     * @return \Illuminate\Http\Response
     */
    public function edit(Jadwal $jadwal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Jadwal  $jadwal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Jadwal $jadwal)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Jadwal  $jadwal
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Jadwal::destroy($id);
        return back()->with('message','Jadwal Terhapus');
    }

    public function ajax_jurusan($id){
        $jurusan = [];
        $data = ClassGroup::where('major_id',$id)->where('status','active')->get();
        return $data;
    }

    public function ajax_jadwal_detail($id)
    {
        $senin = Jadwal::where('class_id',$id)
                        ->where('hari','senin')->get();
        $selasa = Jadwal::where('class_id',$id)
                        ->where('hari','selasa')->get();
        $rabu = Jadwal::where('class_id',$id)
                        ->where('hari','rabu')->get();    
        $kamis = Jadwal::where('class_id',$id)
                        ->where('hari','kamis')->get();
        $jumat = Jadwal::where('class_id',$id)
                        ->where('hari','jumat')->get();
        return view('pages.jadwal.load_detail',compact('senin','selasa','rabu','kamis','jumat'));
    }
}
