<?php

namespace App\Http\Controllers;

use App\SchoolYear;
use Illuminate\Http\Request;

class SchoolYearController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = SchoolYear::latest()->get();
        return view('pages.school_year.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.school_year.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'first_year' => 'unique:school_years'
        ]);
        if ( $request->first_year < $request->last_year) {
            SchoolYear::create([
                'first_year' => $request->first_year,
                'last_year' => $request->last_year,
                'status' => $request->status
            ]);

            return redirect()->route('school_year.index')->with('message',' Berhasil Menambahkan');
        } else {
            return redirect()->route('school_year.create')->with('error', 'Tahun Akhir lebih kecil dari Tahun Awal');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SchoolYear  $schoolYear
     * @return \Illuminate\Http\Response
     */
    public function show(SchoolYear $schoolYear)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SchoolYear  $schoolYear
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = SchoolYear::find($id);
        return view('pages.school_year.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SchoolYear  $schoolYear
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        if ($request->first_year < $request->last_year) {
            SchoolYear::where('id', $id)->update([
                'first_year' => $request->first_year,
                'last_year' => $request->last_year,
                'status' => $request->status
            ]);
            return redirect()->route('school_year.index')->with('message', "Berhasil di Ubah");
        } else {
            return redirect()->route('school_year.edit')->with('error', 'Tahun Akhir lebih kecil dari Tahun Awal');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SchoolYear  $schoolYear
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SchoolYear::destroy($id);
        return redirect()->route('school_year.index')->with('message','Tahun Ajar di Hapus');
    }
}
