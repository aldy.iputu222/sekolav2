<?php

namespace App\Http\Controllers;

use Auth;
use App\Score;
use App\Major;
use App\Student;
use App\ClassGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ScoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.score.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jurusan = Major::all();
        return view('pages.score.create',compact('jurusan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(count($request->score) > 0)
        {
            foreach ($request->score as $value) {
                    Score::create([
                        'title' => $request->title,
                        'type' => $request->type,
                        'rombel_id' => $request->rombel_id,
                        'score' => $value,
                        'student_id' => $request->student_id,
                        'teacher_id' => Auth::guard('web_teacher')->user()->id
                    ]);
            }
        }

        return redirect()->route('score.index')->with('message','Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Score  $score
     * @return \Illuminate\Http\Response
     */
    public function show(Score $score)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Score  $score
     * @return \Illuminate\Http\Response
     */
    public function edit(Score $score)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Score  $score
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Score $score)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Score  $score
     * @return \Illuminate\Http\Response
     */
    public function destroy(Score $score)
    {
        //
    }

    public function ajax_rombel($id)
    {
        $jurusan = [];
        $data = ClassGroup::where(['major_id'=>$id,'status'=>'active'])->get();
        return $data;
    }

    public function ajax_siswa($id)
    {
        $siswa =  Student::where(['class_group_id'=>$id])->get();
        $rombel = ClassGroup::where(['id'=>$id])->get();
        $count = Student::where(['class_group_id'=>$id])->count();
        return view('pages.score.load_student',compact('siswa','count','rombel'));
    }
}
