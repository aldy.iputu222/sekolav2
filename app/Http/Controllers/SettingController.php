<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Setting::count() > 0)
        {
            return view('pages.setting.index',['data'=>Setting::latest()->first()]);
        }else{
            return redirect()->route('setting.create');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Setting::count() > 0)
        {
            return redirect()->route('setting.index');
        }else{
            return view('pages.setting.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'   =>'required|string',
            'logo'   =>'required|file|mimes:jpg,png',
            'address'=>'required',
            'about'  =>'required',
            'grade'  =>'required',
            'phone'  =>'required',
        ]);

        $setting       = new Setting($request->except('logo'));
        $setting->logo = $request->file('logo')->store('setting');
        $setting->save();

        return redirect()->route('setting.index')->with('message','Mengisi data');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show(Setting $setting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        return view('pages.setting.edit',['data'=>$setting]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Setting $setting)
    {
        $this->validate($request,[
            'name'   =>'required|string',
            'logo'   =>'file|mimes:jpg,png',
            'address'=>'required',
            'about'  =>'required',
            'grade'  =>'required',
            'phone'  =>'required',
        ]);
        
        $setting       = Setting::find($setting->id);

        if($request->hasFile('logo'))
        {
            $setting->logo = $request->file('logo')->store('setting');
            $setting->save();
        }else{
            $setting->update($request->except('_token','_method','logo'));
        }

        return redirect()->route('setting.index')->with('message','Mengubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Setting $setting)
    {
        //
    }
}
