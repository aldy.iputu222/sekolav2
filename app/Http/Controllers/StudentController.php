<?php

namespace App\Http\Controllers;

use App\Student;
use App\ClassGroup;
use Illuminate\Http\Request;
use DNS1D;
use DNS2D;
use Storage;
use Hash;
use App\SchoolYear;
class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.student.index',['data'=>Student::latest()->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.student.create',[
            'school_year'=>SchoolYear::where('status','active')->latest()->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nis'=>'required|unique:students',
            'name'=>'required|string',
            'school_year_id'=>'required',
            'address'=>'required',
            'date_of_birth'=>'required|date',
            'gender'=>'required',
            'phone'=>'required',
            'class_group_id'=>'required',
            'photo'=>'required',
            'region'=>'required', 
        ]);

        $student = new Student($request->all());
        $student->photo    = $request->file('photo')->store('student');
        $student->qrcode   = "student-".$request->nis.".jpg";
        $student->barcode  = "student-".$request->nis.".jpg"; 
        $student->password = Hash::make($request->nis);
        $student->save();

        Storage::disk('local_bar')
        ->put("student-".$student->nis.".jpg",base64_decode(DNS1D::getBarcodePNG("student-".$student->nis, "C39+")));
        Storage::disk('local_qr')
        ->put("student-".$student->nis.".jpg",base64_decode(DNS2D::getBarcodePNG("student-".$student->nis, "QRCODE",10,10)));

        return redirect()->route('student.index')->with('message','Menambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        return view('pages.student.show',['data'=>$student]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        return view('pages.student.edit',[
            'data'=>$student,
            'school_year'=>SchoolYear::where('status','active')->latest()->get(),
            'classgroup'=>ClassGroup::latest()->get()
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        $this->validate($request,[
            'name'=>'required|string',
            'school_year_id'=>'required',
            'address'=>'required',
            'date_of_birth'=>'required|date',
            'gender'=>'required',
            'phone'=>'required',
            'class_group_id'=>'required',
            'photo'=>'file',
            'region'=>'required', 
        ]);

        $student_update = Student::find($student->id);
        if($request->hasFile('photo')){
            $student_update->photo    = $request->file('photo')->store('student');
            $student_update->save();
        }else{
            $student_update->update($request->except('_token','_method','photo'));
        }

        return redirect()->route('student.index')->with('message','Mengubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        unlink(public_path('app/barcode/').$student->id.".jpg");
        unlink(public_path('app/qrcode/').$student->id.".jpg");
        
        if($student->photo != "student/default.png")
        {
            unlink(public_path('app/').$student->photo);
        }

        $student->delete();
        return redirect()->route('student.index')->with('message','Menghapus');
    }

    public function dashboard()
    {
        return view('pages.student.dashboard');
    }
}
