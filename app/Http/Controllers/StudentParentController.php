<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StudentParent;
use Hash;
class StudentParentController extends Controller
{
    public function register(Request $request)
    {
        $check = StudentParent::where('email',$request->email)->count();
        if($check > 0)
        {
            return response([
                'data'  =>StudentParent::where('email',$request->email)
                ->with('parent_relate')->first(),
                'status'=>'success',
            ]);   
        }else{
            $parent = new StudentParent();
            $parent->fill($request->all());
            $parent->password = Hash::make($request->email);
            $parent->save();

            return response([
                'data'  =>$parent->with('parent_relate'),
                'status'=>'success',
            ]);
        }
    }
    
}
