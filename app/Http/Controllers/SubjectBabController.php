<?php

namespace App\Http\Controllers;

use App\SubjectBab;
use App\Bab;
use App\Subject;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubjectBabController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.subjectbab.index',[
            'subjectbab' =>SubjectBab::all(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.subjectbab.create',[
            'subject'=>Subject::all(),
        ]);
    }

    public function storeBab(){
        $jumlah    = session::get('session_jumlah');
        return view('pages.subjectbab.createbab',[
            'jumlah'=>$jumlah,
        ]);
    }

    public function insertbab(Request $request){
        $data = $request->all();
        $subjectid = session::get('session_subjectid');
        foreach ($data['bab'] as $key => $value) {
            $bab = Bab::create([
                'subject_id' => $subjectid,
                'bab' => $value,
            ]);
        }
        return redirect()->route('subjectbab.index')->withMessage("Bab Berhasil ditambahkan");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'subject_id'=>'required',
            'total'=>'required',
        ]);

        $do = new SubjectBab($request->all());
        $do->save();
        $jumlah      = $request->total;
        $subject_id  = $request->subject_id;
        session(['session_jumlah'=>$jumlah]);
        session(['session_subjectid'=>$subject_id]);
        return redirect()->route('teacher.storebab')->withMessage("Bab Berhasil ditambahkan");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubjectBab  $subjectBab
     * @return \Illuminate\Http\Response
     */
    public function show(SubjectBab $subjectBab,$id)
    {
        $data  = SubjectBab::where(['id'=>$id])->first();
        $babb  = Bab::where(['subject_id'=>$id])->get();
        return view('pages.subjectbab.show',[
            'data'=>$data,
            'bab'=>$babb,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubjectBab  $subjectBab
     * @return \Illuminate\Http\Response
     */
    public function edit(SubjectBab $subjectBab,$id)
    {
        $data = SubjectBab::Where(['id'=>$id])->first();
        return view('pages.subjectbab.edit',[
            'data'=>$data,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubjectBab  $subjectBab
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubjectBab $subjectBab)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubjectBab  $subjectBab
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubjectBab $subjectBab,$id)
    {
        SubjectBab::where(['id'=>$id])->delete();
        Bab::where(['subject_id'=>$id])->delete();
        return redirect()->route('subjectbab.index')->with('message','Menghapus');
    }
}
