<?php

namespace App\Http\Controllers;

use App\TaskCollection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TaskCollectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TaskCollection  $taskCollection
     * @return \Illuminate\Http\Response
     */
    public function show(TaskCollection $taskCollection)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TaskCollection  $taskCollection
     * @return \Illuminate\Http\Response
     */
    public function edit(TaskCollection $taskCollection)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TaskCollection  $taskCollection
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TaskCollection $taskCollection)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TaskCollection  $taskCollection
     * @return \Illuminate\Http\Response
     */
    public function destroy(TaskCollection $taskCollection)
    {
        //
    }
}
