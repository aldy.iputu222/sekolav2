<?php

namespace App\Http\Controllers;

use App\Task;
use App\TaskFilter;
use App\Teacher;
use App\SchoolYear;
use App\ClassGroup;
use Auth;
use Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.learning.task.index',[
            'data' => Task::latest()->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = SchoolYear::where('status','active')->latest()->get();
        $array = [];
        foreach($data as $val)
        {
            foreach(ClassGroup::where(['school_year_id'=>$val->id,'status'=>'active'])->with('school_year')->latest()->get() as $field)
            {
                $array[] = $field;
            }
        }
        return view('pages.learning.task.create',[
            'classgroup' => $array
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|string',
            'content' => 'required',
            'file' => 'mimes:jpg,png,jpeg,docx,doc,xls,ppt,pdf|required',
            'deadline_date' => 'required',
        ]);

        $task = new Task($request->all());
        $task->teacher_id = Auth::guard('web_teacher')->user()->id;
        $task->file = $request->file('file')->store('e_learning/task');
        $task->save();
        
        $id = $task->id;

        if ($request->type == "class") 
        {
            if(count($request->class_group_filter) > 0)
            {
                foreach ($request->class_group_filter as $value) {
                    TaskFilter::create([
                        'classgroup_id' => $value,
                        'task_id' => $id,
                        'teacher_id' => Auth::guard('web_teacher')->user()->id,
                        'type' => $request->type
                    ]);
                }
            }
        } 
        else 
        {
            TaskFilter::create([
                'task_id' => $id,
                'teacher_id' => Auth::guard('web_teacher')->user()->id,
                'type' => $request->type
            ]);
        }
        
        return redirect()->route('task.index')->with('message','Succes');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        return view('pages.learning.task.detail',[
            'show' => $task,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        if ($task->task_filter[0]->type == "all") {
            $before = ClassGroup::where('status','active')->get();
        } else {
            $array = [];
            foreach ($task->task_filter as $value) 
            {
                $array[] = $value->classgroup_id;
            }
            $class = ClassGroup::where('status','active')->whereIn('id',$array)->latest()->get();
            $class_not = ClassGroup::where('status','active')->whereNotIn('id',$array)->latest()->get();

            $before = [];

            foreach ($class as $value) {
                $value['is_selected'] = "selected";
                $before[] = $value;
            }

            foreach ($class_not as $value) {
                $value['is_selected'] = "";
                $before[] = $value;
            }
        }
        return view('pages.learning.task.edit',[
            'data' => $task,
            'classgroup' => $before
        ]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        if($request->hasFile('file'))
        {
            $update = Task::find($task->id);
            Storage::delete($update->file);
            $update->file = $request->file('file')->store('e_learning/task');
            $update->title = $request->title;
            $update->content = $request->content;
            $update->deadline_date = $request->deadline_date;
            $update->with_task_collection = $request->with_task_collection;
            $update->save();
        }
        else
        {
            Task::find($task->id)->update($request->except('file','_token','_method'));
        }

        TaskFilter::where('task_id',$task->id)->delete();

        if ($request->type == "class") 
        {
            if(count($request->class_group_filter) > 0)
            {
                foreach ($request->class_group_filter as $value) {
                    TaskFilter::create([
                        'classgroup_id' => $value,
                        'task_id' => $task->id,
                        'teacher_id' => Auth::guard('web_teacher')->user()->id,
                        'type' => $request->type
                    ]);
                }
            }
        } 
        else 
        {
            TaskFilter::create([
                'task_id' => $task->id,
                'teacher_id' => Auth::guard('web_teacher')->user()->id,
                'type' => $request->type
            ]);
        }

        return redirect()->route('task.index')->with('message','Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        $task->delete();
        Storage::delete($task->file);
        TaskFilter::where('task_id',$task->id)->delete();

        return back()->with('message','Success');
    }

    public function student_task()
    {
        $classgroup = Auth::guard('web_student')->user()->class_group_id;
        
        $task_filter = TaskFilter::where('classgroup_id',$classgroup)->orWhere('type','all')->with('task')->latest()->paginate(5);
        $count = TaskFilter::all();
        $i=0;
       foreach($task_filter as $val){
           $ext[$i] = [
                'task_id'=>$val->task_id,
           ];
           $i++;
       }
       for($i=0; $i < count($task_filter); $i++){
            $tes[$i] = Task::where(['id'=>$ext[$i]['task_id']])->latest()->get();
        }

        for($i=0; $i < count($task_filter); $i++){
            $id[] = $tes[$i][0]->file;
        }

        for($i=0; $i<count($task_filter); $i++){
            $extension = $id[$i];
            $tmp       = explode('.',$extension);
            $exten[]       = end($tmp);
        }
        return view('pages.learning.task.student_task',[
            'data' => $task_filter,
            'ext'=> @$exten,
            'count' => $count
        ]);
    }
}
