<?php

namespace App\Http\Controllers;

use App\TaskFilter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TaskFilterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TaskFilter  $taskFilter
     * @return \Illuminate\Http\Response
     */
    public function show(TaskFilter $taskFilter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TaskFilter  $taskFilter
     * @return \Illuminate\Http\Response
     */
    public function edit(TaskFilter $taskFilter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TaskFilter  $taskFilter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TaskFilter $taskFilter)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TaskFilter  $taskFilter
     * @return \Illuminate\Http\Response
     */
    public function destroy(TaskFilter $taskFilter)
    {
        //
    }
}
