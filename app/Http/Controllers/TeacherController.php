<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Teacher;
use App\Subject;
use Storage;
use Hash;
use DNS1D;
use DNS2D;
use Auth;
class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $teacher = Teacher::all(); 
        return view('pages.teacher.index', ['teacher'=>Teacher::latest()->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.teacher.create',[
            'subject'=>Subject::latest()->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nip' => 'required|numeric|unique:teachers',
            'name' => 'required|string',
            'email' => 'required|email',
            'photo' => 'required|file|mimes:jpg,png,jpeg',
            'address' => 'required',
            'date_of_birth' => 'required',
            'gender' => 'required',
            'phone' => 'required|numeric',
            'subject_id' => 'required',
        ]);

        $teacher = new Teacher($request->all());
        $teacher->photo    = $request->file('photo')->store('teacher');
        $teacher->qrcode   = "teacher-".$request->nip.".jpg";
        $teacher->barcode  = "teacher-".$request->nip.".jpg";
        $teacher->password = Hash::make($request->nip);
        $teacher->save();

        Storage::disk('local_bar')->put("teacher-".$request->nip.'.jpg',base64_decode(DNS1D::getBarcodePNG($request->nip, "C39+")));
        Storage::disk('local_qr')->put("teacher-".$request->nip.'.jpg',base64_decode(DNS2D::getBarcodePNG($request->nip, "QRCODE",10,10)));

        return redirect()->route('teacher.index')->with('message','Guru Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function show(Teacher $teacher)
    {
        return view('pages.teacher.show',[
            'data'=>$teacher,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function edit(Teacher $teacher)
    {
        return view('pages.teacher.edit', [
            'data'=>$teacher,
            'subject' => Subject::all(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Teacher $teacher)
    {
        $this->validate($request, [
            'nip' => 'numeric|unique:teachers',
            'name' => 'required|string',
            'photo' => 'file|mimes:jpg,png,jpeg',
            'address' => 'required',
            'date_of_birth' => 'required',
            'gender' => 'required',
            'phone' => 'required|numeric',
        ]);
        
        if ($request->hasFile('photo')) 
        {
            $object = Teacher::where('id',$teacher->id);
            Storage::delete($object->photo);
            $object->photo = $request->file('photo')->store('teacher');
            $object->save();
        } else {
            Teacher::find($teacher->id)->update($request->except('nip','photo','_token','_method'));
        }

        return redirect()->route('teacher.index')->with('message','Update Success');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Teacher $teacher)
    {
        // $cek = Teacher::find($teacher->id);
        // if($cek){
        //     Storage::delete($cek->photo);
        //     $cek->delete();
        //     return back()->withMessage('Berhasil Hapus');
        // }
        unlink(public_path('app/barcode/')."teacher"."-".$teacher->nip.".jpg");
        unlink(public_path('app/qrcode/')."teacher"."-".$teacher->nip.".jpg");
        unlink(public_path('app/').$teacher->photo);
        $teacher->delete();
        return redirect()->route('teacher.index')->with('message','Menghapus');
    }
}
