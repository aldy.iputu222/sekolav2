<?php

namespace App\Http\Controllers;

use App\TemporaryBorrow;
use Illuminate\Http\Request;

class TemporaryBorrowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TemporaryBorrow  $temporaryBorrow
     * @return \Illuminate\Http\Response
     */
    public function show(TemporaryBorrow $temporaryBorrow)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TemporaryBorrow  $temporaryBorrow
     * @return \Illuminate\Http\Response
     */
    public function edit(TemporaryBorrow $temporaryBorrow)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TemporaryBorrow  $temporaryBorrow
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TemporaryBorrow $temporaryBorrow)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TemporaryBorrow  $temporaryBorrow
     * @return \Illuminate\Http\Response
     */
    public function destroy(TemporaryBorrow $temporaryBorrow)
    {
        //
    }
}
