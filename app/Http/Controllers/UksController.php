<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Uks;
use App\ClassGroup;
use App\Student;
use App\SchoolYear;

class UksController extends Controller
{
    public function index()
    {
        $data = Uks::latest()->get();
        return view('pages.uks.index',compact('data'));
    }

    
    public function create()
    {
        return view('pages.uks.create',[
            'school_year'=>SchoolYear::where('status','active')->latest()->get(),
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'student_id'=>'required',
            'description'=>'required|String'
        ]);
        Uks::create($request->all());
    
        return back()->with('message','Success');
    }

    public function show(ClassGroup $data)
    {
        return view('pages.uks.show', [
            'data'=>$data
        ]);
    }

    public function edit(Uks $value)
    {
        
    }

    public function update(Request $request, Uks $value)
    {
        
    }

    public function destroy($id)
    {
        Uks::destroy($id);

        return back()->with('message','Success');
    }
}

