<?php

namespace App\Http\Controllers;

use App\Video;
use App\categoryType;
use Illuminate\Http\Request;
use Storage;
use App\Http\Controllers\Controller;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.web-profile.video.index',['video'=>Video::latest()->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.web-profile.video.create',['category'=>categoryType::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|string',
            'content' => 'required|string',
            'image' => 'required|mimes:jpg,png,jpeg',
            'type' => 'required',
            'video' => 'required',
        ]);



        if ($request->type == "embed") {
            $file = new Video($request->all());
            $file->image = $request->file('image')->store('web_profile/photo');
            $file->save();   
        } else {
            $file = new Video($request->all());
            $file->image = $request->file('image')->store('web_profile/photo');
            $file->video = $request->file('video')->store('web_profile/video');    
            $file->save();
        }

        return redirect()->route('video.index')->with('message', 'Video berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show(Video $video)
    {
        return view('pages.web-profile.video.show',['video'=>$video]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        return view('pages.web-profile.video.edit',[
            'edit'=>$video,
            'kategori'=>categoryType::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Video $video)
    {
        $this->validate($request,[
            'title' => 'required|string',
            'content' => 'required|string',
            'type' => 'required',
            'video' => 'required',
        ]);

        if ($request->hasFile('photo') && $request->hasFile('video')) {
            $object = Video::find($video->id);
            Storage::delete($object->image);
            Storage::delete($object->video);
            $object->image = $request->file('photo')->store('web_profile/photo');
            $object->video = $request->file('video')->store('web_profile/video');
            $object->save();
        } else {
            Video::find($video->id)->update($request->except('video','photo','_token','_method'));
        }

        return redirect()->route('video.index')->with('message','Update Success');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy(Video $video)
    {
        if ($video->type == "local") {
            unlink(public_path('app/').$video->image);
            unlink(public_path('app/').$video->video);
            $video->delete();
        } else {
            unlink(public_path('app/').$video->image);
            $video->delete();
        }
        return redirect()->route('video.index')->with('message','Menghapus');
    }
}
