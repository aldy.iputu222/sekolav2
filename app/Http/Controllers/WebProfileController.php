<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Banner;
use App\Eskul;

class WebProfileController extends Controller
{
    public function home(){

    	return view('templates.web-profile.main', [
    		'eskul' => Eskul::all(),
            'banner' => Banner::all(),
        ]);
    }
}
