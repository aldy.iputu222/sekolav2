<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class TeacherMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guard('web_teacher')->check()){
            return $next($request);
        }else{
            return redirect('/home');
        }
    }
}
