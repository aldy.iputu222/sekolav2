<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    public function send($topics,$message,$title)
    {
        $API_ACCESS_KEY = "AAAAs9TFJZs:APA91bGGJ6m6XrJuhQU0kGdfYo89BDH8lltfDAyKnoiTVPojxACj-ljbRCFoxWtiCevbSm_fDH1ONpK3mHjWm4YdFE6-CKz3E7QfRqI3ZSCYStyoTCbIG0MzXqKH9XuT5QBnnfb4rgNK";

        $fields = [
            'to'    => "/topics/".$topics,
            'data'  => [
                'message'   => $title,
                'title'     => $message,
            ]
        ];
        
        
        $headers = [
            'Authorization: key=' .$API_ACCESS_KEY,
            'Content-Type: application/json'
        ];
            
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch);
        curl_close( $ch );
    }
}
