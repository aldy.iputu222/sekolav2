<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParentRelate extends Model
{
    protected $fillable = [
        'student_id',
        'parent_id'
    ];

    public function student()
    {
        return $this->hasOne('App\Student','id','student_id')->with('classgroup');
    }

    public function parent()
    {
        return $this->hasOne('App\StudentParent','id','parent_id');
    }
}
