<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'questions';
    protected $fillable = [
        'subject_id',
        'bab_id',
        'level',
        'question_type',
        'question',
        'audio',
        'answer_a',
        'answer_b',
        'answer_c',
        'answer_d',
        'answer_e',
        'true_answer',
        'bobot',
        'created_by'
    ];
}
