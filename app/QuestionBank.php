<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionBank extends Model
{
    protected $table = 'question_banks';
    protected $fillable = ['subject_id','bab_id','difficulty','question_type','question_total','created_by'];

    public function subject()
    {
        return $this->hasOne('App\Subject','id','subject_id');
    }

    public function bab()
    {
        return $this->hasOne('App\Bab','id','bab_id');
    }
}
