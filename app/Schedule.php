<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $fillable = [
    	'hari',
    	'jam',
    	'jam_ke',
    	'teacher_id',
    	'class_id',
    	'matpel_id',
        'tahun_id'
    ];

    public function Rombel(){
    	return $this->hasOne('App\ClassGroup','id','class_id');
    }

    public function Teacher(){
    	return $this->hasOne('App\Teacher','id','teacher_id');
    }

    public function Subject(){
    	return $this->hasOne('App\Subject','id','matpel_id');
    }

    public function Tahun(){
        return $this->hasOne('App\SchoolYear','id','tahun_id');
    }
}
