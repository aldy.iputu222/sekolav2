<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolYear extends Model
{
    protected $table = 'school_years';
    protected $fillable = ['first_year','last_year','status'];

    public function classgroup()
    {
        return $this->hasMany('App\ClassGroup','school_year_id','id');
    }
}
