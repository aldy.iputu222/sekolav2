<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    protected $fillable = [
        'title', 'type', 'rombel_id', 'teacher_id', 'score'
    ];
}
