<?php

namespace App;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;

class Student extends Model implements AuthenticatableContract
{
    use Authenticatable;

    protected $fillable = [
        'nis',
        'name',
        'school_year_id',
        'address',
        'date_of_birth',
        'gender',
        'phone',
        'class_group_id',
        'photo',
        'qrcode',
        'barcode',
        'region',
        'password'
    ];

    protected $hidden = [
        'remember_token','created_at','updated_at'
    ];

    public function classgroup()
    {
        return $this->hasOne('App\ClassGroup','id','class_group_id');
    }

    public function school_year()
    {
        return $this->hasOne('App\SchoolYear','id','school_year_id');
    }

    public function absents()
    {
        # code...
    }

    public function absent_today()
    {
        return $this->hasOne('App\Absent','student_id','id')->whereRaw('DATE(created_at) = CURDATE()');
    }
    public function task()
    {
        return $this->hasMany('App\Task');
    }
}
