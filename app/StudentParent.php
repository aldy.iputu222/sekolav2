<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentParent extends Model
{
    protected $fillable = ['name','email','photo','password'];

    public function parent_relate()
    {
        return $this->hasMany('App\ParentRelate','parent_id','id')->with('student');
    }
}
