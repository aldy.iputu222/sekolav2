<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubjectBab extends Model
{
    protected $table = 'subject_babs';
    protected $fillable = ['subject_id','total'];

    public function subject()
    {
        return $this->hasOne('App\Subject','id','subject_id');
    }
}

