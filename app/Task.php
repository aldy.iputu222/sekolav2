<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
    	'teacher_id', 'title', 'content', 'file', 'deadline_date', 'with_task_collection'
    ];

    public function teacher()
    {
    	return $this->hasOne('App\Teacher', 'id', 'teacher_id');
    }

    public function task_filter()
    {
        return $this->hasMany('App\TaskFilter');
    }
}
