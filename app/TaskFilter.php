<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskFilter extends Model
{
    protected $fillable = [
        'task_id', 'classgroup_id', 'teacher_id', 'type'
    ];

    public function task()
    {
        return $this->hasOne('App\Task', 'id', 'task_id');
    }

    public function classgroup()
    {
        return $this->hasOne('App\Classgroup', 'id', 'classgroup_id');
    }

    public function teacher()
    {
        return $this->hasOne('App\Teacher', 'id', 'teacher_id');
    }
}
