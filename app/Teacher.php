<?php

namespace App;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;

class Teacher extends Model implements AuthenticatableContract
{
	use Authenticatable;

    protected $fillable = [
    	'nip',
    	'name',
    	'email',
    	'photo',
    	'address',
    	'date_of_birth',
    	'gender',
    	'phone',
    	'subject_id',
    	'qrcode',
        'barcode'
	];
	
	protected $hidden = [
		'qrcode','password','barcode','remember_token'
	];

    public function subject()
    {
    	return $this->hasOne('App\Subject','id','subject_id');
    }

}
