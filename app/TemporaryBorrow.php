<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TemporaryBorrow extends Model
{
    protected $fillable = ['book_single_id','user_id','student_id','borrow_at','status'];

    public function book_single()
    {
        return $this->hasOne('App\BookSingle','id','book_single_id')->with('book');
    }
}
