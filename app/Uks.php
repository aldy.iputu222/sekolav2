<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Uks extends Model
{
    protected $fillable = ['student_id', 'description'];

    public function student()
    {
        return $this->hasOne('App\Student','id','student_id');
    }
}
