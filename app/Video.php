<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = [
    	'title', 
    	'content', 
    	'image', 
    	'type', 
    	'video', 
    	'category_content_id',
    ];

    public function category()
    {
    	return $this->hasOne('App\categoryType', 'id', 'category_content_id');
    }
}
