<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class categoryType extends Model
{
    protected $fillable = ['name'];
}
