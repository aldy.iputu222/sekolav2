<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nip');
            $table->string('name');
            $table->string('email');
            $table->string('photo')->nullable();
            $table->text('address');
            $table->date('date_of_birth');
            $table->enum('gender',['L','P']);
            $table->string('phone');
            $table->unsignedInteger('subject_id');
            $table->string('qrcode');
            $table->string('barcode');
            $table->string('password');
            $table->string('remember_token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers');
    }
}
