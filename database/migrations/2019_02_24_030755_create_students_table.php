<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nis');
            $table->string('name');
            $table->UnsignedInteger('school_year_id');
            $table->text('address');
            $table->date('date_of_birth');
            $table->enum('gender',['P','L']);
            $table->string('phone');
            $table->UnsignedInteger('class_group_id');
            $table->string('photo');
            $table->string('qrcode');
            $table->string('barcode');
            $table->enum('region',['islam','kristen','hindu','buddha']);
            $table->string('password');
            $table->string('remember_token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
