
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookSinglesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_singles', function (Blueprint $table) {
            $table->increments('id');
            $table->UnsignedInteger('book_id');
            $table->string('qrcode')->nullable();
            $table->string('barcode')->nullable();
            $table->enum('status',['available','lost','borrow']);
            $table->date('date_in');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_singles');
    }
}
