<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttentionFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attention_filters', function (Blueprint $table) {
            $table->increments('id');
            $table->UnsignedInteger('attention_id');
            $table->UnsignedInteger('class_group_id')->nullable();
            $table->enum('type',['class','all']);
            $table->enum('for_who',['parent','student','all']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attention_filters');
    }
}
