<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbsentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('absents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id')->length('11')->nullable();
            $table->integer('teacher_id')->length('11')->nullable();
            $table->integer('employee_id')->length('11')->nullable();
            $table->enum('status',['A','I','H','S']);
            $table->enum('type',['Manual','Machine']);
            $table->enum('for_who',['student','teacher','employee']);
            $table->date('date');
            $table->time('hours_come')->nullable();
            $table->time('hours_home')->nullable();
            $table->enum('status_come',['on_time','late'])->default('on_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('absents');
    }
}
