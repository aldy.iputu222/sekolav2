<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamManagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_manages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('exam_type_id');
            $table->integer('subject_id');
            $table->string('major_id');
            $table->string('grade');
            $table->integer('kkm');
            $table->integer('time');
            $table->text('description')->nullable();
            $table->integer('created_by');
            $table->string('token');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam_manages');
    }
}
