<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePollingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pollings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('attention_id')->unsigned();
            $table->integer('student_id')->unsigned()->nullable();
            $table->integer('studentparent_id')->unsigned()->nullable();
            $table->enum('for_who',['student','parent']);
            $table->enum('choice',['yes','no']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pollings');
    }
}
