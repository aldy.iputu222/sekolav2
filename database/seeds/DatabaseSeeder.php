<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\SchoolYear;
use App\Subject;
use App\Major;
use App\Teacher;
use App\Student;
use App\ClassGroup;
use Faker\Factory;
use App\Schedule;
use App\GuestBook;
use DNS1D as D1D;
use DNS2D AS D2D;
use Storage as ST;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create('id_ID');

        User::create([
            'name'=>'Administrator',
            'email'=>'admin@gmail.com',
            'password'=>Hash::make("admin123"),
            'level'=>'admin',
        ]);

        Subject::create([
            'name' =>'Matematika',
            'alias'=>'MTK',
            'kkm'  =>'75',
        ]);

        Subject::create([
            'name' =>'Sejarah',
            'alias'=>'SI',
            'kkm'  =>'75',
        ]);

        Subject::create([
            'name' =>'Pendidikan Kewarganegaraan',
            'alias'=>'PKN',
            'kkm'  =>'75',
        ]);

        Subject::create([
            'name' =>'Bahasa Inggris',
            'alias'=>'BING',
            'kkm'  =>'75',
        ]);

        Subject::create([
            'name' =>'Bahasa Indonesia',
            'alias'=>'BINDO',
            'kkm'  =>'75',
        ]);

        Subject::create([
            'name' =>'Bahasa Sunda',
            'alias'=>'BS',
            'kkm'  =>'75',
        ]);

        Subject::create([
            'name' =>'Fisika TI',
            'alias'=>'FTI',
            'kkm'  =>'75',
        ]);

        Subject::create([
            'name' =>'Kewirausahaan',
            'alias'=>'KWH',
            'kkm'  =>'75',
        ]);

        Subject::create([
            'name' =>'Pendidikan Jasmani dan Budi Pekerti',
            'alias'=>'PJOK',
            'kkm'  =>'75',
        ]);

        Subject::create([
            'name' =>'Pemograman Dasar',
            'alias'=>'PEMDAS',
            'kkm'  =>'75',
        ]);

        Major::create([
            'name' =>'Rekayasa Perangkat Lunak',
            'alias'=>'RPL',
        ]);

        Major::create([
            'name' =>'Multimedia',
            'alias'=>'MMD',
        ]);

        Major::create([
            'name' =>'Teknik Komputer dan Jaringan',
            'alias'=>'TKJ',
        ]);

        Major::create([
            'name' =>'Administrasi dan Perkatoran',
            'alias'=>'APK',
        ]);

        Major::create([
            'name' =>'Pemasaran',
            'alias'=>'PMN',
        ]);

        Major::create([
            'name' =>'Perhotelan',
            'alias'=>'PH',
        ]);

        Major::create([
            'name' =>'Tataboga',
            'alias'=>'TBG',
        ]);

        Major::create([
            'name' =>'Ilmu Pengetahuan Alam',
            'alias'=>'IPA',
        ]);

        Major::create([
            'name' =>'Ilmu Pengetahuan Sosial',
            'alias'=>'IPS',
        ]);

        for($i = 2000;$i <= 2015;$i++)
        {
            SchoolYear::create([
                'first_year'=>$i + 1,
                'last_year'=>$i + 3,
                'status'    =>'active',
            ]);
        }

        for($i = 1;$i <= 10;$i++)
        {
            $teacher = Teacher::create([
                'nip'=>'112233'.$i,
                'name'=>$faker->name,
                'email'=>'teacher_'.$i.'@gmail.com',
                'photo'=>'teacher/default.png',
                'address'=>$faker->address,
                'date_of_birth'=>$faker->dateTimeThisCentury->format('Y-m-d'),
                'gender'=>"L",
                'phone'=>"0897122585".$i,
                'subject_id'=>$i,
                'password'=>Hash::make('112233'.$i),
                'qrcode'=>"teacher-".'112233'.$i.'.jpg',
                'barcode'=>"teacher-".'112233'.$i.'.jpg',
            ]);
            

            ST::disk('local_bar')->put("teacher-".$teacher->nip.'.jpg',base64_decode(D1D::getBarcodePNG($teacher->nip, "C39+")));
            ST::disk('local_qr')->put("teacher-".$teacher->nip.'.jpg',base64_decode(D2D::getBarcodePNG($teacher->nip, "QRCODE",10,10)));
        }

        for($i = 1;$i<=40;$i++)
        {
            $student = Student::create([
                'nis'=>'116055'.$i,
                'name'=>$faker->name,
                'photo'=>'student/default.png',
                'address'=>$faker->address,
                'date_of_birth'=>$faker->dateTimeThisCentury->format('Y-m-d'),
                'gender'=>"L",
                'phone'=>"0897122585".$i,
                'password'=>Hash::make('116055'.$i),
                'qrcode'=>"teacher-".'116055'.$i.'.jpg',
                'barcode'=>"teacher-".'116055'.$i.'.jpg',
                'school_year_id'=>'1',
                'class_group_id'=>'1',
            ]);

            ST::disk('local_bar')->put("student-".$student->nis.'.jpg',base64_decode(D1D::getBarcodePNG($student->nis, "C39+")));
            ST::disk('local_qr')->put("student-".$student->nis.'.jpg',base64_decode(D2D::getBarcodePNG($student->nis, "QRCODE",10,10)));
        }

        for($i = 1;$i <= 1;$i++)
        {
            $major  = Major::where('id', 8)->first();
            $array  = ['X','XI','XII'];
            foreach($array as $value)
            {
                // dd($value);
                $className = $major->alias . ' ' . $value . '-' . $i;
                if($value != "X")
                {
                    // return $value;
                    ClassGroup::create([
                        'name'     => $className,
                        'grade'    => $value,
                        'major_id' => $major->id,
                        'school_year_id'=>1,
                        'status'   => 'not-active'
                    ]);
                }else{
                    ClassGroup::create([
                        'name'     => $className,
                        'grade'    => $value,
                        'major_id' => $major->id,
                        'school_year_id'=>1,
                        'status'   => 'active'
                    ]);
                }
            }
        }

        Schedule::create([
            'hari'=>'senin',
            'jam'=>'7.30',
            'jam_ke'=>1,
            'teacher_id'=>1,
            'class_id'=>1,
            'matpel_id'=>1,
        ]);

        Schedule::create([
            'hari'=>'senin',
            'jam'=>'9.30',
            'jam_ke'=>2,
            'teacher_id'=>2,
            'class_id'=>1,
            'matpel_id'=>2,
        ]);

        Schedule::create([
            'hari'=>'senin',
            'jam'=>'10.10',
            'jam_ke'=>3,
            'teacher_id'=>4,
            'class_id'=>1,
            'matpel_id'=>4,
        ]);

        Schedule::create([
            'hari'=>'senin',
            'jam'=>'12.30',
            'jam_ke'=>4,
            'teacher_id'=>5,
            'class_id'=>1,
            'matpel_id'=>5,
        ]);

        Schedule::create([
            'hari'=>'senin',
            'jam'=>'14.30',
            'jam_ke'=>5,
            'teacher_id'=>6,
            'class_id'=>1,
            'matpel_id'=>6,
        ]);

        //

        Schedule::create([
            'hari'=>'selasa',
            'jam'=>'7.30',
            'jam_ke'=>1,
            'teacher_id'=>8,
            'class_id'=>1,
            'matpel_id'=>8,
        ]);

        Schedule::create([
            'hari'=>'selasa',
            'jam'=>'9.30',
            'jam_ke'=>2,
            'teacher_id'=>9,
            'class_id'=>1,
            'matpel_id'=>9,
        ]);

        Schedule::create([
            'hari'=>'selasa',
            'jam'=>'10.10',
            'jam_ke'=>3,
            'teacher_id'=>1,
            'class_id'=>1,
            'matpel_id'=>1,
        ]);

        Schedule::create([
            'hari'=>'selasa',
            'jam'=>'12.30',
            'jam_ke'=>4,
            'teacher_id'=>10,
            'class_id'=>1,
            'matpel_id'=>10,
        ]);

        Schedule::create([
            'hari'=>'selasa',
            'jam'=>'14.30',
            'jam_ke'=>5,
            'teacher_id'=>2,
            'class_id'=>1,
            'matpel_id'=>2,
        ]);

        for($i = 1;$i <= 50;$i++)
        {
            GuestBook::create([
                'name'=>$faker->name,
                'address'=>$faker->address,
                'requirement'=>$faker->text,
                'visit_as'=>'parent',
                'phone'=>'089712258'.$i,
            ]);
        }
        
    }
}
