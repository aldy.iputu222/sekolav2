<form action="" method="POST" id="delete-form">
        @method('DELETE')
        @csrf
</form> 
<script>
    $(document).ready(function(){
        
        $('table').on('click','.delete-btn', function(e){
            e.preventDefault();
            var href = $(this).attr('href');
            swal({ title: "Hapus data ?", text: "Data yang di hapus tidak dapat di kembalikan!", type: "warning", showCancelButton: !0, confirmButtonText: "Ya, Hapus!" })
            .then((willDelete) => {
                console.log(willDelete)
              if (willDelete.value === true) {
                console.log("test");
                $('#delete-form').attr('action',href).submit();
              }
            });
        });

    });
</script>
@if(session('message'))
<script>
    toastr.success('{{ session("message") }}');
</script>
@elseif(session('error'))
<script>
    toastr.error('{{ session("error") }}');
</script>
@elseif(session('info'))
<script>
    toastr.info('{{ session("info") }}');
</script>
@endif

@if($errors->any())
@foreach($errors->all() as $error)
<script>
    toastr.error('{{ $error }}');
</script>
@endforeach
@endif