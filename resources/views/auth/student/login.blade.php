<!DOCTYPE html>
<html lang="en" class="kt-sweetalert2--nopadding">
<!-- begin::Head -->

<head>
    <meta charset="utf-8" />

    <title>Sekolah Pintar | Login</title>
    <meta name="description" content="User login example">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" media="all" href="{{ asset('assets_gyph/application-64f670320d32a790640a92f44a12f2bfa20d270c4715f70c2a490fba75dbc97d.css') }}" data-turbolinks-track="true" />
    <!--begin::Fonts -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: { "families": ["Poppins:300,400,500,600,700"] },
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Fonts -->



    <!--begin::Page Custom Styles(used by this page) -->
    <link href="{{ asset('assets/app/custom/user/login-v1.demo6.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Page Custom Styles -->

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="{{ asset('assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/demo/demo6/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/dist/css/iziToast.min.css') }}">
    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <!--end::Layout Skins -->

    <style>
        .kt-login-v1 .kt-login-v1__body .kt-login-v1__wrapper .kt-login-v1__container .kt-login-v1__actions .btn {
            background-color: white;
            color: #3799fb;
        }

        .kt-login-v1 .kt-login-v1__body .kt-login-v1__wrapper .kt-login-v1__container .kt-login-v1__actions .btn:hover {
            border: none;
        }

        .kt-login-v1 .kt-login-v1__body .kt-login-v1__wrapper .kt-login-v1__container .kt-login-v1__form .form-control{
            border : 1px solid white;
        }

        .hero-text{
            color: white !important;
            margin-top: -90px;
        }
    </style>
</head>
<!-- end::Head -->

<!-- begin::Body -->

<body  style="background-image: linear-gradient(to right bottom,#0052d4, #6fb1fc)"  class="kt-sweetalert2--nopadding kt-header-mobile--fixed kt-page-content-white kt-subheader--enabled kt-aside--secondary-enabled kt-offcanvas-panel--left kt-aside--left kt-page--loading"  >

                   <!-- begin::Page loader -->
    
<!-- end::Page Loader -->        
        <!-- begin:: Page -->
    
    <div class="kt-grid kt-grid--ver kt-grid--root">
        <div class="kt-grid__item  kt-grid__item--fluid kt-grid kt-grid--hor kt-login-v1" id="kt_login_v1">
            <!--begin::Item-->
            <div class="kt-grid__item">
                <!--begin::Heade-->
                <div class="kt-login-v1__head">
                    <div class="kt-login-v1__logo">
                        <a href="login-v1.html#">
                            <h3 class="text-white">Kodein</h3>
                        </a>
                    </div>  
                    <div class="kt-login-v1__signup">
                        <h4 class="kt-login-v1__heading">Don't have an account?</h4>
                        <a href="{{ route('register') }}">Sign Up</a>
                    </div> 
                </div>
                <!--begin::Head-->
            </div>
            <!--end::Item-->

            <!--begin::Item-->
            <div class="kt-grid__item  kt-grid kt-grid--ver  kt-grid__item--fluid">
                <!--begin::Body-->
                <div class="kt-login-v1__body pt-0">
                    <!--begin::Section-->
                    <div class="kt-login-v1__section">
                        <div class="block intro">
                        <h1 class="hero-text">Kelola Sekolah <br />Dengan Nyaman,Cepat & <br />Efisien.</h1>
                        <!-- <div class="download-sample-wrap">
                            <a href="helloglyph.html" target="_blank" class="download-sample"><i class="icon-download"></i>Check out sample</a>
                        </div> -->
                        </div>
                    </div>
                    <!--begin::Section-->

                    <!--begin::Separator-->
                    <div class="kt-login-v1__seaprator"></div>
                    <!--end::Separator-->

                    <!--begin::Wrapper-->

                    <div class="kt-login-v1__wrapper">
                        <div class="kt-login-v1__container">
                            <h3 class="kt-login-v1__title">
                                Sign To Account
                            </h3>

                            <!--begin::Form-->
                            <form class="kt-login-v1__form kt-form" action="{{ route('student.login.store') }}" autocomplete="off" method="post">
                                @csrf
                                <div class="form-group">

                                    <input class="form-control" type="text" placeholder="NIS atau NISN" name="nis" value="{{ old('nis') }}" autocomplete="off">

                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="password" placeholder="Password" name="password" autocomplete="off">
                                </div>

                                <div class="kt-login-v1__actions">

                                    <button type="submit" class="btn btn-pill btn-elevate">Sign In</button>
                                </div>
                            </form>
                            <!--end::Form-->
                            @if(session('error'))
                                <div class="alert alert-danger">{{ session('error') }}</div>
                            @endif                 

                        </div>
                    </div>
                    <!--end::Wrapper-->
                </div>
                <!--begin::Body-->
            </div>
            <!--end::Item-->

        </div>  
    </div>

    <!-- end:: Page -->


    <!-- begin::Global Config(global config for global JS sciprts) -->
    <script>
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#3699ff",
                    "metal": "#c4c5d6",
                    "light": "#ffffff",
                    "accent": "#00c5dc",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995",
                    "focus": "#9816f4"
                },
                "base": {
                    "label": [
                        "#c5cbe3",
                        "#a1a8c3",
                        "#3d4465",
                        "#3e4466"
                    ],
                    "shape": [
                        "#f0f3ff",
                        "#d9dffa",
                        "#afb4d4",
                        "#646c9a"
                    ]
                }
            }
        };
    </script>
    <!-- end::Global Config -->

    <!--begin::Global Theme Bundle(used by all pages) -->
    <script src="{{ asset('assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/demo6/base/scripts.bundle.js')}}" type="text/javascript"></script>
    <!--end::Global Theme Bundle -->




    <!--begin::Page Scripts(used by this page) -->
    <script src="{{ asset('assets/dist/js/iziToast.min.js') }}"></script>
    <!--end::Page Scripts -->

    <!--begin::Global App Bundle(used by all pages) -->
    <script src="{{ asset('assets/app/bundle/app.bundle.js') }}" type="text/javascript"></script>
    <!--end::Global App Bundle -->
    @include('alerts.main')
</body>
<!-- end::Body -->

</html>