@extends('templates.temp')

@section('title','Example Create')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Form </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="" class="kt-subheader__breadcrumbs-link">
    Create </a>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <!--begin::Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">Basic Repeater</h3>
                </div>
            </div>
            <!--begin::Form-->
            <form class="kt-form">
                <div class="kt-portlet__body">
                    <div class="form-group row">
                        <label for="example-text-input" class="col-3 col-form-label">Full Name:</label>
                        <div class="col-6">
                            <input type="email" class="form-control" placeholder="Enter full name">
                            <span class="form-text text-muted">Please enter your full name</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-3 col-form-label">Email address:</label>
                        <div class="col-6">
                            <input type="email" class="form-control" placeholder="Enter email">
                            <span class="form-text text-muted">We'll never share your email with anyone else</span>
                        </div>
                    </div>
                    <div class="kt-separator kt-separator--border-dashed"></div>
                    <div class="kt-separator kt-separator--height-sm"></div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Contact</label>
                        <div class="col-lg-6">
                            <div class="kt-repeater">
                                <div data-repeater-list="demo1">
                                    <div data-repeater-item="" class="kt-repeater__item">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-chain"></i></span></div>
                                            <input type="text" class="form-control" placeholder="Phone number">
                                            <div class="input-group-append">
                                                
                                            </div>
                                        </div>  
                                        <div class="kt-separator kt-separator--space-sm"></div>
                                    </div>
                                </div>
                                <div class="kt-repeater__add-data">
                                    <span data-repeater-create="" class="btn btn-info btn-sm">
                                        <i class="la la-plus"></i> Add
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>      
                    <div class="kt-separator kt-separator--border-dashed"></div>
                    <div class="kt-separator kt-separator--space-sm"></div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-3 col-form-label">Communication:</label>
                        <div class="col-9">
                            <div class="kt-checkbox-inline">
                                <label class="kt-checkbox">
                                    <input type="checkbox"> Email 
                                    <span></span>
                                </label>
                                <label class="kt-checkbox">
                                    <input type="checkbox"> SMS 
                                    <span></span>
                                </label>
                                <label class="kt-checkbox">
                                    <input type="checkbox"> Phone 
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions kt-form__actions--right">
                        <div class="row">
                            <div class="col-lg-9 offset-lg-3">
                                <div class="kt-align-left">
                                    <button type="reset" class="btn btn-brand">Submit</button>
                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!--end::Form-->
        </div>
        <!--end::Portlet-->
    </div>
</div>
@endsection