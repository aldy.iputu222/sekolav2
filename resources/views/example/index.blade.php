@extends('templates.temp')

@section('title','Example')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Base </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="" class="kt-subheader__breadcrumbs-link">
    Alert </a>
@endsection

@section('content')
<div class="row">  
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">Local Datatable</h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <a href="data-local.html#" class="btn btn-clean kt-hidden">
                        Back to dashboard
                    </a>
                    <a href="data-local.html#"
                        class="btn btn-clean btn-bold btn-upper btn-font-sm kt-hidden">
                        <i class="la la-long-arrow-left"></i>
                        Back
                    </a>
                    <a href="data-local.html#"
                        class="btn btn-brand btn-bold btn-upper btn-font-sm">
                        <i class="flaticon2-add-1"></i>
                        New Record
                    </a>
                </div>
            </div>
        </div>

        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table data-table" id="kt_table_1">
                <thead>
                    <tr>
                        <th>Record ID</th>
                        <th>Order ID</th>
                        <th>Country</th>
                        <th class="text-center">Actions</th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td>1</td>
                        <td>61715-075</td>
                        <td>China</td>
                        <td nowrap class="text-center">
                            <a data-toggle="dropdown" class="btn btn-secondary btn-elevate btn-circle btn-icon delete-btn"> <i
                                class="la la-trash"></i> </a>
                            <a data-toggle="dropdown" class="btn btn-secondary btn-elevate btn-circle btn-icon"> <i
                                class="la la-edit"></i> </a>
                        </td>
                    </tr>
                    <tr>
                        <td>2   </td>
                        <td>61715-075</td>
                        <td>China</td>
                        <td nowrap class="text-center">
                            <a data-toggle="dropdown" class="btn btn-secondary btn-elevate btn-circle btn-icon delete-btn"> <i
                                class="la la-trash"></i> </a>
                            <a data-toggle="dropdown" class="btn btn-secondary btn-elevate btn-circle btn-icon"> <i
                                class="la la-edit"></i> </a>
                        </td>
                    </tr>
                </tbody>

            </table>
            <!--end: Datatable -->
        </div>
    </div> 
</div>
@endsection