@extends('templates.temp')

@section('title','Pendataan')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
	Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('absent.index') }}" class="kt-subheader__breadcrumbs-link">
	Absensi </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="" class="kt-subheader__breadcrumbs-link">
	Pendataan </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">Pendataan</h3>
		</div>
		<div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <a href="{{ route('absent.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                            <i class="la la-long-arrow-left"></i>
                            Kembali
                        </a>
                    </div>
                </div>
            </div>
        </div>
	</div>
	<div class="kt-portlet__body">
		<!--begin: Datatable -->
		<form action="{{ route('input_absen') }}" method="post">
			@csrf
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label for="">Pilih Tahun Ajar</label>
						<select name="tahun_pelajaran" onchange="getKelasJurusan()" id="tahun_pelajaran"
							class="form-control" required="">
							<option value="">Pilih Tahun Pelajaran</option>
							@foreach($tahun_pelajarans as $tahun_pelajaran)
							<option value="{{ $tahun_pelajaran->id }}">{{ $tahun_pelajaran->first_year }} -
								{{ $tahun_pelajaran->last_year }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label for="">Pilih Jurusan</label>
						<select name="jurusan" id="jurusan" onchange="getKelasJurusan()" class="form-control"
							required="">
							<option value="">Pilih Jurusan</option>
							@foreach($jurusans as $jurusan)
							<option value="{{ $jurusan->id }}">{{ $jurusan->name }} ({{ $jurusan->alias }})</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label for="">Pilih Rombel</label>
						<select name="kelas" id="kelas" class="form-control" required="">
							<option value="">Pilih Kelas</option>
						</select>
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label for="">Pilih Tanggal</label>
						<input type="date" class="form-control"  name="date" value="{{ date('Y-m-d') }}" />
					</div>
				</div>
				<!--end: Datatable -->
			</div>
			<div class="row">
				<div class="col-md-12">
					<button class="btn btn-brand pull-right"><i class="fa fa-search"></i> Cari</button>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
	function getKelasJurusan()
	{
		var id_jurusan = $('#jurusan').find(':selected')[0].value;
		var id_tahunpel = $('#tahun_pelajaran').find(':selected')[0].value;
		var val_jurusan = $('#jurusan').val();
		if (id_jurusan != null) {
		$.ajax({
			type:"GET",
			url:"{{ url('api/major_and_class/') }}" +'/' + id_tahunpel + '/' + id_jurusan,
			success:function(response){
				var data_kelas = response['kelas'];
				var option = '<option value="">Pilih Kelas</option>'
				if (val_jurusan != '') {
					for (var i = 0; i < data_kelas.length ; i++) {
						option += '<option value="'+ data_kelas[i]['id'] +'">'+ data_kelas[i]['name'] +'</option>';
					}
					$('#kelas').html(option);
				}else{
					$('#kelas').html('<option value="">Pilih Kelas</option>')
				}
			},
			error:function(response){
				console.log('Failed');
			}
			});
		}
	}
</script>
@endsection