@extends('templates.temp')

@section('title','Detail Absensi')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
	Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('absent.index') }}" class="kt-subheader__breadcrumbs-link">
	Detail Absensi </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">Detail Absensi</h3>
		</div>
		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<a href="{{ route('absent.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
							<i class="la la-long-arrow-left"></i>
							Kembali
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="kt-portlet__body">
		<div class="row">
			<div class="col-md-2">
				<p>Tanggal</p>
			</div>
			<div class="col-md-1">
				<p>:</p>
			</div>
			<div class="col-md-2">
				<p>{{ $date }}</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<p>Kelas</p>
			</div>
			<div class="col-md-1">
				<p>:</p>
			</div>
			<div class="col-md-2">
				<p>{{ $class->name }}</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<p>Jurusan</p>
			</div>
			<div class="col-md-1">
				<p>:</p>
			</div>
			<div class="col-md-4">
				<p>{{ $major->name." (".$major->alias.")" }}</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<p>Tahun Ajar</p>
			</div>
			<div class="col-md-1">
				<p>:</p>
			</div>
			<div class="col-md-4">
				<p>{{ $schoolyear->first_year." - ".$schoolyear->last_year }}</p>
			</div>
		</div>
		<br>
		<table class="table table-bordered table-hovered">
			<thead>
				<tr>
					<th>#</th>
					<th>NIS</th>
					<th>Nama</th>
					<th>Gender</th>
					<th class="">Izin</th>
					<th class="bg-warning text-white">Sakit</th>
					<th class="bg-danger text-white">Alpa</th>
				</tr>
			</thead>
			<tbody>
				@foreach($students as $student)
				<tr>
					<td>{{ $loop->index + 1 }}</td>
					<td>{{ $student['nis'] }}</td>
					<td>{{ $student['name'] }}</td>
					<td>{{ $student['gender'] }}</td>
					<td>{{ $student['count_i'] }}</td>
					<td>{{ $student['count_s'] }}</td>
					<td>{{ $student['count_a'] }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>

	</div>
</div>
@endsection