@extends('templates.temp')

@section('title','Detail Absensi')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
	Absensi </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('student.absent') }}" class="kt-subheader__breadcrumbs-link">
	Absensi </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">Absenku Bulan ( <b>{{ $this_month }}</b> )</h3>
		</div>
		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<a href="{{ route('student.absent') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
							<i class="la la-long-arrow-left"></i>
							Kembali
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="kt-portlet__body">
		<div class="row">
			<div class="col-md-4">
				<div class="card">
					<div class="card-header bg-warning">
						<h4 class="text-center text-white">Total Sakit</h4>
					</div>
					<div class="card-body text-center">
						<h1>{{ $count_s }} Hari</h1>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="card">
					<div class="card-header bg-danger">
						<h4 class="text-center text-white">Total Alpa</h4>
					</div>
					<div class="card-body text-center">
						<h1>{{ $count_a }} Hari</h1>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="card">
					<div class="card-header bg-dark">
						<h4 class="text-center text-white">Total Izin</h4>
					</div>
					<div class="card-body text-center">
						<h1>{{ $count_i }} Hari</h1>
					</div>
				</div>
			</div>
			<div class="col-md-12" style="padding-top:20px">
				<h4>Rekapitulasi</h4>
				<table class="table table-bordered text-center">
					<thead>
						<tr>
							<th>No</th>
							<th>Keterangan</th>
							<th>Tanggal</th>
							<th>Jam</th>
						</tr>
					</thead>
					<tbody>
						@if(count($report) > 0)
						@foreach($report as $att)
							<tr>
								<td>{{ $loop->index + 1 }}</td>
								@if($att->status == "H")
								<td>Hadir</td>
								<td>{{ date('d F Y',strtotime($att->date)) }}</td>
								<td>{{ substr($att->created_at,10,10) }}</td>
								@elseif($att->status == "I")
								<td>Izin</td>
								<td>{{ date('d F Y',strtotime($att->date)) }}</td>
								<td> - </td>
								@elseif($att->status == "S")
								<td>Sakit</td>
								<td>{{ date('d F Y',strtotime($att->date)) }}</td>
								<td> - </td>
								@else
								<td>Alpa</td>
								<td>{{ date('d F Y',strtotime($att->date)) }}</td>
								<td> - </td>
								@endif
							</tr>
						@endforeach
						@else
							<tr>
								<td colspan="4">No Data Found</td>
							</tr>
						@endif
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<button type="button" class="btn btn-info btn-block btn-more" onclick="showFiltering()">(+) Lihat absen di bulan lain <span class="fa fa-caret-down"></span></button>

<div class="kt-portlet kt-portlet--mobile" id="filterAbsent" style="display:none">		
	<div class="kt-portlet__body">
		<div class="row">
			<div class="col-md-12">
				<form action="{{ route('student.absent.filter') }}" method="POST">
					@csrf
				<h4>Filter</h4>
				<div class="form-group">
					<label for="">Bulan</label>
					<select name="month" id="" class="form-control" required="">
						<option value="">Pilih Bulan</option>
						@foreach($months as $month)
						<option value="{{ $month['id'] }}-{{ $month['name'] }}">{{ $month['name'] }}</option>
						@endforeach
					</select>
				</div>
				<button type="submit" class="btn btn-primary">Cari</button>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
<script>
	function showFiltering()
	{
		var form = document.getElementById('filterAbsent');
		if (form.style.display == 'block') {
			$('#filterAbsent').slideToggle("slow",function(){
				form.style.display = 'none';
			});
			document.getElementsByClassName('btn-more')[0].innerHTML = "(+) Lihat absen di bulan lain <span class='fa fa-caret-down'></span>";
		}else{
			$('#filterAbsent').slideToggle("slow",function(){
				form.style.display = 'block';
			});
			document.getElementsByClassName('btn-more')[0].innerHTML = "(-) Sembunyikan Filter <span class='fa fa-caret-up'></span>";
		}
	}
</script>
@endsection