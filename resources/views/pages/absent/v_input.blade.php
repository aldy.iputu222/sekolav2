@extends('templates.temp')

@section('title','Detail Absensi')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
	Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('absent.index') }}" class="kt-subheader__breadcrumbs-link">
	Input Absensi </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
	<div class="kt-portlet__head kt-portlet__head--lg">
		<div class="kt-portlet__head-label">
			<h3 class="kt-portlet__head-title">Input Absensi</h3>
		</div>
		<div class="kt-portlet__head-toolbar">
			<div class="kt-portlet__head-wrapper">
				<div class="kt-portlet__head-toolbar">
					<div class="kt-portlet__head-wrapper">
						<a href="{{ route('absent.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
							<i class="la la-long-arrow-left"></i>
							Kembali
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="kt-portlet__body">
		<div class="row">
			<div class="col-md-2">
				<p>Tanggal</p>
			</div>
			<div class="col-md-1">
				<p>:</p>
			</div>
			<div class="col-md-2">
				<p>{{ $date }}</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<p>Kelas</p>
			</div>
			<div class="col-md-1">
				<p>:</p>
			</div>
			<div class="col-md-2">
				<p>{{ $class->name }}</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<p>Jurusan</p>
			</div>
			<div class="col-md-1">
				<p>:</p>
			</div>
			<div class="col-md-4">
				<p>{{ $major->name." (".$major->alias.")" }}</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2">
				<p>Tahun Ajar</p>
			</div>
			<div class="col-md-1">
				<p>:</p>
			</div>
			<div class="col-md-4">
				<p>{{ $schoolyear->first_year." - ".$schoolyear->last_year }}</p>
			</div>
		</div>
		<br>
		<form action="{{ route('absent.store') }}" method="POST">
					@csrf
				<input type="hidden" name="tgl_absen" value="{{ $date }}" placeholder="" class="form-control" required="">
				@if(count($students) > 0 )
				<div class="form-group">
				<label class="kt-checkbox kt-checkbox--solid kt-checkbox--success">
						<input type="checkbox" onchange="CheckAll()" id="CheckH"> Hadir Semua
						<span></span>
					</label>
				</div>
				@endif
				<input type="hidden" name="kelas" value="{{ $class_group_id }}">
				<input type="hidden" name="tahun_pelajaran" value="{{ $school_year_id }}">

				<table class="table table-bordered table-hovered">
					<thead>
						<tr>
						<th>#</th>
						<th>NIS</th>
						<th>Nama</th>
						<th>Jenis Kelamin</th>
						<th class="text-center">Status</th>
						</tr>
					</thead>
					<tbody>
						@foreach($students as $student)
						<tr>
							<td>{{ $loop->index + 1 }}</td>
							<td>{{ $student->nis }}</td>
							<td>{{ $student->name }}</td>
							<td>{{ $student->gender }}</td>
							<td class="text-center">
								<div class="form-check form-check-inline">
								  <label class="form-check-label" data-toggle="tooltip" data-placement="top" title="Alpa">
								    <input class="form-check-input A" type="radio" id="" required="" name="status[{{ $loop->index + 1 }}]" value="{{ $student->id }}-A"> A
								  </label>
								</div>
								<div class="form-check form-check-inline">
								  <label class="form-check-label" data-toggle="tooltip" data-placement="top" title="Izin">
								    <input class="form-check-input I" type="radio" id="" required="" name="status[{{ $loop->index + 1 }}]" value="{{ $student->id }}-I"> I
								  </label>
								</div>
								<div class="form-check form-check-inline">
								  <label class="form-check-label" data-toggle="tooltip" data-placement="top" title="Sakit">
								    <input class="form-check-input S" type="radio" id="" required="" name="status[{{ $loop->index + 1 }}]" value="{{ $student->id }}-S"> S
								  </label>
								</div>
								<div class="form-check form-check-inline">
								  <label class="form-check-label" data-toggle="tooltip" data-placement="top" title="Hadir">
								    <input class="form-check-input H" type="radio" id="tes" required="" name="status[{{ $loop->index + 1 }}]" value="{{ $student->id }}-H"> H
								  </label>
								</div>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				@if(count($students) > 0)
				<button type="submit" class="btn btn-brand">Simpan</button>
				@endif
				</form>

	</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
	function CheckAll()
	{
		var trigger_h = document.getElementById('CheckH');
		var radio_h = document.getElementsByClassName('H');
		if (trigger_h.checked == true) {
			for (var i =0;i<radio_h.length;i++) {
				radio_h[i].checked = true;
			}
		}else{
			for (var i =0;i<radio_h.length;i++) {
				radio_h[i].checked = false;
			}
		}
	}
</script>
@endsection