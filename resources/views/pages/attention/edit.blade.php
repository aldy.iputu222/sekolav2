@extends('templates.temp')

@section('title','Ubah Pengumuman')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('attention.index') }}" class="kt-subheader__breadcrumbs-link">
    Pengumuman </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="" class="kt-subheader__breadcrumbs-link">
    Ubah Pengumuman </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Ubah Pengumuman</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('attention.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>

    <form action="{{ route('attention.update',$data) }}" method="POST" class="kt-form kt-form--label-right">
        @csrf @method('put')
        <div class="kt-portlet__body">
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Judul</label>
                <div class="col-lg-6 col-md-9 col-sm-12">
                    <input type="text" name="title" class="form-control" required placeholder="Kumpul Sehabis Sekolah"
                        value="{{ old('title',$data->title) }}" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Konten</label>
                <div class="col-lg-6 col-md-9 col-sm-12">
                    <textarea class="summernote" id="m_summernote_1" rows="10" name="content">{{ old('content',$data->content) }}</textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Untuk</label>
                <div class="col-lg-6 col-md-9 col-sm-12">
                    <select name="for_who" id="" class="form-control kt_selectpicker" id="select_for">
                        @if($data->for_who == "parent")
                        <option value="parent"selected>Orang tua</option>
                        <option value="student">Siswa</option>
                        @else
                        <option value="parent">Orang tua</option>
                        <option value="student" selected>Siswa</option>
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Cakupan Pengumuman</label>
                <div class="col-lg-6 col-md-9 col-sm-12">
                    <select name="type" id="" class="form-control select_type">
                        @if($data->attention_filter[0]->type == "all")
                        <option value="all" selected>Semua</option>
                        <option value="class">Kelas</option>
                        @else
                        <option value="all">Semua</option>
                        <option value="class" selected>Kelas</option>
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-class">
                <div class="form-group row">
                    <label class="col-form-label col-lg-3 col-sm-12">Kelas</label>
                    <div class="col-lg-6 col-md-9 col-sm-12">
                        <select name="class_group_filter[]" id="" class="kt_selectpicker form-control"
                            multiple="multiple">
                            @foreach($classgroup as $field)
                            <option value="{{ $field->id }}" {{ $field->is_selected }}>{{ $field->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Tampilkan pada menu utama </label>
                <div class="col-lg-6 col-md-9 col-sm-12">
                    <select name="important" id="" class="kt_selectpicker form-control">
                        <option value="1">Ya</option>
                        <option value="0">Tidak</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-3 col-sm-12"></div>
                    <div class="col-lg-9 col-sm-12">
                        <button type="submit" class="btn btn-brand">Simpan</button>
                        <button type="reset" class="btn btn-secondary">Batal</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
@section('script')
@if($data->attention_filter[0]->type == "all")
<script>
    $(document).ready(function(){
        $('.form-class').hide();
    });
</script>
@endif
<script>
    $(document).ready(function(){
        $('.select_type').change(function(){
            if($(this).val() == "class")
            {
                $('.form-class').show();
            }else{
                $('.form-class').hide();
                $('.form-class').prop('selected', function() {
                    return this.defaultSelected;
                });
            }
        });
    });
</script>
@endsection