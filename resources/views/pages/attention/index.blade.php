@extends('templates.temp')

@section('title','Pengumuman')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('attention.index') }}" class="kt-subheader__breadcrumbs-link">
    Pengumuman </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Pengumuman</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <!-- <button class="btn btn-default btn-bold btn-upper btn-font-sm btn-reset">Reset</button>&nbsp;&nbsp;&nbsp; -->
                <a href="{{ route('attention.create') }}" class="btn btn-default btn-bold btn-upper btn-font-sm">
                    <i class="flaticon2-add-1"></i>
                    Buat Baru
                </a>
            </div>
        </div>
    </div>

    <div class="kt-portlet__body">
        <table class="table table-striped data-table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Judul</th>
                    <th>Tanggal</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $field)
                <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $field->title }}</td>
                    <td>{{ $field->created_at->diffForHumans() }}</td>
                    <td class="text-center">
                        <a href="{{ route('attention.show',$field) }}" class="btn btn-secondary btn-elevate btn-circle btn-icon"> <i
                            class="la la-eye"></i> </a>
                        <a href="{{ route('attention.destroy',$field) }}" class="btn btn-secondary btn-elevate btn-circle btn-icon delete-btn"> <i
                            class="la la-trash"></i> </a>
                        <a href="{{ route('attention.edit',$field) }}" class="btn btn-secondary btn-elevate btn-circle btn-icon"> <i
                            class="la la-edit"></i> </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection