@extends('templates.temp')

@section('title','Detail Pengumuman')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('attention.index') }}" class="kt-subheader__breadcrumbs-link">
    Pengumuman </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="" class="kt-subheader__breadcrumbs-link">
    Detail Pengumuman </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Detail Pengumuman</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('attention.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>

    <div class="kt-portlet__body">
        <div class="kt-blog-post">
            <h1 class="kt-blog-post__title kt-heading kt-heading--lg kt-heading--medium">
                {{ $data->title }}
            </h1>
            <div class="kt-blog-post__meta">
                <div class="kt-blog-post__date">
                    {{ $data->created_at->format("M d,Y") }}
                </div>
                <div class="kt-blog-post__author">
                    By <a href="" class="kt-blog-post__link">{{ $data->from_attention->name }}</a>
                </div>
            </div>
            <div class="kt-blog-post__content">
                {!! $data->content  !!}
            </div>
            <div class="kt-blog-post__foot">
                <div class="kt-blog-post__author">
                    <div class="kt-blog-post__profile">
                        @if($data->from == "admin")
                        <img src="{{ asset('app/teacher/default.png') }}"
                            class="kt-blog-post__image" />
                        @else
                        <img src="{{ $data->from_attention->photo }}"
                            class="kt-blog-post__image" />
                        @endif
                    </div>
                    <div class="kt-blog-post__label">
                        <span>{{ $data->from_attention->name }},</span> {{ $data->from }}
                    </div>
                </div>
                <!-- <div class="kt-blog-post__social">
                    <div class="kt-blog-post__label">
                        Share this post:
                    </div>
                    <a href="" class="kt-blog-post__link"><i class="flaticon-facebook-logo-button"></i></a>
                    <a href="" class="kt-blog-post__link"><i class="flaticon-twitter-logo-button"></i></a>
                    <a href="" class="kt-blog-post__link"><i class="flaticon-email-black-circular-button"></i></a>
                </div> -->
            </div>

            <!-- <div class="kt-blog-post__comments">
                <div class="kt-blog-post__input">
                    <div class="kt-blog-post__input-title">
                        38 Comments
                    </div>
                    <form class="kt-blog-post__form">
                        <textarea class="kt-blog-post__textarea" rows="4"></textarea>
                        <button class="btn btn-pill btn-tall btn-wider btn-brand btn-upper btn-bold">Submit</button>
                    </form>
                </div>
                <div class="kt-blog-post__threads">
                    <div class="kt-blog-post__thread">
                        <div class="kt-blog-post__head">
                            <img src="https://keenthemes.com/keen/preview/demo6/custom/blog/assets/media/users/100_10.jpg"
                                class="kt-blog-post__image" />
                        </div>
                        <div class="kt-blog-post__body">
                            <div class="kt-blog-post__top">
                                <div class="kt-blog-post__author">
                                    <div class="kt-blog-post__label">
                                        <span>Jeremy Fisher,</span> 2 days ago
                                    </div>
                                </div>
                                <a href="" class="kt-blog-post__link">Reply</a>
                            </div>
                            <div class="kt-blog-post__content">
                                <p>For instance, at Yoast.com, we write blogposts largely to
                                    inform people about SEO. So the objective is to inform
                                    people. However, we also want people to become return
                                    visitors and to gain trust in our brand. </p>
                            </div>
                        </div>
                    </div>
                    <div class="kt-blog-post__thread kt-blog-post__thread--sub">
                        <div class="kt-blog-post__head">
                            <img src="https://keenthemes.com/keen/preview/demo6/custom/blog/assets/media/users/100_7.jpg"
                                class="kt-blog-post__image" />
                        </div>
                        <div class="kt-blog-post__body">
                            <div class="kt-blog-post__top">
                                <div class="kt-blog-post__author">
                                    <div class="kt-blog-post__label">
                                        <span>Jeremy Fisher,</span> 2 days ago
                                    </div>
                                </div>
                                <a href="" class="kt-blog-post__link">Reply</a>
                            </div>
                            <div class="kt-blog-post__content">
                                <p>For instance, at Yoast.com, we write blogposts largely to
                                    inform people about SEO. So the objective is to inform
                                    people. However, we also want people to become return
                                    visitors and to gain trust in our brand. </p>
                            </div>
                        </div>
                    </div>
                    <div class="kt-blog-post__thread">
                        <div class="kt-blog-post__head">
                            <img src="https://keenthemes.com/keen/preview/demo6/custom/blog/assets/media/users/100_9.jpg"
                                class="kt-blog-post__image" />
                        </div>
                        <div class="kt-blog-post__body">
                            <div class="kt-blog-post__top">
                                <div class="kt-blog-post__author">
                                    <div class="kt-blog-post__label">
                                        <span>Jeremy Fisher,</span> 2 days ago
                                    </div>
                                </div>
                                <a href="" class="kt-blog-post__link">Reply</a>
                            </div>
                            <div class="kt-blog-post__content">
                                <p>For instance, at Yoast.com, we write blogposts largely to
                                    inform people about SEO. So the objective is to inform
                                    people. However, we also want people to become return
                                    visitors and to gain trust in our brand. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</div>
@endsection