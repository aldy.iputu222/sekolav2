@extends('templates.main')

@section('title','Pengumuman')
@section('page_name','Pengumuman')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="">Info</a></li>
<li class="breadcrumb-item"><a href="">Pengumuman</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <h5>Filter</h5>
            </div>
            <div class="card-body">
                
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <form action="">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Cari Pengumuman...">
            </div>
        </form>
        @foreach($data as $field)
        <div class="card">
            <div class="card-header">
                <h5 class="mt-2">{{ $field->attention->title }}
                @if($field->attention->important == "1")
                <div class="badge badge-danger">Informasi Penting</div>
                @endif

                <p class="text-muted">Dari : {{ $field->attention->from_attention->name }}</p>
            </h5>
            <div class="card-header-right">
                    <div class="btn-group card-option">
                        <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="feather icon-more-horizontal"></i>
                        </button>
                        <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                            <li class="dropdown-item full-card"><a href="#!"><span><i class="feather icon-maximize"></i> maximize</span><span style="display:none"><i class="feather icon-minimize"></i> Restore</span></a></li>
                            <li class="dropdown-item minimize-card"><a href="#!"><span><i class="feather icon-minus"></i> collapse</span><span style="display:none"><i class="feather icon-plus"></i> expand</span></a></li>
                            <li class="dropdown-item reload-card"><a href="#!"><i class="feather icon-refresh-cw"></i> reload</a></li>
                            <li class="dropdown-item close-card"><a href="#!"><i class="feather icon-trash"></i> remove</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card-body">
                {!! $field->attention->content !!}
            </div>
            <div class="card-footer">
            <p class="mb-0 text-muted">{{ $field->created_at->diffForHumans() }}</p>
            </div>
        </div>
        @endforeach
    </div>
</div>
<div class="row">
    <div class="offset-10 col-md-2">
        {{ $data->links() }}
    </div>
</div>
@endsection
