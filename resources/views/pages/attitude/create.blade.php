@extends('templates.temp')

@section('title','Formulir attitude')
@section('page_name','Formulir attitude')
@section('title','Buat Reward / Punishment')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('attitude.index') }}" class="kt-subheader__breadcrumbs-link">
    attitude </a>
<a href="{{ route('attention.index') }}" class="kt-subheader__breadcrumbs-link">
    Reward & Punishment </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="" class="kt-subheader__breadcrumbs-link">
    Buat Reward / Punishment </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Attitude</h3>
            <h3 class="kt-portlet__head-title">Buat Reward / Punishment</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('attitude.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>

    <form class="kt-form kt-form--label-right" method="POST" action="{{ route('attitude.store') }}" enctype="multipart/form-data">
        @csrf
        <div class="kt-portlet__body">

            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Nama</label>
                <div class="col-lg-6 col-md-9 col-sm-12">
                    <input type="text" name="name" class="form-control" required placeholder="Contoh : Aktif di kelas" autocomplete="off" 
                        value="{{ old('name') }}" />
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Tipe</label>
                <div class="col-lg-6 col-md-9 col-sm-12">
                    <select name="type" id="" class="form-control kt_selectpicker" id="select_for">
                        <option value="reward">Reward</option>
                        <option value="punishment">Punishment</option>
                    </select>
                </div>
            </div>

             <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Kode</label>
                <div class="col-lg-6 col-md-9 col-sm-12">
                    <input type="text" name="code" value="" placeholder="" class="form-control" >
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Point</label>
                <div class="col-lg-6 col-md-9 col-sm-12">
                    <input type="number" name="point" value="" placeholder="" class="form-control" >
                </div>
            </div>
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-3 col-sm-12"></div>
                    <div class="col-lg-9 col-sm-12">
                        <button type="submit" class="btn btn-brand">Simpan</button>
                        <button type="reset" class="btn btn-secondary">Batal</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function(){
        $('#school_year_select').change(function(){
            var school_year = $(this).val();
            $.post("{{ url('api/school_year/get_class') }}",{
                'school_year_id': school_year,
            },function(data){
                var options = '<option value="">Pilih Kelas</option>';
                $(data).each(function(index,value){
                    options += '<option value="'+value.id+'">'+value.name+'</option>';
                });
                $('#classgroup_select').html(options);
            });
        });
    });
</script>
@endsection
