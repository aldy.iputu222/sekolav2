@extends('templates.temp')

@section('title','Formulir Edit attitude')
@section('page_name','Formulir Edit attitude')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('attitude.index') }}" class="kt-subheader__breadcrumbs-link">
    attitude </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Attitude</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('attitude.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>

    <form class="kt-form kt-form--label-right" method="POST" action="{{ route('attitude.update',$data) }}" enctype="multipart/form-data">
        @csrf @method('patch')
        <div class="kt-portlet__body">

            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Nama</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                        <input type="text" class="form-control" name="name" id="nama" required p value="{{ $data->name }}">
                </div>
            </div>

             <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Kode</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" name="code" class="form-control"  required placeholder="Contoh : P.0.1" value="{{ $data->code }}" />
                </div>
            </div>
            

            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Type</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                     <select name="type" class="form-control kt_selectpicker">
                        @if($data->type == "punishment")
                        <option value="punishment" selected>Punishment</option>
                        <option value="reward">Rewards</option>
                        @else
                        <option value="punishment" selected>Punishment</option>
                        <option value="reward">Rewards</option>
                        @endif
                    </select>
                </div>
            </div>
           
        </div>

        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-4 col-sm-12"></div>
                    <div class="col-lg-8 col-sm-12">
                        <button type="submit" class="btn btn-brand">Simpan</button>
                        <button type="reset" class="btn btn-secondary">Batal</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
