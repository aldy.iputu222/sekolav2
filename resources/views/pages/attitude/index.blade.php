@extends('templates.temp')

@section('title','Attitude')
@section('title','Reward & Punishment')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('attention.index') }}" class="kt-subheader__breadcrumbs-link">
    Reward & Punishment </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Attitude</h3>
        </div>  
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('attitude.create') }}" class="btn btn-default btn-bold btn-upper btn-font-sm">
                    <i class="flaticon2-add-1"></i>
                    Buat Baru
                </a>
            </div>
        </div>
    </div>

    <div class="kt-portlet__body">

        <!--begin: Datatable -->
        <table class="table  data-table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Code</th>
                    <th>Point</th>
                    <th>Cation</th>
            </thead>
            <tbody>
                @foreach($data as $field)
                <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $field->name }}</td>
                    <td>{{ $field->type }}</td>
                    <td>{{ $field->code}}</td>
                    <td>{{ $field->point }}</td>
                    <td class="text-center">
                        <a href="{{ route('attitude.destroy',$field) }}" class="btn btn-secondary btn-elevate btn-circle btn-icon delete-btn"><i class="la la-trash"></i> </a>
                        <a href="{{ route('attitude.edit',$field) }}" class="btn btn-secondary btn-elevate btn-circle btn-icon"> <i
                            class="la la-edit"></i> </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <!--end: Datatable -->
    </div>
</div>
@endsection


