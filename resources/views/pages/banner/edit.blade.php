@extends('templates.temp')

@section('title','Edit Banner')
@section('page_name','Edit Banner')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
CMS Sekolah </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('banner.index') }}" class="kt-subheader__breadcrumbs-link">
    Banner </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="" class="kt-subheader__breadcrumbs-link">
    Edit Banner </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Banner</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('banner.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>
    <form action="{{ route('banner.update',$data) }}" method="POST" enctype="multipart/form-data">
        {{ method_field('put') }}
        @csrf
    <div class="kt-portlet__body">
        <div class="form-group row">
            <label class="col-form-label col-lg-4 col-sm-12">Title Banner</label>
            <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control" name="name" id="nama" required value="{{ $data->name }}">
            </div>
        </div>
        <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Subtitle Banner</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                        <input type="text" class="form-control" name="subname" id="nama" required value="{{ $data->subname }}">
                </div>
        </div>
        <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Status</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <select name="status" class="form-control kt_selectpicker">
                    @if($data->status == 'show')
                        <option value="show" checked>Show</option>
                        <option value="hide">Hide</option>
                    @elseif($data->status == 'hide')
                        <option value="hide" checked>Hide</option>
                        <option value="show">Show</option>
                    @endif
                    </select>
                </div>
        </div>
        <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Foto</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="file" name="image" id="photo" data-default-file="{{ asset('app/'.$data->image) }}" data-allowed-file-extensions="png jpg jpeg" class="dropify" data-height="250">
                </div>
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-4 col-sm-12"></div>
                    <div class="col-lg-8 col-sm-12">
                        <button type="submit" class="btn btn-brand">Update</button>
                        <button type="reset" class="btn btn-secondary">Batal</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
