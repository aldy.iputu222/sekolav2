@extends('templates.temp')

@section('title','Ubah Bayaran')
@section('page_name','Ubah Bayaran')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('bill.index') }}" class="kt-subheader__breadcrumbs-link">
    Bayaran </a>
<a href="" class="kt-subheader__breadcrumbs-link">
    Ubah Bayaran </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Bayaran</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('bill.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>

    <form class="kt-form kt-form--label-right" method="POST" action="{{ route('bill.update', $data->id) }}"
        enctype="multipart/form-data">
        @method('PATCH')
        @csrf
        <div class="kt-portlet__body">
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Name</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control" value="{{$data->name}}" name="name" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Konten</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <textarea class="form-control" rows="5" name="content" required>{{$data->content}}</textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Total</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="number" class="form-control" value="{{$data->total}}" name="total" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Kelas</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <select name="class_group_filter[]" id="" class="kt_selectpicker form-control"
                        multiple="multiple">
                        @foreach($classgroups as $field)
                        <option value="{{ $field->id }}" {{ $field->is_selected }}>{{ $field->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-4 col-sm-12"></div>
                    <div class="col-lg-8 col-sm-12">
                        <button type="submit" class="btn btn-brand">Simpan</button>
                        <button type="reset" class="btn btn-secondary">Batal</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection