@extends('templates.temp')

@section('title','Buat  Buku')
@section('page_name','Buat  Buku')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('book.index') }}" class="kt-subheader__breadcrumbs-link">
     Buku </a>
<a href="" class="kt-subheader__breadcrumbs-link">
    Buat  Buku </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title"> Buku</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('book.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>

    <form class="kt-form kt-form--label-right" method="POST" action="{{ route('book.store') }}"
        enctype="multipart/form-data">
        @csrf
        <div class="kt-portlet__body">
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Nama Buku</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control" required name="title" value="{{ old('title') }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Deskripsi</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <textarea id="" class="form-control" rows="10" name="description">{{ old('description') }}</textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Penulis</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <select name="author_id" class="form-control kt_selectpicker" data-live-search="true">
                        @foreach($authors as $author)
                        <option value="{{ $author->id }}">{{ $author->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Penerbit</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <select name="publisher_id" class="form-control kt_selectpicker" data-live-search="true">
                        @foreach($publishers as $publisher)
                        <option value="{{ $publisher->id }}">{{ $publisher->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Tahun Terbit</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control" onkeypress="return isNumber(event)" required name="publication_year" value="{{ old('title') }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Halaman</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control" onkeypress="return isNumber(event)" required name="pages" value="{{ old('title') }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Kategori</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <select name="book_category_id" class="form-control kt_selectpicker" data-live-search="true">
                        @foreach($book_categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Cover Buku</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="file" name="cover_book" id="cover_book" data-allowed-file-extensions="png jpg jpeg" class="dropify" data-height="250">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Jumlah</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control" onkeypress="return isNumber(event)" required name="copy_count" value="{{ old('title') }}">
                </div>
            </div>

        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-4 col-sm-12"></div>
                    <div class="col-lg-8 col-sm-12">
                        <button type="submit" class="btn btn-brand">Simpan</button>
                        <button type="reset" class="btn btn-secondary">Batal</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

