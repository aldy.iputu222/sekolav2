@extends('templates.temp')

@section('title','Detail Buku')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('book.index') }}" class="kt-subheader__breadcrumbs-link">
    Detail Buku </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Detail Buku</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('book.index') }}" class="btn btn-default btn-bold btn-upper btn-font-sm">
                    <i class="fas fa-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>

    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <h4>Judul</h4>
                    <p>{{ $data->title }}</p>
                </div>
                <hr>
                <div class="form-group">
                    <h4>Deskripsi</h4>
                    <p>{!! $data->description !!}</p>
                </div>
                <hr>
                <div class="form-group">
                    <h4>Penerbit</h4>
                    <p>{{ $data->publisher->name }}</p>
                </div>
                <hr>
                <div class="form-group">
                    <h4>Pengarang</h4>
                    <p>{{ $data->author->name }}</p>
                </div>
                <hr>
                <div class="form-group">
                    <h4>Jumlah Halaman</h4>
                    <p>{{ $data->pages }}</p>
                </div>
                <hr>
                <div class="form-group">
                    <h4>Photo</h4>
                    <input type="file" disabled class="dropify"
                        data-default-file="{{ asset('app/'.$data->cover_book) }}">
                </div>
                <hr>
            </div>
        </div>
    </div>

    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-md-12">
                <h2>QRCode Copy Buku</h2>
                <hr>
            </div>
        </div>
        <div class="row">
            @foreach($book_copy as $field)
            <div class="col-md-3">
                <img src="{{ asset('app/qrcode/'.$field->qrcode.'.jpg') }}" alt="" style="width:100%">                    
                <br><br>
                <div class="alert alert-success">
                    <p>Nomor Buku : {{ $field->id }}</p>
                    <p>Status     : {{ $field->status }}</p>
                </div>
                <br><br>
            </div>
            @endforeach
        </div>
        <div class="row">
            {{ $book_copy->links() }}
        </div>
    </div>
</div>




<!-- 

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5>Buku</h5><a href="{{ route('book.index') }}" class="btn btn-dark"><i class="fa fa-arrow-left"></i> Kembali</a>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <h4>Judul</h4>
                            <p>{{ $data->title }}</p>
                        </div>
                        <hr>
                        <div class="form-group">
                            <h4>Deskripsi</h4>
                            <p>{!! $data->description !!}</p>
                        </div>
                        <hr>
                        <div class="form-group">
                            <h4>Penerbit</h4>
                            <p>{{ $data->publisher->name }}</p>
                        </div>
                        <hr>
                        <div class="form-group">
                            <h4>Pengarang</h4>
                            <p>{{ $data->author->name }}</p>
                        </div>
                        <hr>
                        <div class="form-group">
                            <h4>Jumlah Halaman</h4>
                            <p>{{ $data->pages }}</p>
                        </div>
                        <hr>
                        <div class="form-group">
                            <h4>Photo</h4>
                            <input type="file" disabled class="dropify"
                                data-default-file="{{ asset('app/'.$data->cover_book) }}">
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <h2>QRCode Copy Buku</h2>
                    <hr>
                </div>
            </div>
            <div class="row">
                @foreach($book_copy as $field)
                <div class="col-md-3">
                    <img src="{{ asset('app/qrcode/'.$field->qrcode.'.jpg') }}" alt="" style="width:100%">                    
                    <br><br>
                    <div class="alert alert-success">
                        <p>Nomor Buku : {{ $field->id }}</p>
                        <p>Status     : {{ $field->status }}</p>
                    </div>
                    <br><br>
                </div>
                @endforeach
            </div>
            <div class="row">
                {{ $book_copy->links() }}
            </div>
            </div>
        </div>
    </div>
</div>
@endsection -->