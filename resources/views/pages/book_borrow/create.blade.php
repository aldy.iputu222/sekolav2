@extends('templates.temp')

@section('title','Buat Peminjaman')
@section('page_name','Buat Peminjaman')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('book_borrow.index') }}" class="kt-subheader__breadcrumbs-link">
    Peminjaman </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="" class="kt-subheader__breadcrumbs-link">
    Buat Peminjaman </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Peminjaman</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('book_borrow.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>

    <form class="kt-form kt-form--label-right" method="post" id="form-submit" action="{{ route('book_borrow.store') }}">
        @csrf
        <div class="kt-portlet__body">
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">NIS</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="number" class="form-control" placeholder="Masukan nis" name="nis" id="nis" required value="{{ old('nis') }}">
                </div>
            </div>

            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-lg-4 col-sm-12"></div>
                        <div class="col-lg-8 col-sm-12">
                            <button type="submit" class="btn btn-brand">Lanjutkan</button>
                            <button type="reset" class="btn btn-secondary">Batal</button>
                        </div>
                    </div>
                </div>
            </div>
    </form>
</div>
@endsection
@section('js')
<script>
    // $(document).ready(function(){
    //     $('#form-submit').submit(function(e){
    //         e.preventDefault();
    //         var url = "{{ url('admin/book_borrow/next/') }}"+"/"+$('#nis').val();
    //         console.log(url);
    //         $('#form-submit').attr('action',url).submit();
    //     });
    // });
</script>
@endsection