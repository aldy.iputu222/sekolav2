@extends('templates.temp')

@section('title','Peminjaman & Pengembalian Buku')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Perpustakaan </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('employee.index') }}" class="kt-subheader__breadcrumbs-link">
    Peminjaman & Pengembalian Buku </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Peminjaman & Pengembalian Buku</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('book_borrow.create') }}" class="btn btn-default btn-bold btn-upper btn-font-sm">
                    <i class="flaticon2-add-1"></i>
                    Buat Baru
                </a>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">
        <table class="table  data-table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Operator</th>
                    <th>Peminjam</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $field)
                <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $field->user->name }}</td>
                    <td>{{ $field->student->name }}</td>
                    <td>
                        @if($field->status == "uncomplete")
                        <div class="badge badge-danger">Belum Selesai</div>
                        @endif
                    </td>
                    <td class="text-center">
                        <div class="btn-group">
                            <a href="{{ route('book_borrow.destroy',$field) }}"
                                class="btn btn-sm btn-info delete-btn"><i class="mr-0 fa fa-trash"></i></a>
                            <a href="{{ route('book_borrow.next',$field) }}" class="btn btn-sm btn-info"><i
                                    class="mr-0 fa fa-forward"></i></a>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection