@extends('templates.temp')

@section('title','Peminjaman')
@section('page_name','Peminjaman')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('book_borrow.index') }}" class="kt-subheader__breadcrumbs-link">
    Peminjaman </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="" class="kt-subheader__breadcrumbs-link">
    Form Peminjaman </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Peminjaman</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('book_borrow.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>

    <form class="kt-form kt-form--label-right" method="post" id="form-transaction"
        action="{{ url('api/book_temporary/store') }}">
        @csrf
        <div class="kt-portlet__body">
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Nomor Buku : </label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="number" class="form-control" placeholder="Contoh : 19273123" name="book_single_id" id="book_code" required>
                    <input type="hidden" class="form-control" name="user_id" value="{{ Auth::user()->id }}" required>
                    <input type="hidden" class="form-control" name="borrow_at" value="{{ date('Y-m-d') }}" required>
                    <input type="hidden" class="form-control" name="student_id" value="{{ $data->id }}" required>
                </div>
            </div>
            <div class="form-group row">
                    <label class="col-form-label col-lg-4 col-sm-12"></label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                            <button class="btn btn-primary">Pinjam</button>
                    </div>
                </div>
            <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg kt-separator--portlet-fit"></div>

            <table id="table-borrow"></table>

        </div>
    </form>
</div>
<br><br><br>
<form action="" method="post" id="form-delete">
    @csrf @method('delete')
</form>
@endsection
@section('script')
<script>
    $(document).ready(function () {
        var borrow_id = "{{ $data->id }}";
        load_table();
        function load_table() {
            $.get("{{ url('api/book_temporary/load') }}" + "/" + borrow_id, function (data) {
                var tr;
                tr += '<thead>'
                tr += '<tr>';
                tr += '<td>No</td>';
                tr += '<td>Judul Buku</td>';
                tr += '<td>Nomor Buku</td>';
                tr += '<td>Tanggal Pinjam</td>';
                tr += '<td>Aksi</td>';
                tr += '</tr>';
                tr += '</thead>'
                tr += '<tbody>'
                $(data).each(function (index, value) {
                    var number = index + 1;
                    var link = "{{ url('admin/book_borrow/delete') }}"+"/"+value.data.book_single_id;
                    console.log(value.data.book_single_id);
                    tr += '<tr>';
                    tr += '<td>' + number + '</td>';
                    tr += '<td>' + value.book_data.title + '</td>';
                    tr += '<td>' + value.data.book_single_id + '</td>';
                    tr += '<td>' + value.data.created_at + '</td>';
                    tr += '<td>';
                    if(value.data.status == "returned")
                    {
                        tr += '<span class="badge badge-success">Telah di kembalikan</span>'
                    }else{
                        tr += '<a href="' + link + '" class="btn btn-sm btn-danger ajax-btn">Kembalikan</a>'
                    }
                    tr += '</td>';
                    tr += '</tr>';
                });
                tr += '</tbody>'

                $('#table-borrow').html(tr);
            });
        }

        load_select();
        function load_select() {
            $.get("{{ url('api/book_status') }}", function (data) {
                var options;
                options = '<option value="">Pilih Buku</option>'
                $(data.book).each(function (index, value) {
                    options += '<option value="' + value.id + '">' + value.title + '</option>';
                });
                $('#book_select').html(options);
            });
        }

        function load_number() {
            $.get("{{ url('api/book_temporary/') }}" + "/" + $('#book_select').val(), function (data) {
                var option = '<option value="">Pilih Nomor Buku</option>'
                $(data.book_single).each(function (index, value) {
                    option += '<option value="' + value.id + '">' + value.id + '</option>';
                });

                $('#book_single_select').html(option);
            });
        }

        $('#book_select').change(function () {
            load_number()
        });

        $('#form-transaction').on('submit', function (e) {
            e.preventDefault();
            $.post($(this).attr('action'), $(this).serialize(), function (data) {
                console.log(data);
                switch (data.status) {
                    case 'not-found':
                        toastr.error(data.message);
                        break;
                    case 'unavailable':
                        toastr.info(data.message);
                        break;
                    case 'success':
                        load_table();
                        toastr.success(data.message);
                        break;
                
                    default:
                        break;
                }
            });
        });

        $('table').on('click','.ajax-btn',function(e){
            e.preventDefault();
            swal({ title: "Kembalikan data ?", text: "Lakukan jika yakin!", type: "warning", showCancelButton: !0, confirmButtonText: "Ya, Hapus!" })
            .then((willDelete) => {
              if (willDelete.value === true) {;
                $.get($(this).attr('href'),{},function(data){
                    console.log(data);
                    swal('Success','success','success');
                    load_table();
                });
              }
            });
        });
    });
</script>
@endsection