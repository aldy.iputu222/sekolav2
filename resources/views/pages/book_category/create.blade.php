@extends('templates.temp')

@section('title','Buat Kategori Buku')
@section('page_name','Buat Kategori Buku')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('book_category.index') }}" class="kt-subheader__breadcrumbs-link">
    Kategori Buku </a>
<a href="" class="kt-subheader__breadcrumbs-link">
    Buat Kategori Buku </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Kategori Buku</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('book_category.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>

    <form class="kt-form kt-form--label-right" method="POST" action="{{ route('book_category.store') }}"
        enctype="multipart/form-data">
        @csrf
        <div class="kt-portlet__body">
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Nama Kategori Buku</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" name="name" class="form-control" required placeholder="Contoh : Fiksi"
                        value="{{ old('name') }}" />
                </div>
            </div>

        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-4 col-sm-12"></div>
                    <div class="col-lg-8 col-sm-12">
                        <button type="submit" class="btn btn-brand">Simpan</button>
                        <button type="reset" class="btn btn-secondary">Batal</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection