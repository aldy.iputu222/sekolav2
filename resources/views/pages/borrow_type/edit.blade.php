@extends('templates.main')

@section('title','Buat Tipe Kateogir')
@section('page_name','Ubah Tipe Kateogri')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="">Dashboard</a></li>
<li class="breadcrumb-item"><a href="{{route('borrow_type.index')}}">Tipe Kateogri</a></li>
<li class="breadcrumb-item"><a href="">Ubah Tipe Kateogri</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header"><h5>Kategori</h5><a href="{{route('borrow_type.index')}}" class="btn btn-dark text-right"><i class="fas fa-arrow-left"></i>Kembali</a></div>
            <div class="card-body">
                <form action="{{route('borrow_type.update',$data)}}" method="POST">
                    @csrf @method('patch')
                    <div class="form-group">
                        <label for="">Nama</label>
                        <input type="text" class="form-control" name="name" value="{{ old('name',$data->name) }}">
                    </div>
                    <div class="form-group">
                        <label for="">Lama Hari</label>
                        <input type="number" class="form-control" name="number_date" value="{{ old('number_date',$data->number_date) }}">
                    </div>
                    <div class="form-group">
                        <label for="">Denda</label>
                        <input type="number" class="form-control" name="expired_price" value="{{ old('expired_price',$data->expired_price) }}">
                    </div>
                    <button class="btn btn-primary">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
