@extends('templates.main')

@section('title','Tipe Peminjaman')
@section('page_name','Tipe Peminjaman')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="">Dashboard</a></li>
<li class="breadcrumb-item"><a href="">Tipe Peminjaman</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header"><h5>Tipe Peminjaman</h5><a href="{{route('borrow_type.create')}}" class="btn btn-primary"><i class="fas fa-plus"></i>Tambah Data </a></div>
            <div class="card-body">
                <table class="table table-striped table-bordered data-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Lama Hari</th>
                            <th>Denda</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $field )
                        <tr>
                            <td>{{ $loop->index+1 }}</td>
                            <td>{{ $field->name }}</td>
                            <td>{{ $field->number_date }}</td>
                            <td>{{ $field->expired_price }}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                    <a href="{{ route('borrow_type.destroy',$field) }}" class="btn btn-sm btn-info delete-btn"><i class="mr-0 fa fa-trash"></i></a>
                                    <a href="{{ route('borrow_type.edit',$field) }}" class="btn btn-sm btn-info"><i class="mr-0 fa fa-pen"></i></a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
