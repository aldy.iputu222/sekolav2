@extends('templates.temp')

@section('title','Buat Kelas')
@section('page_name','Buat Kelas')
@section('breadcrumb')

    <a href="" class="kt-subheader__breadcrumbs-link">
    Form </a>
    <span class="kt-subheader__breadcrumbs-separator"></span>
    <a href="" class="kt-subheader__breadcrumbs-link">
    Class Create </a>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <!--begin::Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">Create a Class</h3>
                </div>
            </div>
            <!--begin::Form-->
            <form action="{{ route('classgroup.store') }}" class="kt-form" method="post">
                @csrf
                <div class="kt-portlet__body">
                    <div class="form-group row">
                        <label for="example-text-input" class="col-3 col-form-label">Jurusan :</label>
                        <div class="col-6">
                            <select class="form-control" name="majorid">
                                @if (count($data) > 0)
                                    @foreach ($data as $major)
                                        <option value="{{ $major->id }}">{{ $major->name }}</option>
                                    @endforeach
                                @else
                                    <option>Belum ada data</option>    
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-3 col-form-label">Jumlah Kelas:</label>
                        <div class="col-6">
                            <input type="number" name="class_count" class="form-control" placeholder="Contoh: 10">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-3 col-form-label">Tahun Ajar :</label>
                        <div class="col-6">
                            <select class="form-control" name="school_year_id">
                                @if (count($school_years) > 0)
                                    @foreach($school_years as $val)
                                    <option value="{{ $val->id }}">{{ $val->first_year }} - {{ $val->last_year }}</option>
                                    @endforeach
                                @else
                                    <option>Belum ada data</option>    
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="kt-separator kt-separator--border-dashed"></div>
                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions kt-form__actions--right">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="kt-align-left">
                                    <button type="submit" class="btn btn-brand mr-2">Submit</button>
                                    <a href="{{ route('classgroup.index') }}" class="btn btn-secondary">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!--end::Form-->
        </div>
        <!--end::Portlet-->
    </div>
<a href="" class="kt-subheader__breadcrumbs-link">
    Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('student.index') }}" class="kt-subheader__breadcrumbs-link">
    Kelas </a>
<a href="" class="kt-subheader__breadcrumbs-link">
    Buat Kelas </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Kelas</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('classgroup.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>

    <form class="kt-form kt-form--label-right" method="POST" action="{{ route('classgroup.store') }}"
        enctype="multipart/form-data">
        @csrf
        <div class="kt-portlet__body">
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Jumlah Kelas</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="number" name="class_count" class="form-control" required placeholder="Contoh : 4"
                        value="{{ old('class_count') }}" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Tahun Ajar</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <select name="school_year_id" id="" class="form-control kt_selectpicker">
                        @foreach($school_years as $val)
                        <option value="{{ $val->id }}">{{ $val->first_year }} - {{ $val->last_year }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Kelas</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <select name="majorid" id="" class="form-control kt_selectpicker">
                        @if (count($data) > 0)
                        @foreach ($data as $major)
                        <option value="{{ $major->id }}">{{ $major->name }}</option>
                        @endforeach
                        @else
                        <option>Belum ada data</option>
                        @endif

                    </select>
                </div>
            </div>

        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-4 col-sm-12"></div>
                    <div class="col-lg-8 col-sm-12">
                        <button type="submit" class="btn btn-brand">Simpan</button>
                        <button type="reset" class="btn btn-secondary">Batal</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection