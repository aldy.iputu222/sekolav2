@extends('templates.temp')

@section('title','Kelas')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('classgroup.index') }}" class="kt-subheader__breadcrumbs-link">
    Kelas </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Kelas</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('classgroup.create') }}" class="btn btn-brand btn-bold btn-upper btn-font-sm">
                    <i class="flaticon2-add-1"></i>
                    Buat Baru
                </a>
            </div>
        </div>
    </div>

    <div class="kt-portlet__body">
        <!--begin: Datatable -->
        <table class="table data-table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Tingkat</th>
                    <th>Tahun Ajar</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $field)
                <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $field->name }}</td>
                    <td>{{ $field->grade }}</td>
                    <td>{{ $field->school_year->first_year }} - {{ $field->school_year->last_year }}</td>
                    <td>{{ $field->status }}</td>
                    <td class="text-center">
                        <a href="{{ route('classgroup.destroy',$field) }}"
                        class="btn btn-secondary btn-elevate btn-circle btn-icon delete-btn"> <i
                            class="la la-trash"></i> </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <!--end: Datatable -->
    </div>
</div>
@endsection