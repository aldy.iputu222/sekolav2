@extends('templates.main')

@section('title','Document')
@section('page_name','Document')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="">Data Master</a></li>
<li class="breadcrumb-item"><a href="">Pembelajran</a></li>
<li class="breadcrumb-item"><a href="">Document</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header"><h5>Document</h5><a href="" class="btn btn-dark text-right"><i class="fas fa-arrow-left"></i>Kembali</a></div>
            <div class="card-body">
                <form action="{{ route('document.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="">Nama</label>
                        <input type="text" class="form-control" name="name">
                    </div>

                     <div class="form-group">
                        <label for="">Kontent</label>
                        <input type="text" class="form-control" name="content">
                    </div>
					
					  <div class="form-group">
                        <label for="Type">Type</label>
                        <select name="type" id="type" class="select2">
                            <option value="#">Pilih Tipe</option>
                            <option value="local">Local</option>
                            <option value="embed">Embed</option>
                        </select>
                    </div>

                    <div class="form-group form-file">
                        <label for="">Local</label>
                        <input type="file" class="dropify" name="file">
                    </div>
                    <div class="form-group form-embed">
                        <label for="">Embed</label>
                        <input type="text" class="form-control" placeholder="https://www.youtube.com/embed/CacPltdpmrI" name="file">
                    </div>

                     <div class="form-group">
                        <label for="">Status</label>
                        <select id="" class="select2" name="status">
                            <option value="publish">publish</option>
                            <option value="draft">draft</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="teacher_id">Guru</label>
                        <select name="teacher_id" id="" class="select2">
                            @foreach($teacher as $field)
                            <option value="{{ $field->id }}">{{ $field->name }}</option>
                            @endforeach
                        </select>
                    </div>

                     <div class="form-group">
                        <label for="matpel_id">Mata Pelajaran</label>
                        <select name="matpel_id" id="" class="select2">
                            @foreach($matpel as $field)
                            <option value="{{ $field->id }}">{{ $field->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    
                    <button class="btn btn-primary">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('.form-embed').hide();
        $('.form-file').hide();
        $('#type').change(function(){
            if($(this).val()=="embed")
            {
                $('.form-embed').show();
                $('.form-file').hide();
            }else if($(this).val()=="local"){
                $('.form-embed').hide();
                $('.form-file').show();
            }else{
                $('.form-embed').hide();
                $('.form-file').hide();
            }
        });
    });
</script>
@endsection

