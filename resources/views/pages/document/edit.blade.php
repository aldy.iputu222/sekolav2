@extends('templates.main')

@section('title','Video Edit')
@section('page_name','Video Edit')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="">Dashboard</a></li>
<li class="breadcrumb-item"><a href="{{ route('document.index') }}">document</a></li>
<li class="breadcrumb-item"><a href="{{ route('document.edit',$edit->id) }}">document Edit</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header"><h5>Title</h5><a href="{{ route('document.index') }}" class="btn btn-dark text-right"><i class="fas fa-arrow-left"></i>Kembali</a></div>
            <div class="card-body">
                <form action="{{ route('document.update',$edit->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf @method('patch')
                    <div class="form-group">
                        <label for="title">Nama Materi</label>
                        <input type="text" class="form-control" autocomplete="" name="name" id="title" value="{{ $edit->name }}">
                    </div>

                     <div class="form-group">
                        <label for="title">Nama Mata Pelajaran</label>
                        <input type="text" class="form-control" readonly="" name="matpel_id" id="title" value="{{ $edit->subject->name }}">
                    </div>

                      <div class="form-group">
                        <label for="title">Nama Guru</label>
                        <input type="text" class="form-control" readonly="" name="teacher_id" id="title" value="{{ $edit->teacher->name }}">
                    </div>

                    <div class="form-group">
                        <label for="content">Konten</label>
                         <input type="text" class="form-control" name="content" id="title" value="{{ $edit->content }}">
                    </div>

                    <div class="form-group">
                        <label for="Type">Type</label>
                        <p style="color: red;font-size: 12px;font-weight: bold">*Hanya bisa diganti dengan tipe yang sama*</p>
                        <select name="type" id="type" class="select2" style="display: none;">
                            @if($edit->type == "local")
                                <option value="">Pilih Tipe</option>
                                <option value="local" selected>Local</option>
                            @elseif($edit->type == "embed")
                                <option value="">Pilih Tipe</option>
                                <option value="embed" selected>Embed</option>
                            @endif
                        </select>
                    </div>

                    @if($edit->type == "local")
                    <div class="form-group form-file">
                        <label for="">Local</label>
                        <input type="file" class="dropify" data-allowed-file-extensions="pdf" name="file" data-default-file="{{ asset('app/'.$edit->file) }}">
                    </div>
                    @elseif($edit->type == "embed")
                    <div class="form-group form-embed">
                        <label for="">Embed</label>
                        <input type="text" class="form-control" placeholder="https://www.youtube.com/embed/CacPltdpmrI?list=RDCacPltdpmrI" name="file" value="{{ $edit->file }}">
                    </div>
                    @endif

                   

                    <button class="btn btn-primary" type="submit">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- <script>
    $(document).ready(function(){
        $('.form-embed').hide();
        $('.form-file').hide();
        $('.form-dropify').hide();
        $('#type').change(function(){
            if($(this).val()=="embed")
            {
                $('.form-embed').show();
                $('.form-file').hide();
            }else if($(this).val()=="local"){
                $('.form-embed').hide();
                $('.form-dropify').show();
            }else{
                $('.form-embed').hide();
                $('.form-file').hide();
            }
        });
    });
</script> -->
@endsection
    