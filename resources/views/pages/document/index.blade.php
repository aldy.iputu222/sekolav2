@extends('templates.main')

@section('title', 'Document')
@section('page_name', 'Document')
@section('breadcrumb')

    <li class="breadcrumb-item"><a href="">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="">Document</a></li>
    
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header"><h5>Tabel Document</h5><a href="{{ route('document.create') }}" class="btn btn-primary"><i class="fas fa-plus"></i>Tambah Data </a></div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered data-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Konten</th>
                            <th>Tipe</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($document as $show)
                       <tr>
                           <td>1</td>
                           <td>{{$show->name}}</td>
                           <td>{{$show->content}}</td>
                           <td>{{$show->type}}</td>
                           <td class="text-center">
                            <div class="btn-group">
                                <a href="{{ route('document.destroy',$show->id) }}" class="btn btn-sm btn-danger delete-btn"><i class="mr-0 fa fa-trash"></i></a>
                                <a href="{{ route('document.edit',$show) }}" class="btn btn-sm btn-info"><i class="mr-0 fa fa-pen"></i></a>
                                <a href="{{ route('document.show',$show->id) }}" class="btn btn-info"><i class="mr-0 fa fa-eye"></i></a>
                            </div>
                            </td>
                       </tr>
                       @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
