@extends('templates.main')

@section('title', 'Document')
@section('page_name', 'Document')
@section('breadcrumb')

    <li class="breadcrumb-item"><a href="">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="">Document</a></li>
    
@endsection

@section('content')

        <div class="container  pull-up">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                     <div class="card-header"><h5>Kembali</h5><a href="{{ route('document.index') }}" class="btn btn-primary"><i class="fas fa-reply-all"></i></a></div>
                        <div class="card-body">
                            <h3 align="center">Document Details</h3>
                            <hr>

                            <div class="col-md-8">
                            	<table cellpadding="10" cellspacing="0">
                            		<thead>
	                            		<tr>
	                            			<td>Nama Document</td>
	                            			<td>:</td>
	                            			<td>{{ $details->name }}</td>
	                            		</tr>
                                        <tr>
                                            <td>Nama Guru</td>
                                            <td>:</td>
                                            <td>{{ $details->teacher->name }}</td>
                                        </tr>
                                         <tr>
                                            <td>Nama Mata Pelajaran</td>
                                            <td>:</td>
                                            <td>{{ $details->subject->name }}</td>
                                        </tr>
	                            	</thead>	
                            	</table>
                            </div>
                            <div class="col-md-12">
                            	<div class="card">
                            		<div class="card-header">Document Pembelajaran</div>
                            		<div class="card-body">
                            			@if($ext == "png" or $ext == "jpg" or $ext == "jpeg")
											<img src="{{ asset('app/'.$details->file) }}" alt="">
		                            	@elseif($ext == "pdf")
											<embed src="{{ asset('app/'.$details->file) }}" width="100%" height="100%" type='application/pdf'>
		                            	@elseif($ext == "mp4" or $ext == "MP4")
		                            		<video width="100%" height="100%" controls="">
		                            			<source src="{{ asset('app/'.$details->file) }}" type="video/mp4">
		                            		</video>
                                        @else
                                            <input type="text" class="form-control" readonly value="{{$details->file}}">
		                            	@endif
                            		</div>
                            	</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
	
	

@endsection