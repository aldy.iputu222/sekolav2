@extends('templates.temp')

@section('title','Buat employee')
@section('page_name','Buat employee')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('employee.index') }}" class="kt-subheader__breadcrumbs-link">
    employee </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="" class="kt-subheader__breadcrumbs-link">
    Buat employee </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">employee</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('employee.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>

    <form class="kt-form kt-form--label-right" action="{{route('employee.store')}}" method="POST" enctype="multipart/form-data">
        @csrf
    <div class="kt-portlet__body">

        <div class="form-group row">
            <label class="col-form-label col-lg-4 col-sm-12">Nama</label>
            <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" class="form-control" name="name" id="nama" required value="{{ old('name') }}">
            </div>
        </div>
		
		<div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Email</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="email" class="form-control" name="email" required value="{{ old('email') }}">
                </div>
        </div>

        <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Address</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                        <textarea type="text" class="form-control" name="address" required value="{{ old('address') }}"></textarea> 
                </div>
        </div>

        <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Nomor Telepon</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="number" class="form-control" name="phone" required value="{{ old('phone') }}">
                </div>
        </div>

        <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Foto</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="file" name="photo" id="photo" data-allowed-file-extensions="png jpg jpeg" class="dropify" data-height="250">
                </div>
        </div>
		
		 <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Jabatan</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <select name="as" class="form-control kt_selectpicker">
                        <option value="kesiswaan">Kesiswaan</option>
                        <option value="satpam">Satpam</option>
                        <option value="perpustakaan">Perpustakaan</option>
                    </select>
                </div>
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-4 col-sm-12"></div>
                    <div class="col-lg-8 col-sm-12">
                        <button type="submit" class="btn btn-brand">Simpan</button>
                        <button type="reset" class="btn btn-secondary">Batal</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
