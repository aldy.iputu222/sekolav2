@extends('templates.exam.main')

@section('title', 'Exam Confirmation')

@section('content')
	
<div class="pcoded-main-container ml-0">
	<div class="pcoded-wrapper">
		<div class="pcoded-content">
			<div class="pcoded-inner-content">
				<div class="row align-items-center">
					<div class="col-sm-8 offset-2">
				        <div class="card">
				            
				            <div class="card-body">
				            	<h5 class="card-title pb-0">
				                <span>Konfirmasi Ulangan</span>
				                <span class="alert alert-primary d-block mt-3 f-16" role="alert">
	                                Konfirmasi semua data diri kamu dan tekan mulai ulangan jika semua sudah benar
	                            </span>
				            </h5>
				                <div class="row mt-4">
				                    <div class="col-12">
				                        <h5 class="mb-0">Nama</h5>
				                        <hr class="my-1">
				                        <p>Haikal Fikri Luzain</p>
				                    </div>

				                    <div class="col-12">
				                        <h5 class="mb-0">Kelas</h5>
				                        <hr class="my-1">
				                        <p>RPL XII-3</p>
				                    </div>

				                    <div class="col-12">
				                        <h5 class="mb-0">Jenis Ulangan</h5>
				                        <hr class="my-1">
				                        <p>Matematika</p>
				                    </div>

				                    <div class="col-12">
				                        <h5 class="mb-0">Jumlah soal</h5>
				                        <hr class="my-1">
				                        <p>40</p>
				                    </div>

				                    <div class="col-12">
				                        <h5 class="mb-0">Waktu</h5>
				                        <hr class="my-1">
				                        <p>2 jam</p>
				                    </div>

				                    <div class="col-12 text-center">
				                        <a href="{{ route('exam.start') }}" class="btn btn-info">Mulai Ulangan</a>
				                    </div>
				                    
				                </div>
				            </div>
				        </div>
			    	</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection