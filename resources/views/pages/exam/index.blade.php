@extends('templates.exam.main')

@section('title','Examp Start')

@section('subject')
    <div class="alert alert-primary mb-0 text-center" style="line-height: 0.6">
        <p class="f-22 text-dark">Matematika</p>
        <label class="text-muted font-weight-bold f-16 mb-0">Paket 05</label>
    </div>
@endsection

@section('content')
<div class="pcoded-main-container" style="margin-left:0%">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="pcoded-inner-content">
                    <!-- [ breadcrumb ] start -->
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12 filter-bar">

                                    <div id="styleSelector" class="menu-styler">
                                        <div class="style-toggler">
                                            <a href="#!"></a>
                                        </div>
                                        <div class="style-block">
                                            <h6 class="mb-2 f-18">Jawaban</h6>
                                            <hr class="my-0">

                                            <div class="card-block text-center mt-3">
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">1</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-primary dropdown-toggle"><span class="f-22">2</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">3</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">4</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">5</span></button>
                                                </div>
                                            </div>

                                            <div class="card-block text-center">
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">6</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">7</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">8</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">9</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">10</span></button>
                                                </div>
                                            </div>

                                            <div class="card-block text-center">
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">11</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-warning dropdown-toggle"><span class="f-22">12</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">13</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">14</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">15</span></button>
                                                </div>
                                            </div>

                                            <div class="card-block text-center">
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">16</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">17</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">18</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">19</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">20</span></button>
                                                </div>
                                            </div>

                                            <div class="card-block text-center">
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">21</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">22</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">23</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">24</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">25</span></button>
                                                </div>
                                            </div>

                                            <div class="card-block text-center">
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">26</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">27</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">28</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">29</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">30</span></button>
                                                </div>
                                            </div>

                                            <div class="card-block text-center">
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">31</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">32</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">33</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">34</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">35</span></button>
                                                </div>
                                            </div>

                                            <div class="card-block text-center">
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">36</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">37</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">38</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">39</span></button>
                                                </div>
                                                <div class="btn-group mb-2 mr-2">
                                                    <button class="btn drp-icon btn-rounded btn-outline-secondary dropdown-toggle"><span class="f-22">40</span></button>
                                                </div>
                                            </div>

                                            <div class="card-block text-center mt-4">
                                                <button class="btn btn-primary">Selesai</button>
                                            </div>
                                        </div>

                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ breadcrumb ] end -->
                    <div class="main-body">
                        <div class="page-wrapper">
                            <!-- [ Main Content ] start -->
                            <div class="row vertical-align-middle">
                                <!-- [ horizontal-layout ] start -->
                                <div class="col-sm-12 filter-bar">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 class="f-22">No. 5</h5>
                                            <span id="timer"></span>
                                        </div>
                                        <div class="card-block">
                                            <h5>Perhatikan sintak dibawah ini!</h5>
                                            
                                            <pre>
                                                <code class="language-markup">
                                                    &lt;!-- [ navigation menu ] start --&gt;
                                                    &lt;nav class="pcoded-navbar theme-horizontal"&gt;
                                                        &lt;div class="navbar-wrapper"&gt;
                                                            &lt;div class="navbar-brand header-logo"&gt;
                                                                &lt;!-- Your Logo is hear --&gt;
                                                            &lt;/div&gt;
                                                            &lt;div class="navbar-content sidenav-horizontal" id="layout-sidenav"&gt;
                                                                &lt;ul class="nav pcoded-inner-navbar sidenav-inner"&gt;
                                                                &lt;/ul&gt;
                                                            &lt;/div&gt;
                                                        &lt;/div&gt;
                                                    &lt;/nav&gt;
                                                    &lt;!-- [ navigation menu ] end --&gt;

                                                    &lt;script src="../assets/js/horizontal-menu.js"&gt;&lt;/script&gt;

                                                    
                                                </code>
                                            </pre>

                                            <h5 class="f-20 mb-4">Pada baris dengan tag <code><strong>&lt;script src="../assets/js/horizontal-menu.js"&gt;&lt;/script&gt;</strong></code> berfungsi untuk...</h5>

                                            <div class="card-block p-2">
                                                <div class="row d-flex align-items-center">
                                                    <div class="col-auto pr-0">
                                                        <button class="btn btn-sm drp-icon mb-0 btn-rounded btn-outline-primary dropdown-toggle wid-30 hei-30"><span>A</span>
                                                        </button>
                                                    </div>
                                                    <div class="col">
                                                        <h5 class="text-dark mb-0">Menggabungkan sintak css dari luar file
                                                        </h5>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card-block p-2">
                                                <div class="row d-flex align-items-center">
                                                    <div class="col-auto pr-0">
                                                        <button class="btn btn-sm drp-icon mb-0 btn-rounded btn-outline-primary dropdown-toggle wid-30 hei-30"><span>B</span>
                                                        </button>
                                                    </div>
                                                    <div class="col">
                                                        <h5 class="text-dark mb-0">Menggabungkan sintak css dari luar file Menggabungkan sintak css dari luar fileMenggabungkan sintak css dari luar fileMenggabungkan sintak css dari luar fileMenggabungkan sintak css dari luar file
                                                        </h5>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card-block p-2">
                                                <div class="row d-flex align-items-center">
                                                    <div class="col-auto pr-0">
                                                        <button class="btn btn-sm drp-icon mb-0 btn-rounded btn-outline-primary dropdown-toggle wid-30 hei-30"><span>C</span>
                                                        </button>
                                                    </div>
                                                    <div class="col">
                                                        <h5 class="text-dark mb-0">Menggabungkan sintak css dari luar file Menggabungkan sintak css dari luar fileMenggabungkan sintak css dari luar fileMenggabungkan sintak css dari luar fileMenggabungkan sintak css dari luar file
                                                        </h5>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card-block p-2">
                                                <div class="row d-flex align-items-center">
                                                    <div class="col-auto pr-0">
                                                        <button class="btn btn-sm drp-icon mb-0 btn-rounded btn-outline-primary dropdown-toggle wid-30 hei-30"><span>D</span>
                                                        </button>
                                                    </div>
                                                    <div class="col">
                                                        <h5 class="text-dark mb-0">Menggabungkan sintak css dari luar file Menggabungkan sintak css dari luar fileMenggabungkan sintak css dari luar fileMenggabungkan sintak css dari luar fileMenggabungkan sintak css dari luar file
                                                        </h5>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="card-block p-2">
                                                <div class="row d-flex align-items-center">
                                                    <div class="col-auto pr-0">
                                                        <button class="btn btn-sm drp-icon mb-0 btn-rounded btn-outline-primary dropdown-toggle wid-30 hei-30"><span>E</span>
                                                        </button>
                                                    </div>
                                                    <div class="col">
                                                        <h5 class="text-dark mb-0">Menggabungkan sintak css dari luar file Menggabungkan sintak css dari luar fileMenggabungkan sintak css dari luar fileMenggabungkan sintak css dari luar fileMenggabungkan sintak css dari luar file
                                                        </h5>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="card-footer">
                                            <div class="row">
                                                <div class="col text-right">
                                                    <button type="button" class="btn btn-primary"><i class="feather icon-arrow-left"></i>Sebelumnya</button>
                                                </div>
                                                <div class="col text-center">
                                                    <button class="btn btn-warning" id="btn-ragu">
                                                        <div class="custom-control checkbox checkbox-danger d-inline">
                                                            <input type="checkbox" class="custom-control-input" id="ragu">
                                                            <label class="cr mb-0">Ragu ragu</label>
                                                        </div>
                                                    </button>
                                                </div>
                                                <div class="col text-left">
                                                    <button type="button" class="btn btn-primary">Selanjutnya <i class="feather icon-arrow-right ml-2 mr-0"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- [ horizontal-layout ] end -->
                            </div>
                            <!-- [ Main Content ] end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection

@section('script')
    <script src="{{ asset('assets/js/exam-menu-setting.js') }}"></script>

    @include('templates.exam.exam-script')
@endsection
