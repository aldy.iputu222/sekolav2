@include('templates.exam.header')

@section('title','Examp Login')


<div class="auth-wrapper">

    <div class="auth-content subscribe">
        <div class="card">
            <div class="row no-gutters">
                <div class="col-md-4 col-lg-6 d-none d-md-flex d-lg-flex theme-bg align-items-center justify-content-center">
                    <img src="../assets/images/user/user.png" alt="lock images" class="img-fluid">
                </div>
                <div class="col-md-8 col-lg-6">
                    <div class="card-body text-center">
                        <div class="row justify-content-center">
                            <div class="col-sm-10">
                                <h3 class="mb-5">Login</h3>
                                <div class="input-group mb-4">
                                    <input type="text" class="form-control" placeholder="Username">
                                </div>
                                <div class="input-group mb-4">
                                    <input type="password" class="form-control" placeholder="password">
                                </div>

                                <div class="form-group mb-3">
                                    <div class="checkbox checkbox-primary d-inline">
                                        <input type="checkbox" name="checkbox-p-in-1" id="checkbox-p-in-1" checked="">
                                        <label for="checkbox-p-in-1" class="cr">Remember me</label>
                                    </div>
                                </div>
                                
                                <a href="{{ route('exam.confirm') }}" class="btn btn-primary shadow-2 mb-4 text-white">Login</a>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@include('templates.exam.footer')