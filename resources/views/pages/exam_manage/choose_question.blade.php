@extends('templates.temp')

@section('title','Pilih Butir Soal')
@section('page_name','Pilih Butir Soal')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
CMS Sekolah </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('exam_manage.index') }}" class="kt-subheader__breadcrumbs-link">
    Daftar Ulangan </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="" class="kt-subheader__breadcrumbs-link">
    Pilih Butir Soal </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">Pilih Butir Soal</h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <a href="{{ route('exam_manage.index') }}"
                        class="btn btn-default btn-bold btn-upper btn-font-sm">
                        <i class="flaticon2-add-1"></i>
                        Kembali
                    </a>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
                <table class="table data-table">
                        <thead>
                            <tr>
                                <th>Tipe</th>
                                <th>Nama Bab</th>
                                <th>Level 1</th>
                                <th>Level 2</th>
                                <th>Level 3</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>PG</td>
                                <td>Eksponen</td>
                                <td>
                                  <select name="" class="form-control" id="">
                                    <option value="">Pilih</option>
                                    <option value="">1</option>
                                  </select>
                                </td>
                                <td>
                                    <select name="" class="form-control" id="">
                                      <option value="">Pilih</option>
                                      <option value="">1</option>
                                    </select>
                                  </td>
                                  <td>
                                      <select name="" class="form-control" id="">
                                        <option value="">Pilih</option>
                                        <option value="">1</option>
                                      </select>
                                    </td>
                            </tr> 
                            <tr>
                                <td>ESSAY</td>
                                <td>Eksponen</td>
                                <td>
                                  <select name="" class="form-control" id="">
                                    <option value="">Pilih</option>
                                    <option value="">1</option>
                                  </select>
                                </td>
                                <td>
                                    <select name="" class="form-control" id="">
                                      <option value="">Pilih</option>
                                      <option value="">1</option>
                                    </select>
                                  </td>
                                  <td>
                                      <select name="" class="form-control" id="">
                                        <option value="">Pilih</option>
                                        <option value="">1</option>
                                      </select>
                                    </td>
                            </tr>
                            <tr>
                                <td>LISTENING</td>
                                <td>Eksponen</td>
                                <td>
                                  <select name="" class="form-control" id="">
                                    <option value="">Pilih</option>
                                    <option value="">1</option>
                                  </select>
                                </td>
                                <td>
                                    <select name="" class="form-control" id="">
                                      <option value="">Pilih</option>
                                      <option value="">1</option>
                                    </select>
                                  </td>
                                  <td>
                                      <select name="" class="form-control" id="">
                                        <option value="">Pilih</option>
                                        <option value="">1</option>
                                      </select>
                                    </td>
                            </tr>     
                        </tbody>
                    </table>
                </div>    
            </div>
        </div>
    </div>
</div>
@endsection
