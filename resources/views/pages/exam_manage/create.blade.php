@extends('templates.temp')

@section('title','Setting Ulangan')
@section('page_name','Setting Ulangan')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
CMS Sekolah </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('exam_manage.index') }}" class="kt-subheader__breadcrumbs-link">
    Daftar Ulangan </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('exam_manage.create') }}" class="kt-subheader__breadcrumbs-link">
    Setting Ulangan </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Setting Ulangan</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('exam_manage.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>

    <form class="kt-form kt-form--label-right" action="{{route('exam_manage.store')}}" method="POST" enctype="multipart/form-data">
        <div class="kt-portlet__body">
            @csrf
                    <div class="form-group row">
                        <label class="col-form-label col-lg-4 col-sm-12">Tipe Ulangan</label>
                        <div class="col-lg-6 col-md-9 col-sm-12">
                            <select name="exam_type_id" class="form-control kt_selectpicker">
                            @foreach($exam_type as $show)
                                <option value="{{ $show->id }}">{{ $show->name }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-4 col-sm-12">Mata Pelajaran</label>
                        <div class="col-lg-6 col-md-9 col-sm-12">
                            <select name="subject_id" class="form-control kt_selectpicker">
                            @foreach($subject as $show)
                                <option value="{{ $show->id }}">{{ $show->name }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-4 col-sm-12">Untuk Jurusan</label>
                        <div class="col-lg-6 col-md-9 col-sm-12">
                            <select name="major_id" class="form-control kt_selectpicker">
                                <option value="all">Semua Jurusan</option>
                            @foreach($major as $show)
                                <option value="{{ $show->id }}">{{ $show->name }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-4 col-sm-12">Untuk Kelas</label>
                        <div class="col-lg-6 col-md-9 col-sm-12">
                            <select name="grade" class="form-control kt_selectpicker">
                                <option value="X">Kelas 10</option>
                                <option value="XI">Kelas 11</option>
                                <option value="XII">Kelas 12</option>
                           
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-4 col-sm-12">KKM</label>
                        <div class="col-lg-6 col-md-9 col-sm-12">
                                <input type="text" class="form-control" onkeypress="return isNumber(event)" name="kkm" id="nama" required value="{{ old('kkm') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-4 col-sm-12">Durasi Ulangan</label>
                        <div class="col-lg-6 col-md-9 col-sm-12">
                                <input type="text" class="form-control" onkeypress="return isNumber(event)" name="time" id="nama" required value="{{ old('time') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-4 col-sm-12">Deskripsi</label>
                        <div class="col-lg-6 col-md-9 col-sm-12">
                               <textarea name="description" id="" cols="30" rows="10" class="summernote">{{ old('description') }}</textarea>
                        </div>
                    </div>
                    <div class="kt-portlet__foot text-center">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-12 col-sm-12"></div>
                                <div class="col-lg-12 col-sm-12">
                                    <button type="submit" class="btn btn-brand">Lanjutkan</button>
                                    <button type="reset" class="btn btn-secondary">Batal</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
@endsection
