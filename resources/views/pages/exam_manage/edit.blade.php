@extends('templates.main')

@section('title','Exam Manage Edit')
@section('page_name','Exam Manage Edit')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="">Dashboard</a></li>
<li class="breadcrumb-item"><a href="{{ route('exam_manage.index') }}">Exam Type</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header"><h5>Exam Manage Edit</h5><a href="{{ route('exam_manage.index') }}" class="btn btn-dark text-right"><i class="fas fa-arrow-left"></i>Kembali</a></div>
            <div class="card-body">
                <form action="{{ route('exam_manage.update',$data) }}" method="POST" enctype="multipart/form-data">
                @csrf @method('patch')
                    <div class="form-group">
                        <label for="nama">Tipe Ulangan</label>
                        <select name="exam_type_id" class="select2">
                            <option value="">Pilih Tipe</option>
                        @foreach($exam_type as $value)
                            @if($value->id == $data->exam_type_id)
                                <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
                            @else
                                <option value="{{ $value->id }}">{{ $value->name }}</option>
                            @endif
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="email">Mata Pelajaran</label>
                        <select name="subject_id" class="select2">
                                <option value="">Pilih Mata Pelajaran</option>
                            @foreach($subject as $value)
                                @if($value->id == $data->subject_id)
                                    <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
                                @else
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="email">Untuk Jurusan</label>
                        <select name="major_id" class="select2">
                            <option value="">Pilih Jurusan</option>
                            <option value="all">Semua Jurusan</option>
                            @foreach($major as $value)
                                @if($value->id == $data->major_id)
                                    <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
                                @else
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="email">Untuk Kelas</label>
                        <select name="grade" class="select2">
                            <option value="">Pilih Kelas</option>
                            <option value="X">X</option>
                            <option value="XI">XI</option>
                            <option value="XII">XII</option>
                        </select>
                    </div>
                    <script>$("option[value='{{ $data->grade }}']").attr('selected','selected');</script>
                    <div class="form-group">
                        <label for="email">KKM</label>
                        <input type="text" class="form-control" value="{{ $data->kkm }}" name="kkm">
                    </div>
                    <div class="form-group">
                        <label for="email">Durasi Ulangan</label>
                        <p style="color:red; font-weight:bold;">(Dalam Menit)</p>
                        <input type="text" class="form-control" value="{{ $data->time }}" name="time">
                    </div>
                    <div class="form-group">
                        <label for="email">Deskripsi</label>
                        <textarea class="ck-editor" name="description">{{ $data->description }}</textarea>
                    </div>
                    <button class="btn btn-primary" type="submit">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
