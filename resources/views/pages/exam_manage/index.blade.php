@extends('templates.temp')

@section('title','Daftar Ulangan')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Manajemen Ulangan </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('exam_manage.index') }}" class="kt-subheader__breadcrumbs-link">
    Daftar Ulangan </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">Daftar Ulangan</h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <a href="{{ route('exam_manage.create') }}"
                        class="btn btn-default btn-bold btn-upper btn-font-sm">
                        <i class="flaticon2-add-1"></i>
                        Buat Baru
                    </a>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
                <table class="table data-table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tipe Ulangan</th>
                                <th>Mapel</th>
                                <th>Jurusan</th>
                                <th>Kelas</th>
                                <th>KKM</th>
                                <th>Waktu</th>
                                <th><div class="badge badge-warning">Token</div></th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($exam_manage as $value)
                            <tr>
                                <td>{{$loop->index+1}}</td>
                                <td>{{ $value->exam_type->name }}</td>
                                <td>{{ $value->subject->name }}</td>
                                @if($value->major_id == 'all')
                                <td><div class="badge badge-primary">Semua Jurusan</div></td>
                                @else
                                <td>{{ $value->major->name }}</td>
                                @endif
                                <td>
                                    @switch($value->grade)
                                        @case('X')
                                        10
                                        @break
                                        @case('XI')
                                        11
                                        @break
                                        @case('XII')
                                        12
                                        @break
                                    @endswitch
                                </td>
                                <td>{{ $value->kkm }}</td>
                                <td>{{ $value->time }} Menit</td>
                                <td style="font-size:16px;">
                                    <div class="badge badge-danger">{{ $value->token }}</div>
                                </td>
                                <td nowrap class="text-center">
                                      <a href="{{ route('exam_manage.edit',$value) }}" class="btn btn-secondary btn-elevate btn-circle btn-icon"><i class="la la-edit"></i></a>
                                      <a href="" class="btn btn-secondary btn-elevate btn-circle btn-icon delete-btn"><i class="la la-trash"></i></a>
                                      <a href="{{ url('teacher/choose_question/'.$value->id.'/'.$value->subject_id) }}"  class="btn btn-secondary btn-elevate btn-circle btn-icon"><i class="la la-gear"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>    
            </div>
        </div>
    </div>
</div>
@endsection
