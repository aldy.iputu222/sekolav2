@extends('templates.main')

@section('title','Create Exam Type')
@section('page_name','Create Exam Type')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="">Dashboard</a></li>
<li class="breadcrumb-item"><a href="{{ route('examtype.index') }}">Exam Type</a></li>
<li class="breadcrumb-item"><a href="{{ route('examtype.create') }}">Create Exam Type</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header"><h5>Title</h5><a href="{{ route('examtype.index') }}" class="btn btn-dark text-right"><i class="fas fa-arrow-left"></i>Kembali</a></div>
            <div class="card-body">
                <form action="{{ route('examtype.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" class="form-control" name="name" id="nama">
                    </div>
                    <div class="form-group">
                        <label for="email">Grade</label>
                        <input type="grade" class="form-control" name="grade" id="grade">
                    </div>

                    <button class="btn btn-primary" type="submit">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
