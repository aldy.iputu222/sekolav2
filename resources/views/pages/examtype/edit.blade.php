@extends('templates.main')

@section('title','Edit Exam Type')
@section('page_name','Edit Exam Type')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="">Dashboard</a></li>
<li class="breadcrumb-item"><a href="{{ route('examtype.index') }}">Exam Type</a></li>
<li class="breadcrumb-item"><a href="{{ route('examtype.edit',$data->id) }}">Exam Type Edit</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header"><h5>Title</h5><a href="{{ route('examtype.index') }}" class="btn btn-dark text-right"><i class="fas fa-arrow-left"></i>Kembali</a></div>
            <div class="card-body">
                <form action="{{ route('examtype.update',$data->id) }}" method="post" enctype="multipart/form-data">
                    @csrf @method('patch')
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" class="form-control" name="name" value="{{ $data->name }}" id="nama">
                    </div>
                    <div class="form-group">
                        <label for="grade">Nama</label>
                        <input type="text" class="form-control" name="grade" value="{{ $data->grade }}" id="grade">
                    </div>

                    <button class="btn btn-primary" type="submit">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
