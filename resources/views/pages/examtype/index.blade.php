@extends('templates.main')

@section('title','Exam Type')
@section('page_name','Exam Type')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="">Dashboard</a></li>
<li class="breadcrumb-item"><a href="{{ route('examtype.index') }}">Exam Type</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header"><h5>Exam Type</h5><a href="{{ route('examtype.create') }}" class="btn btn-primary"><i class="fas fa-plus"></i>Tambah Data </a></div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered data-table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Grade</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $s)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td>{{ $s->name }}</td>
                                <td>{{ $s->grade }}</td>
                                <td class="text-center">
                                    <div class="btn-group text-center">
                                        <a href="{{ route('examtype.edit',$s) }}" class="btn btn-sm btn-info text-center"><i class="mr-0 fas fa-edit"></i></a>
                                        <a href="{{ route('examtype.destroy',$s->id) }}" class="btn btn-sm btn-info delete-btn"><i class="mr-0 fas fa-trash"></i></a>
                                        <a href="{{ route('examtype.show',$s) }}" class="btn btn-sm btn-info"><i class="mr-0 fas fa-eye"></i></a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>    
            </div>
        </div>
    </div>
</div>
@endsection
