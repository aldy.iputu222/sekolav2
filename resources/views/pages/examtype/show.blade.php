@extends('templates.main')

@section('title','Detail Guru')
@section('page_name','Detail Guru')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="">Data Master</a></li>
<li class="breadcrumb-item"><a href="">Detail Guru</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5>Sekolah</h5><a href="{{ route('teacher.index') }}" class="btn btn-dark"><i class="fa fa-arrow-left"></i> Kembali</a>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <h4>Nama</h4>
                            <p>{{ $data->name }}</p>
                        </div>
                        <hr>
                        <div class="form-group">
                            <h4>Email</h4>
                            <p>{{ $data->email }}</p>
                        </div>
                        <hr>
                        <div class="form-group">
                            <h4>Jenis Kelamin</h4>
                            <p>{{ $data->gender }}</p>
                        </div>
                        <hr>
                        <div class="form-group">
                            <h4>Phone</h4>
                            <p>{{ $data->phone }}</p>
                        </div>
                        <hr>
                        <div class="form-group">
                            <h4>Address</h4>
                            <p>{{ $data->address }}</p>
                        </div>
                        <div class="form-group">
                            <h4>Mata Pelajaran</h4>
                            <p>{{ $data->subject->name }}</p>
                        </div>
                        <hr>
                        <div class="form-group">
                            <h4>Photo</h4>
                            <input type="file" disabled class="dropify"
                                data-default-file="{{ asset('app/'.$data->photo) }}">
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <h4>Barcode</h4>
                            <img src="{{ asset('app/barcode/'.$data->barcode) }}" alt="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <h4>QRCode</h4>
                            <img src="{{ asset('app/qrcode/'.$data->qrcode) }}" alt="" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection