@extends('templates.temp')

@section('title','Penjadwalan')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('schedule.index') }}" class="kt-subheader__breadcrumbs-link">
    Penjadwalan </a>
<a href="" class="kt-subheader__breadcrumbs-link">
    Buat Jadwal </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Buat Jadwal</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <a href="{{ route('student.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                            <i class="la la-long-arrow-left"></i>
                            Kembali
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet__body">

        <form action="{{route('schedule.store')}}" method="POST" class="kt-form kt-form--label-right">
            @csrf
            <div class="kt-portlet__body">
                <div class="form-group row">
                    <label class="col-form-label col-lg-4 col-sm-12">Hari</label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <select name="hari" class="kt_selectpicker form-control">
                            <option value="senin">Senin</option>
                            <option value="selasa">Selasa</option>
                            <option value="rabu">Rabu</option>
                            <option value="kamis">Kamis</option>
                            <option value="jumat">Jumat</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-4 col-sm-12">Jam</label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <input type="time" class="form-control" name="jam">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-4 col-sm-12">Ke</label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <input type="number" class="form-control" name="jam_ke">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-4 col-sm-12">Mata Pelajaran</label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <select name="matpel_id" id="" class="kt_selectpicker form-control" data-live-search="true">
                            @foreach($subject as $field)
                            <option value="{{ $field->id }}">{{ $field->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-4 col-sm-12">Guru</label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <select name="teacher_id" id="" class="kt_selectpicker form-control" data-live-search="true">
                            @foreach($teacher as $field)
                            <option value="{{ $field->id }}">{{ $field->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-4 col-sm-12">Kelas</label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <select name="class_id" id="" class="kt_selectpicker form-control" data-live-search="true">
                            @foreach($class as $field)
                            <option value="{{ $field->id }}">{{ $field->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-form-label col-lg-4 col-sm-12">Tahun Ajar</label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <select name="tahun_id" id="" class="kt_selectpicker form-control" data-live-search="true">
                            @foreach($tahun as $field)
                            <option value="{{ $field->id }}">{{ $field->first_year." - ".$field->last_year }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <div class="row">
                            <div class="col-lg-4 col-sm-12"></div>
                            <div class="col-lg-8 col-sm-12">
                                <button type="submit" class="btn btn-brand">Simpan</button>
                                <button type="reset" class="btn btn-secondary">Batal</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>
@endsection
@section('script')

@endsection