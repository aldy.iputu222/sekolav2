@extends('templates.temp')

@section('title','Penjadwalan')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
  Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('schedule.index') }}" class="kt-subheader__breadcrumbs-link">
  Penjadwalan </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
  <div class="kt-portlet__head kt-portlet__head--lg">
    <div class="kt-portlet__head-label">
      <h3 class="kt-portlet__head-title">Penjadwalan</h3>
    </div>
    <div class="kt-portlet__head-toolbar">
      <div class="kt-portlet__head-wrapper">
        <!-- <button class="btn btn-default btn-bold btn-upper btn-font-sm btn-reset">Reset</button>&nbsp;&nbsp;&nbsp; -->
        <a href="{{ route('schedule.create') }}" class="btn btn-default btn-bold btn-upper btn-font-sm">
          <i class="flaticon2-add-1"></i>
          Buat Baru
        </a>
      </div>
    </div>
  </div>

  <div class="kt-portlet__body">
    <!--begin: Datatable -->
    <div class="row">
      <div class="col-md-5">
        <div class="form-group">
          <label for="">Pilih Jurusan</label>
          <select name="" id="jurusan_select" class="kt_selectpicker form-control">
            <option value="" selected="">Pilih Jurusan</option>
            @foreach($jurusan as $field)
            <option value="{{ $field->id }}">{{ $field->name }}</option>
            @endforeach
          </select>
        </div>
      </div>
      <div class="col-md-5">
        <div class="form-group">
          <label for="">Pilih Rombel</label>
          <select name="" id="rombel_select" class="form-control">
            <option value=""></option>
          </select>
        </div>
      </div>
      <!--end: Datatable -->
    </div>
    <div class="row">
        <div class="col-md-12">
        <div id="jadwal_detail"></div>
      </div>
    </div>
  </div>
</div>
  @endsection
  @section('script')
  <script>
    $("#rombel_select").attr("disabled", "disabled");
    $("#jurusan_select").change(function () {
      $("#rombel_select").html("");
      var value = $(this).val();
      $("#rombel_select").removeAttr("disabled");
      if (value != null) {
        var options = '<option value =""> Pilih Rombel </option>';
        $.get("{{ url('api/ajax/rombel') }}" + "/" + value, function (data) {
          console.log(data);
          $(data).each(function (index, value) {
            options += '<option value="' + value.id + '">' + value.name + '</option>';
          });
          $("#rombel_select").html(options);
        });
      }
    });
    $('#rombel_select').change(function () {
      var value = $(this).val();
      $('#jadwal_detail').load("{{ url('api/ajax/jadwal_detail') }}" + "/" + value);
    });

    $('.btn-reset').click(function () {
      $('#jadwal_detail').html("");
    });

  </script>
  @endsection