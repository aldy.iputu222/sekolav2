<table class="table table-bordered">
  <tr>
    <td class="text-center" colspan="4"><h3>Senin</h3></td>
  </tr>
  <tr style="background:#f1f1f1;">
    <th width="100">Jam Ke</th>
    <th width="250">Pelajaran</th>
    <th>Guru</th>
    <th width="100">Jam</th>
  </tr>
  @foreach($senin as $show)
  <tr>
    <td>{{$show->jam_ke}}</td>
    <td>{{$show->Subject->name}}</td>
    <td>{{$show->Teacher->name}}</td>
    <td>{{$show->jam}}</td>
  </tr>
  @endforeach
</table>

 <table class="table table-bordered">
  <tr>
    <td class="text-center" colspan="4"><h3>Selasa</h3></td>
  </tr>
  <tr style="background:#f1f1f1;">
    <th width="100">Jam Ke</th>
    <th width="250">Pelajaran</th>
    <th>Guru</th>
    <th width="100">Jam</th>
  </tr>
  @foreach($selasa as $show)
  <tr>
    <td>{{$show->jam_ke}}</td>
    <td>{{$show->Subject->name}}</td>
    <td>{{$show->Teacher->name}}</td>
    <td>{{$show->jam}}</td>
  </tr>
  @endforeach
</table>

 <table class="table table-bordered">
  <tr>
    <td class="text-center" colspan="4"><h3>Rabu</h3></td>
  </tr>
  <tr style="background:#f1f1f1;">
    <th width="100">Jam Ke</th>
    <th width="250">Pelajaran</th>
    <th>Guru</th>
    <th width="100">Jam</th>
  </tr>
  @foreach($rabu as $show)
  <tr>
    <td>{{$show->jam_ke}}</td>
    <td>{{$show->Subject->name}}</td>
    <td>{{$show->Teacher->name}}</td>
    <td>{{$show->jam}}</td>
  </tr>
  @endforeach
</table>

 <table class="table table-bordered">
  <tr>
    <td class="text-center" colspan="4"><h3>Kamis</h3></td>
  </tr>
  <tr style="background:#f1f1f1;">
    <th width="100">Jam Ke</th>
    <th width="250">Pelajaran</th>
    <th>Guru</th>
    <th width="100">Jam</th>
  </tr>
  @foreach($kamis as $show)
  <tr>
    <td>{{$show->jam_ke}}</td>
    <td>{{$show->Subject->name}}</td>
    <td>{{$show->Teacher->name}}</td>
    <td>{{$show->jam}}</td>
  </tr>
  @endforeach
</table>

 <table class="table table-bordered">
  <tr>
    <td class="text-center" colspan="4"><h3>Jumat</h3></td>
  </tr>
  <tr style="background:#f1f1f1;">
    <th width="100">Jam Ke</th>
    <th width="250">Pelajaran</th>
    <th>Guru</th>
    <th width="100">Jam</th>
  </tr>
  @foreach($jumat as $show)
  <tr>
    <td>{{$show->jam_ke}}</td>
    <td>{{$show->Subject->name}}</td>
    <td>{{$show->Teacher->name}}</td>
    <td>{{$show->jam}}</td>
  </tr>
  @endforeach
</table>