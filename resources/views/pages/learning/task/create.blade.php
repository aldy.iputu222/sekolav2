@extends('templates.temp')

@section('title','Task Create')
@section('page_name','Task Create')
@section('breadcrumb')
<li class="breadcrumb-item">Dashboard   </li>
<li class="breadcrumb-item"><a href="{{ route('task.index') }}">Tugas</a></li>
<li class="breadcrumb-item"><a href="{{ route('task.create') }}">Tugas Create</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header"><h5>Title</h5><a href="{{ route('task.index') }}" class="btn btn-dark text-right"><i class="fas fa-arrow-left"></i>Kembali</a></div>
            <div class="card-body">
                <form action="{{ route('task.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input type="text" name="title" id="title" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="content">Konten</label>
                        <textarea name="content" id="content" class="summernote" cols="130" rows="10"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="file">File</label>
                        <input type="file" class="dropify" data-allowed-file-extensions="jpg png jpeg docx doc xls ppt pdf" name="file">
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3 col-sm-12">Kelas Yang Dituju</label>
                        <div class="col-lg-6 col-md-9 col-sm-12">
                            <select name="type" id="" class="form-control select_type">
                                <option value="all">Semua</option>
                                <option value="class">Kelas</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-class">
                        <div class="form-group row">
                            <label class="col-form-label col-lg-3 col-sm-12">Kelas</label>
                            <div class="col-lg-6 col-md-9 col-sm-12">
                                <select name="class_group_filter[]" id="" class="kt_selectpicker form-control"
                                    multiple="multiple">
                                    @foreach($classgroup as $field)
                                    <option value="{{ $field->id }}">{{ $field->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="deadline_date">Batas Waktu</label>
                        <input type="date" name="deadline_date" id="deadline_date" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="with_task_collection">Dengan Pengumpulan Tugas</label>
                        <select name="with_task_collection" class="form-control kt_selectpicker">
                            <option value="ya">Ya</option>
                            <option value="tidak">Tidak</option>
                        </select>
                    </div>
                    <button class="btn btn-primary" type="submit">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    $(document).ready(function () {
        $('.form-class').hide();
        $('.select_type').change(function () {
            if ($(this).val() == "class") {
                $('.form-class').show();
            } else {
                $('.form-class').hide();
                $('.form-class').prop('selected', function () {
                    return this.defaultSelected;
                });
            }
        });
    });
</script>
@endsection
    