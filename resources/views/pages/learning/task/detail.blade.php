@extends('templates.temp')

@section('title','Detail Tugas')
@section('breadcrumb')
<a href="#" class="kt-subheader__breadcrumbs-link">
    Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('task.index') }}" class="kt-subheader__breadcrumbs-link">
    Tugas </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('task.show',$show) }}" class="kt-subheader__breadcrumbs-link">
    Detail Tugas </a>    
@endsection

@section('content')

<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Detail Tugas</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('task.index') }}"
                    class="btn btn-default btn-bold btn-upper btn-font-sm">
                    <i class="fas fa-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="col-md-10">
            <div class="form-group">
                <table class="table">
                    <tr>
                        <td>Judul Tugas</td>
                        <td>:</td>
                        <td>{{ $show->title }}</td>
                    </tr>
                    <tr>
                        <td>Deskripsi</td>
                        <td>:</td>
                        <td>{!! $show->content !!}</td>
                    </tr>
                    <tr>
                        <td>File</td>
                        <td>:</td>
                        <td><input type="file" class="dropify" disabled data-default-file="{{ asset('app/'.$show->file) }}" name="file"></td>
                    </tr>
                    <tr>
                        <td>Deadline</td>
                        <td>:</td>
                        <td>{{ $show->deadline_date }}</td>
                    </tr>
                </table>
            </div>
        </div>    
    </div>
    
@endsection