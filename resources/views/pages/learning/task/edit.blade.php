@extends('templates.temp')

@section('title','Task Edit')
@section('page_name','Task Edit')
@section('breadcrumb')
<li class="breadcrumb-item">Dashboard   </li>
<li class="breadcrumb-item"><a href="{{ route('task.index') }}">Tugas</a></li>
<li class="breadcrumb-item"><a href="{{ route('task.edit',$data) }}">Tugas Edit</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header"><h5>Title</h5><a href="{{ route('task.index') }}" class="btn btn-dark text-right"><i class="fas fa-arrow-left"></i>Kembali</a></div>
            <div class="card-body">
                <form action="{{ route('task.update',$data) }}" method="POST" enctype="multipart/form-data">
                    @csrf @method('patch')
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input type="text" name="title" id="title" value="{{ old('title',$data->title) }}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="content">Konten</label>
                        <textarea name="content" id="content" class="summernote" cols="130" rows="10">{{ old('content',$data->content) }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="file">File</label>
                        <input type="file" class="dropify" data-default-file="{{ asset('app/'.$data->file) }}" name="file">
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-3 col-sm-12">Kelas Yang Dituju</label>
                        <div class="col-lg-6 col-md-9 col-sm-12">
                            <select name="type" id="" class="form-control select_type">
                                @if($data->task_filter[0]->type == "all")
                                <option value="all" selected>Semua</option>
                                <option value="class">Kelas</option>
                                @else
                                <option value="all">Semua</option>
                                <option value="class" selected>Kelas</option>
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="form-class">
                        <div class="form-group row">
                            <label class="col-form-label col-lg-3 col-sm-12">Kelas</label>
                            <div class="col-lg-6 col-md-9 col-sm-12">
                                <select name="class_group_filter[]" id="" class="kt_selectpicker form-control"
                                    multiple="multiple">
                                    @foreach($classgroup as $field)
                                    <option value="{{ $field->id }}" {{ $field->is_selected }}>{{ $field->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="deadline_date">Batas Waktu</label>
                        <input type="date" name="deadline_date" id="deadline_date" class="form-control" value="{{ old('deadline_date',$data->deadline_date) }}">
                    </div>
                    <div class="form-group">
                        <label for="with_task_collection">Dengan Pengumpulan Tugas</label>
                        <select name="with_task_collection" class="form-control kt_selectpicker">
                            @if($data->with_task_collection == "ya")
                            <option value="ya" selected>Ya</option>
                            <option value="tidak">Tidak</option>
                            @else
                            <option value="ya">Ya</option>
                            <option value="tidak" selected>Tidak</option>
                            @endif
                        </select>
                    </div>
                    <button class="btn btn-primary" type="submit">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
@if($data->task_filter[0]->type == "all")
<script>
    $(document).ready(function(){
        $('.form-class').hide();
    });
</script>
@endif
<script>
    $(document).ready(function(){
        $('.select_type').change(function(){
            if($(this).val() == "class")
            {
                $('.form-class').show();
            }else{
                $('.form-class').hide();
                $('.form-class').prop('selected', function() {
                    return this.defaultSelected;
                });
            }
        });
    });
</script>
@endsection
    