@extends('templates.temp')

@section('title','Tugas')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('task.index') }}" class="kt-subheader__breadcrumbs-link">
    Tugas </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">Tugas</h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <a href="{{ route('task.create') }}"
                        class="btn btn-default btn-bold btn-upper btn-font-sm">
                        <i class="flaticon2-add-1"></i>
                        Buat Baru
                    </a>
                </div>
            </div>
        </div>

    <div class="kt-portlet__body">
        <!--begin: Datatable -->
        <table class="table data-table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Judul</th>
                    <th>Batas Waktu</th>
                    <th class="text-center">Aksi</th>
                </tr>
            </thead>

            <tbody>
                @foreach ($data as $datas)
                    <tr>
                        <td>{{ $loop->index + 1 }}</td>
                        <td>{{ $datas->title }}</td>
                        <td>{{ $datas->deadline_date }}</td>
                        <td nowrap class="text-center">
                            <a href="{{ route('task.show',$datas) }}" class="btn btn-secondary btn-elevate btn-circle btn-icon"> <i
                                class="la la-eye"></i> </a>
                            <a href="{{ route('task.destroy',$datas) }}" class="btn btn-secondary btn-elevate btn-circle btn-icon delete-btn"> <i
                                class="la la-trash"></i> </a>
                            <a href="{{ route('task.edit',$datas) }}" class="btn btn-secondary btn-elevate btn-circle btn-icon"> <i class="la la-pencil"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>

        </table>
        <!--end: Datatable -->
    </div>
</div>
@endsection