@extends('templates.temp')

@section('title','Tugas Siswa')

@section('content')

<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Kumpulan Tugas Siswa</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('student.dashboard') }}"
                    class="btn btn-default btn-bold btn-upper btn-font-sm">
                    <i class="fas fa-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">
        @if($count == (null))
            <div class="col-md-12">
                <h4 class="text-center">Tidak Ada Tugas</h4>
            </div>
        @else
        <div class="col-md-12">
            @php
                $i=0;
            @endphp
            @foreach($data as $field)
                @if($field->task->with_task_collection == "ya")
                    <div class="card">
                        <div class="card-header">
                            <img src="{{ asset('app/'.$field->teacher->photo) }}" alt="" width="40" height="40" style="border-radius: 80%">
                            <b>{{ $field->teacher->name }} - {{ $field->teacher->subject->name }}</b>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="kt-section kt-section--first">
                                        <div class="form-group">
                                            <h4>Deadline Tugas</label>
                                            <h4>{{ $field->task->deadline_date }}</h4>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Nama Tugas</label>
                                            <p>{{ $field->task->title }}</p>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Deskripsi Tugas</label>
                                            <p>{!! $field->task->content !!}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="kt-section kt-section--first">
                                        <h3 class="kt-section__title">File Tugas</h3>
                                        @if($ext[$i] == "docx" || $ext[$i] == "doc" || $ext[$i] == "xls" || $ext[$i] == "ppt")
                                            <a download="{{ $field->task->file }}" href="{{ asset('app/'.$field->task->file) }}" class="btn btn-info">Download File</a>
                                        @elseif($ext[$i] == "pdf")
                                            <embed src="{{ asset('app/'.$field->task->file) }}" type="" height="350" width="500">
                                        @elseif($ext[$i] == "jpg" || $ext[$i] == "jpeg" || $ext[$i] == "png")
                                            <input type="file" class="dropify" data-default-file="{{ asset('app/'.$field->task->file) }}" disabled>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <br><hr>
                            <a href="" class="btn btn-info">Submit Tugas</a>
                        </div>
                    </div>
                    <br>
                @else
                    <div class="card">
                        <div class="card-header">
                            <img src="{{ asset('app/'.$field->teacher->photo) }}" alt="" width="40" height="40" style="border-radius: 80%">
                            <b>{{ $field->teacher->name }} - {{ $field->teacher->subject->name }}</b>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="kt-section kt-section--first">
                                        <div class="form-group">
                                            <h4>Deadline Tugas</h4>
                                            <h4>{{ $field->task->deadline_date }}</h4>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Nama Tugas</label>
                                            <p>{{ $field->task->title }}</p>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Deskripsi Tugas</label>
                                            <p>{!! $field->task->content !!}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="kt-section kt-section--first">
                                        <h3 class="kt-section__title">File Tugas</h3>
                                        <embed src="{{ asset('app/'.$field->task->file) }}" type="" height="350" width="500">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                @endif
                @php
                    $i++;   
                @endphp
            @endforeach    
            <div class="row">
                <div class="offset-10 col-md-2">
                    {{ $data->links() }}
                </div>
            </div>
        </div>    
        
        @endif
    </div>
    
@endsection