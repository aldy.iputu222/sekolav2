@extends('templates.main')

@section('title','Catatan Ringan')
@section('page_name','Catatan Ringan')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="">Fitur Lainnya</a></li>
<li class="breadcrumb-item"><a href="">Catatan Ringan</a></li>
@endsection

@section('content')
<div class="row">
<div class="col-sm-12">
    <div class="card">
        <div class="card-block note-card">
            <div class="note-box-wrapper row">
                <div class="note-box-aside col-lg-12 col-xl-3">
                    <div class="row">
                        <div class="col-sm-12">
                            <form class="form-material">
                                <div class="material-group">
                                    <div class="form-group form-primary">
                                        <input type="text" name="task-insert" id="Note-search" class="form-control" placeholder="Search notes" required="">
                                        <span class="form-bar"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="notes-list">
                        <ul id="Note-list" class="Note-list list-group"></ul>
                    </div>
                </div>
                <div class="note-box-content col-lg-12 col-xl-9">
                    <div class="Note-header">
                        <div class="Note-created float-right">
                            <span class="Note-created__on d-block">Note Created On</span>
                            <span class="Note-created__date" id="Note-created__date"></span>
                        </div>
                        <a href="#!" class="btn btn-md btn-primary hidden-xs Note-add">Tambah Catatan</a>
                    </div>
                    <div class="note-body">
                        <div class="note-write">
                            <textarea id="Note-pad" class="form-control" placeholder="Tulis Disini" rows="10"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
@endsection
