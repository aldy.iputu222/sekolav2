@extends('templates.temp')

@section('title','Dashboard')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Admin </a>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-4 col-xl-4 order-lg-1 order-xl-1">
        <!--begin::Portlet-->
        <div class="kt-portlet kt-portlet--fit kt-portlet--height-fluid">
            <div class="kt-portlet__body kt-portlet__body--fluid">
                <div class="kt-widget-3 kt-widget-3--brand">
                    <div class="kt-widget-3__content">
                        <div class="kt-widget-3__content-info">
                            <div class="kt-widget-3__content-section">
                                <div class="kt-widget-3__content-title">Annual Profit</div>
                                <div class="kt-widget-3__content-desc">SAAS TOTAL PORIFT</div>
                            </div>
                            <div class="kt-widget-3__content-section">
                                <span class="kt-widget-3__content-bedge">$</span>
                                <span class="kt-widget-3__content-number">17<span>M</span></span>               
                            </div>
                        </div>  
                        <div class="kt-widget-3__content-stats">                
                            <div class="kt-widget-3__content-progress">
                                <div class="progress">
                                    <!-- Doc: A bootstrap progress bar can be used to show a user how far along it's in a process, see https://getbootstrap.com/docs/4.1/components/progress/ -->
                                    <div class="progress-bar bg-light" style="width: 72%;" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <div class="kt-widget-3__content-action">
                                <div class="kt-widget-3__content-text">CHANGE</div>
                                <div class="kt-widget-3__content-value">+72%</div>
                            </div>
                        </div>
                    </div>
                </div>      
            </div>
        </div>
        <!--end::Portlet--> </div>
            <div class="col-lg-4 col-xl-4 order-lg-1 order-xl-1">
                <!--begin::Portlet-->
        <div class="kt-portlet kt-portlet--fit kt-portlet--height-fluid">
            <div class="kt-portlet__body kt-portlet__body--fluid">
                <div class="kt-widget-3 kt-widget-3--danger">
                    <div class="kt-widget-3__content">
                        <div class="kt-widget-3__content-info">
                            <div class="kt-widget-3__content-section">
                                <div class="kt-widget-3__content-title">Annual Sales</div>
                                <div class="kt-widget-3__content-desc">TOTAL SALES AMOUNT</div>
                            </div>
                            <div class="kt-widget-3__content-section">  
                                <span class="kt-widget-3__content-number">920<span>K</span></span>              
                            </div>
                        </div>  

                        <div class="kt-widget-3__content-stats">                
                            <div class="kt-widget-3__content-progress">
                                <div class="progress">
                                    <!-- Doc: A bootstrap progress bar can be used to show a user how far along it's in a process, see https://getbootstrap.com/docs/4.1/components/progress/ -->
                                    <div class="progress-bar bg-light" style="width: 85%;" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <div class="kt-widget-3__content-action">
                                <div class="kt-widget-3__content-text">CHANGE</div>
                                <div class="kt-widget-3__content-value">+85%</div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
        <!--end::Portlet--> </div>
            <div class="col-lg-4 col-xl-4 order-lg-1 order-xl-1">
                <!--begin::Portlet-->
        <div class="kt-portlet kt-portlet--fit kt-portlet--height-fluid">
            <div class="kt-portlet__body kt-portlet__body--fluid">
                <div class="kt-widget-3 kt-widget-3--success">
                    <div class="kt-widget-3__content">
                        <div class="kt-widget-3__content-info">
                            <div class="kt-widget-3__content-section">
                                <div class="kt-widget-3__content-title">Response Rate</div>
                                <div class="kt-widget-3__content-desc">Support Quality</div>
                            </div>
                            <div class="kt-widget-3__content-section">
                                <span class="kt-widget-3__content-number">89<span>%</span></span>
                            </div>
                        </div>  

                        <div class="kt-widget-3__content-stats">
                            <div class="kt-widget-3__content-progress">
                                <div class="progress">
                                    <!-- Doc: A bootstrap progress bar can be used to show a user how far along it's in a process, see https://getbootstrap.com/docs/4.1/components/progress/ -->
                                    <div class="progress-bar bg-light" style="width: 45%;" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <div class="kt-widget-3__content-action">
                                <div class="kt-widget-3__content-text">CHANGE</div>
                                <div class="kt-widget-3__content-value">+45%</div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
        <!--end::Portlet--> 
    </div>
</div>

<div class="row">
    <div class="col-lg-6 col-xl-4 order-lg-1 order-xl-1">
        <!--begin::Portlet-->
        <div class="kt-portlet kt-portlet--height-fluid kt-widget-12">
            <div class="kt-portlet__body">
                <div class="kt-widget-12__body">
                    <div class="kt-widget-12__head">
                        <div class="kt-widget-12__date kt-widget-12__date--focus">
                            <span class="kt-widget-12__day">05</span>
                            <span class="kt-widget-12__month">Jan</span>
                        </div>
                        <div class="kt-widget-12__label">
                            <h3 class="kt-widget-12__title">Outdoor Activity Day</h3>
                            <span class="kt-widget-12__desc">South Suton</span>
                        </div>
                    </div>
                    <div class="kt-widget-12__info">
                        To start a blog, think of a topic about and first brainstorm party is ways to write details
                    </div>
                </div>
            </div>
            <div class="kt-portlet__foot kt-portlet__foot--md">
                <div class="kt-portlet__foot-wrapper">
                    <div class="kt-portlet__foot-info">
                        <div class="kt-widget-12__members">
                            <a href="general.html#" class="kt-widget-12__member" data-toggle="kt-tooltip" data-skin="brand" data-placement="top" title="" data-original-title="John Myer">
                                <img src="{{ asset('assets/media/users/300_5.jpg') }}" alt="image">
                            </a>
                            <a href="general.html#" class="kt-widget-12__member" data-toggle="kt-tooltip" data-skin="brand" data-placement="top" title="" data-original-title="Alison Brandy">
                                <img src="{{ asset('assets/media/users/300_16.jpg') }}" alt="image">
                            </a>
                            <a href="general.html#" class="kt-widget-12__member" data-toggle="kt-tooltip" data-skin="brand" data-placement="top" title="" data-original-title="Selina Cranson">
                                <img src="{{ asset('assets/media/users/300_11.jpg') }}" alt="image">
                            </a>
                            <a href="general.html#" class="kt-widget-12__member" data-toggle="kt-tooltip" data-skin="brand" data-placement="top" title="" data-original-title="Luke Walls">
                                <img src="{{ asset('assets/media/users/300_21.jpg') }}" alt="image">
                            </a>
                            <a href="general.html#" class="kt-widget-12__member" data-toggle="kt-tooltip" data-skin="brand" data-placement="top" title="" data-original-title="Micheal York">
                                <img src="{{ asset('assets/media/users/300_22.jpg') }}" alt="image">
                            </a>
                            <a href="general.html#" class="kt-widget-12__member kt-widget-12__member--last">
                                +7
                            </a>
                        </div>  
                    </div>
                    <div class="kt-portlet__foot-toolbar">
                        <a href="general.html#" class="btn btn-default btn-sm btn-bold btn-upper">Join</a>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Portlet-->

    </div>

    <div class="col-lg-6 col-xl-4 order-lg-1 order-xl-1">
        <!--begin::Portlet-->
        <div class="kt-portlet kt-portlet--height-fluid kt-widget ">
            <div class="kt-portlet__body">
                <div id="kt-widget-slider-13-1" class="kt-slider carousel slide" data-ride="carousel" data-interval="8000">
                    <div class="kt-slider__head">
                        <div class="kt-slider__label">Announcements</div>
                        <div class="kt-slider__nav">
                            <a class="kt-slider__nav-prev carousel-control-prev" href="general.html#kt-widget-slider-13-1" role="button" data-slide="prev">
                                <i class="fa fa-angle-left"></i>
                            </a>
                            <a class="kt-slider__nav-next carousel-control-next" href="general.html#kt-widget-slider-13-1" role="button" data-slide="next">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                    <div class="carousel-inner">
                        <div class="carousel-item kt-slider__body active">
                            <div class="kt-widget-13">
                                <div class="kt-widget-13__body">
                                    <a class="kt-widget-13__title" href="general.html#">Keen Admin Launch Day</a>
                                    <div class="kt-widget-13__desc">
                                        To start a blog, think of a topic about and first brainstorm party is ways to write details
                                    </div>
                                </div>
                                <div class="kt-widget-13__foot">
                                    <div class="kt-widget-13__label">
                                        <div class="btn btn-sm btn-label btn-bold">
                                            07 OCT, 2018
                                        </div> 
                                    </div>
                                    <div class="kt-widget-13__toolbar">
                                        <a href="general.html#" class="btn btn-default btn-sm btn-bold btn-upper">View</a>
                                    </div>                      
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item kt-slider__body">
                            <div class="kt-widget-13">
                                <div class="kt-widget-13__body">
                                    <a class="kt-widget-13__title" href="general.html#">Incredibly Positive Reviews</a>
                                    <div class="kt-widget-13__desc">
                                        To start a blog, think of a topic about and first brainstorm party is ways to write details
                                    </div>
                                </div>
                                <div class="kt-widget-13__foot">
                                    <div class="kt-widget-13__title">
                                        <div class="btn btn-sm btn-label btn-bold">
                                            17 NOV, 2018
                                        </div> 
                                    </div>
                                    <div class="kt-widget-13__toolbar">
                                        <a href="general.html#" class="btn btn-default btn-sm btn-bold btn-upper">View</a>
                                    </div>                      
                                </div>
                            </div>
                        </div>

                        <div class="carousel-item kt-slider__body">
                            <div class="kt-widget-13">
                                <div class="kt-widget-13__body">
                                    <a class="kt-widget-13__title" href="general.html#">Award Winning Theme</a>
                                    <div class="kt-widget-13__desc">
                                        To start a blog, think of a topic about and first brainstorm party is ways to write details
                                    </div>
                                </div>
                                <div class="kt-widget-13__foot">
                                    <div class="kt-widget-13__label">
                                        <div class="btn btn-sm btn-label btn-bold">
                                            03 SEP, 2018
                                        </div> 
                                    </div>
                                    <div class="kt-widget-13__toolbar">
                                        <a href="general.html#" class="btn btn-default btn-sm btn-bold btn-upper">View</a>
                                    </div>                      
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--end::Portlet--> 
    </div>  
</div>
@endsection
