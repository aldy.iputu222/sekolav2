@extends('templates.temp')

@section('title','Setting Soal')
@section('page_name','Setting Soal')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
Manajemen Ulangan </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="" class="kt-subheader__breadcrumbs-link">
    Setting Soal </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Setting Soal</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('subjectbab.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>
    <form class="kt-form kt-form--label-right" action="{{route('qbank.store')}}" method="POST" enctype="multipart/form-data">
        <div class="kt-portlet__body">
            @csrf
                    <div class="form-group row">
                        <label class="col-form-label col-lg-4 col-sm-12">Mata Pelajaran</label>
                        <div class="col-lg-4 col-md-9 col-sm-12">
                            <select name="subject_id" id="subject_id" class="form-control kt_selectpicker">
                                <option value="">Pilih Mata Pelajaran</option>
                            @foreach($subject as $show)
                                <option value="{{ $show->id }}">{{ $show->name }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-4 col-sm-12">Bab Mata Pelajaran</label>
                        <div class="col-lg-4 col-md-9 col-sm-12">
                            <select name="bab_id" id="bab_id" class="form-control">
                                <option></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-4 col-sm-12">Tingkat Kesulitan</label>
                        <div class="col-lg-4 col-md-9 col-sm-12">
                            <select name="difficulty" class="form-control kt_selectpicker">
                                <option value="Mudah">Mudah</option>
                                <option value="Sedang">Sedang</option>
                                <option value="Sulit">Sulit</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-4 col-sm-12">Tingkat Kesulitan</label>
                        <div class="col-lg-4 col-md-9 col-sm-12">
                            <select name="question_type" class="form-control kt_selectpicker">
                                <option value="PG">Pilihan Ganda</option>
                                <option value="ESSAY">Essay</option>
                                <option value="LISTENING">Listening</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-4 col-sm-12">Jumlah Soal</label>
                        <div class="col-lg-4 col-md-9 col-sm-12">
                                <input type="text" class="form-control" onkeypress="return isNumber(event)" name="question_total" id="nama" required value="{{ old('total') }}">
                        </div>
                    </div>
                    <div class="kt-portlet__foot text-center">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-12 col-sm-12"></div>
                                <div class="col-lg-12 col-sm-12">
                                    <button type="submit" class="btn btn-brand">Lanjutkan</button>
                                    <button type="reset" class="btn btn-secondary">Batal</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
@endsection
@section('script')
<script>
    $(document).ready(function(){
        $('#bab_id').attr('disabled','disabled');
        $('#subject_id').change(function(){
            $('#bab_id').removeAttr('disabled'); 
            var subject_id = $(this).val();
            $.post("{{ url('api/subject_bab/get_bab') }}",{
                'subject_id': subject_id,
            },function(data){
                var options = '<option value="">Pilih Bab</option>';
                $(data).each(function(index,value){
                    options += '<option value="'+value.id+'">'+value.bab+'</option>';
                });

                $('#bab_id').html(options);
            });
        });
    });
</script>
@endsection