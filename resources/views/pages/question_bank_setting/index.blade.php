@extends('templates.temp')

@section('title','Bank Soal')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Manajemen Ulangan </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('qbank.index') }}" class="kt-subheader__breadcrumbs-link">
    Bank Soal </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">Bab Mata Pelajaran</h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <a href="{{ route('qbank.create') }}"
                        class="btn btn-default btn-bold btn-upper btn-font-sm">
                        <i class="flaticon2-add-1"></i>
                        Buat Baru
                    </a>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
                <table class="table data-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Pelajaran</th>
                            <th>Bab</th>
                            <th>Kesulitan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($questionbank as $all)
                        <tr>
                            <td>{{ $loop->index + 1 }}</td>
                            <td>{{$all->subject->name}}</td>
                            <td>{{$all->bab->bab}}</td>
                            <td>{{$all->difficulty}}</td>
                            <td nowrap class="text-center">
                                <a href="{{ route('qbank.destroy',$all->id) }}" class="btn btn-secondary btn-elevate btn-circle btn-icon delete-btn"><i class="la la-trash"></i></a>
                                <a href="{{ route('qbank.edit',$all->id) }}" class="btn btn-secondary btn-elevate btn-circle btn-icon"><i class="la la-edit"></i></a>
                                <a href="{{ route('qbank.show',$all->id) }}" class="btn btn-secondary btn-elevate btn-circle btn-icon"><i class="la la-eye"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
@endsection
