@extends('templates.temp')

@section('title','Buat Soal')
@section('page_name','Buat Soal')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
Manajemen Ulangan </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('qbank.create') }}" class="kt-subheader__breadcrumbs-link">
    Setting Soal </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="" class="kt-subheader__breadcrumbs-link">
    Buat Soal </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Buat Soal</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
            </div>
        </div>
    </div>
    <form class="kt-form kt-form--label-right" action="{{route('teacher.addquestion')}}" method="POST" enctype="multipart/form-data">
        <div class="kt-portlet__body">
            @csrf
                    <p>Sisa Soal Yang Harus Diinput : {{ session::get('session_question_total') }}</p>
                    <hr>
                <!-- PILIHAN GANDA -->
                @if($question_type == "PG")
                <div class="col-sm-12 form-pg">
                        <div class="form-group row">
                                <label class="col-form-label col-lg-2 col-sm-12">Soal</label>
                                <div class="col-lg-8 col-md-9 col-sm-12">
                                        <textarea name="question" id="" class="summernote" cols="30" rows="10">{{ old('question') }}</textarea>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-form-label col-lg-2 col-sm-12">Jawaban A</label>
                                <div class="col-lg-8 col-md-9 col-sm-12">
                                        <textarea name="j_a" id="" class="summernote" cols="30" rows="10">{{ old('j_a') }}</textarea>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-form-label col-lg-2 col-sm-12">Jawaban B</label>
                                <div class="col-lg-8 col-md-9 col-sm-12">
                                        <textarea name="j_b" id="" class="summernote" cols="30" rows="10">{{ old('j_b') }}</textarea>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-form-label col-lg-2 col-sm-12">Jawaban C</label>
                                <div class="col-lg-8 col-md-9 col-sm-12">
                                        <textarea name="j_c" id="" class="summernote" cols="30" rows="10">{{ old('j_c') }}</textarea>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-form-label col-lg-2 col-sm-12">Jawaban D</label>
                                <div class="col-lg-8 col-md-9 col-sm-12">
                                        <textarea name="j_d" id="" class="summernote" cols="30" rows="10">{{ old('j_d') }}</textarea>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-form-label col-lg-2 col-sm-12">Jawaban E</label>
                                <div class="col-lg-8 col-md-9 col-sm-12">
                                        <textarea name="j_e" id="" class="summernote" cols="30" rows="10">{{ old('j_e') }}</textarea>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-form-label col-lg-2 col-sm-12">Jawaban Benar</label>
                                <div class="col-lg-8 col-md-9 col-sm-12">
                                        <select name="true_answer" style="z-index:1;" class="form-control kt_selectpicker">
                                        <option value="">Pilih Jawaban</option>
                                        <option value="A">A</option>
                                        <option value="B">B</option>
                                        <option value="C">C</option>
                                        <option value="D">D</option>
                                        <option value="E">E</option>
                                        </select>
                                </div>
                        </div>
                    @elseif($question_type == "ESSAY")
                    <div class="form-group row">
                                <label class="col-form-label col-lg-2 col-sm-12">Soal</label>
                                <div class="col-lg-8 col-md-9 col-sm-12">
                                        <textarea name="question" id="" class="summernote" cols="30" rows="10">{{ old('question') }}</textarea>
                                </div>
                    </div>
                    @elseif($question_type == "LISTENING")
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 col-sm-12">File Soal</label>
                        <div class="col-lg-8 col-md-9 col-sm-12">
                                <input type="file" name="file" id="photo" data-allowed-file-extensions="mp3" class="dropify" data-height="250">
                        </div>
                    </div>
                    <div class="form-group row">
                                <label class="col-form-label col-lg-2 col-sm-12">Soal</label>
                                <div class="col-lg-8 col-md-9 col-sm-12">
                                        <textarea name="question" id="" class="summernote" cols="30" rows="10">{{ old('question') }}</textarea>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-form-label col-lg-2 col-sm-12">Jawaban A</label>
                                <div class="col-lg-8 col-md-9 col-sm-12">
                                        <textarea name="j_a" id="" class="summernote" cols="30" rows="10">{{ old('j_a') }}</textarea>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-form-label col-lg-2 col-sm-12">Jawaban B</label>
                                <div class="col-lg-8 col-md-9 col-sm-12">
                                        <textarea name="j_b" id="" class="summernote" cols="30" rows="10">{{ old('j_b') }}</textarea>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-form-label col-lg-2 col-sm-12">Jawaban C</label>
                                <div class="col-lg-8 col-md-9 col-sm-12">
                                        <textarea name="j_c" id="" class="summernote" cols="30" rows="10">{{ old('j_c') }}</textarea>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-form-label col-lg-2 col-sm-12">Jawaban D</label>
                                <div class="col-lg-8 col-md-9 col-sm-12">
                                        <textarea name="j_d" id="" class="summernote" cols="30" rows="10">{{ old('j_d') }}</textarea>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-form-label col-lg-2 col-sm-12">Jawaban E</label>
                                <div class="col-lg-8 col-md-9 col-sm-12">
                                        <textarea name="j_e" id="" class="summernote" cols="30" rows="10">{{ old('j_e') }}</textarea>
                                </div>
                        </div>
                        <div class="form-group row">
                                <label class="col-form-label col-lg-2 col-sm-12">Jawaban Benar</label>
                                <div class="col-lg-8 col-md-9 col-sm-12">
                                        <select name="true_answer" class="form-control kt_selectpicker">
                                        <option value="">Pilih Jawaban</option>
                                        <option value="A">A</option>
                                        <option value="B">B</option>
                                        <option value="C">C</option>
                                        <option value="D">D</option>
                                        <option value="E">E</option>
                                        </select>
                                </div>
                        </div>
                    @else
                    <h4>TIPE SOAL TIDAK DIKETAHUI</h4>
                    @endif
                    <div class="kt-portlet__foot text-center">
                        <div class="kt-form__actions">
                                <div class="row">
                                <div class="col-lg-12 col-sm-12"></div>
                                <div class="col-lg-12 col-sm-12">
                                        <button type="submit" class="btn btn-brand">Simpan</button>
                                        <button type="reset" class="btn btn-secondary">Batal</button>
                                </div>
                                </div>
                        </div>
                    </div>
                </form>
            </div>
@endsection
