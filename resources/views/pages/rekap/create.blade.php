@extends('templates.temp')

@section('title','Formulir Rekap Sikap')
@section('page_name','Formulir Rekap Sikap')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('attitudeTransaction.index') }}" class="kt-subheader__breadcrumbs-link">
    Rekap Sikap </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Guru</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('attitudeTransaction.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>

    <form class="kt-form kt-form--label-right" method="POST" action="{{ route('attitudeTransaction.store') }}" enctype="multipart/form-data">
        @csrf
        <div class="kt-portlet__body">


            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Jenis Kegiatan</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <select name="attitude_id" id="" class="kt_selectpicker form-control">
                        @foreach($att as $att)
                        <option value="{{ $att->id }}">{{$att->name}} ( Score : {{ $att->point}} )</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
            	 <label class="col-form-label col-lg-4 col-sm-12">Siswa Yang Bersangkutan</label>
            	  <div class="col-lg-4 col-md-9 col-sm-12">
                    <select name="user_id" id="" class="kt_selectpicker form-control">
                        @foreach($ybs as $ybs)
                        <option value="{{ $ybs->id }}">{{ $ybs->name}}  &nbsp;  {{ $ybs->classgroup->name}} </option>
                        @endforeach
                    </select>
                </div>
            </div>
            	
             <div class="form-group row">
            	<label class="col-form-label col-lg-4 col-sm-12">Saksi</label>
            	 <div class="col-lg-4 col-md-9 col-sm-12">
                   <input type="text" class="form-control" name="teacher_id" value="{{Auth::guard('web_teacher')->user()->name}}" disabled="">
                </div>
            </div>
            
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-4 col-sm-12"></div>
                    <div class="col-lg-8 col-sm-12">
                        <button type="submit" class="btn btn-brand">Simpan</button>
                        <button type="reset" class="btn btn-secondary">Batal</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
