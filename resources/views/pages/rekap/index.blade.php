@extends('templates.temp')

@section('title','Attitude Transaction')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    CMS Sekolah </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('attitudeTransaction.index') }}" class="kt-subheader__breadcrumbs-link">
    Attitude Transaction </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">Attitude Transaction</h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <a href="{{ route('attitudeTransaction.create') }}"
                        class="btn btn-default btn-bold btn-upper btn-font-sm">
                        <i class="flaticon2-add-1"></i>
                        Buat Baru
                    </a>
                </div>
            </div>
        </div>

    <div class="kt-portlet__body">
                <table class="table data-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Siswa</th>
                            <th>Jenis Kegiatan</th>
                            <th>Kode Kegiatan</th>
                            <th>Saksi</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $show)
                        <tr>
                            <td>{{$loop->index+1}}</td>
                            <td>{{$show->student->name}}</td>
                            <td>{{$show->attitude->name}}</td>
                            <td>{{$show->attitude->code}}</td>
                            <td>Bapak {{$show->teacher->name}}</td>


                            <td nowrap class="text-center">
                                <a href="{{ route('attitudeTransaction.destroy',$show) }}" class="btn btn-secondary btn-elevate btn-circle btn-icon delete-btn"><i class="la la-trash"></i></a>
                                <a href="{{ route('attitudeTransaction.edit',$show) }}" class="btn btn-secondary btn-elevate btn-circle btn-icon"><i class="la la-edit"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
@endsection
