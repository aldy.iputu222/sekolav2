@extends('templates.temp')

@section('title','Buat Tahun Ajar')
@section('page_name','Buat Tahun Ajar')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('school_year.index') }}" class="kt-subheader__breadcrumbs-link">
    Tahun Ajar </a>
<a href="" class="kt-subheader__breadcrumbs-link">
    Buat Tahun Ajar </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Tahun Ajar</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('school_year.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>

    <form class="kt-form kt-form--label-right" method="POST" action="{{ route('school_year.store') }}"
        enctype="multipart/form-data">
        @csrf
        <div class="kt-portlet__body">
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Tahun Awal</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <select name="first_year" id="first_year" onchange="setLastYear()" class="form-control">
                        <option  selected disabled>----Pilih----</option>
                        <?php
                            $tahun = date("Y");
                            for($i=$tahun+6; $i>=$tahun; $i--) : 
                        ?>
                        <option value="{{$i}}">{{$i}}</option>
                        <?php endfor; ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Tahun Akhir</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="text" name="last_year" id="last_year" class="form-control" required readonly
                        value="{{ old('last_year') }}" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Status</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <select name="status" id="" class="form-control">
                        <option value="active">Aktif</option>
                        <option value="not_active">Tidak Aktif</option>
                    </select>
                </div>
            </div>

        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-4 col-sm-12"></div>
                    <div class="col-lg-8 col-sm-12">
                        <button type="submit" class="btn btn-brand">Simpan</button>
                        <button type="reset" class="btn btn-secondary">Batal</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@section('script')
<script>
// $(document).ready(function(){
//     var first_year = $('#first_year').value();
//     var last_year = first_year + 3 ;

// });

function setLastYear() {
    var first_year = document.getElementById('first_year').value;
    var last_year =  parseInt(first_year)  + 2 ;
    document.getElementById('last_year').value = last_year;
    // alert('haha');
}
</script>


@endsection