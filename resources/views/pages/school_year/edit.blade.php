@extends('templates.temp')

@section('title','Edit Tahun Ajar')
@section('page_name','Edit Tahun Ajar')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('school_year.index') }}" class="kt-subheader__breadcrumbs-link">
    Tahun Ajar </a>
<a href="" class="kt-subheader__breadcrumbs-link">
    Edit Tahun Ajar </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Tahun Ajar</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('school_year.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>

    <form class="kt-form kt-form--label-right" method="POST" action="{{ route('school_year.update', $data->id) }}"
        enctype="multipart/form-data">
        @method('PATCH')
        @csrf
        <div class="kt-portlet__body">
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Tahun Awal</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <select name="first_year" id="" class="form-control">
                        <?php
                            $tahun = date("Y");
                            for($i=$tahun+6; $i>=$tahun; $i--) : 
                        ?>
                        <option value="{{$i}}" <?php if($data->first_year == $i){echo"selected";} ?> >{{$i}}</option>
                        <?php endfor; ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Tahun Akhir</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                <select name="last_year" id="" class="form-control">
                        <?php
                            $tahun = date("Y");
                            for($i=$tahun+6; $i>=$tahun; $i--) : 
                        ?>
                        <option value="{{$i}}" <?php if($data->last_year == $i){echo"selected";} ?>>{{$i}}</option>
                        <?php endfor; ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Status</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <select name="status" id="" class="form-control">
                        <option value="active" <?php if($data->status == "active"){echo"selected";} ?>>Aktif</option>
                        <option value="not_active" <?php if($data->status == "not_active"){echo"selected";} ?>>Tidak Aktif</option>
                    </select>
                </div>
            </div>

        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-4 col-sm-12"></div>
                    <div class="col-lg-8 col-sm-12">
                        <button type="submit" class="btn btn-brand">Simpan</button>
                        <button type="reset" class="btn btn-secondary">Batal</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection