@extends('templates.temp')

@section('title','Tahun Ajar')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('school_year.index') }}" class="kt-subheader__breadcrumbs-link">
    Tahun Ajar </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Tahun Ajar</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('school_year.create') }}" class="btn btn-brand btn-bold btn-upper btn-font-sm">
                    <i class="flaticon2-add-1"></i>
                    Buat Baru
                </a>
            </div>
        </div>
    </div>

    <div class="kt-portlet__body">
        <!--begin: Datatable -->
        <table class="table data-table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Tahun Awal</th>
                    <th>Tahun Akhir</th>
                    <th>status</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $no => $field )
                <tr>
                    <td>{{$no+1}}</td>
                    <td>{{$field->first_year}}</td>
                    <td>{{$field->last_year}}</td>
                    <td>
                        <?php if ( $field->status == "active") { ?>
                            <span class="badge badge-success">{{$field->status}}</span>
                        <?php } if( $field->status == "not_active" ) {?>
                            <span class="badge badge-danger">{{$field->status}}</span>
                        <?php } ?>
                            
                    </td>
                    <td class="text-center">
                        <a href="{{ route('school_year.destroy',$field->id) }}"
                            class="btn btn-secondary btn-elevate btn-circle btn-icon delete-btn"> <i
                                class="la la-trash"></i> </a>
                        <a href="{{ route('school_year.edit',$field->id) }}"
                            class="btn btn-secondary btn-elevate btn-circle btn-icon"> <i class="la la-edit"></i>
                        </a>
                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>
        <!--end: Datatable -->
    </div>
</div>
@endsection