@extends('templates.temp')

@section('title','Penilaian')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
  Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('score.index') }}" class="kt-subheader__breadcrumbs-link">
  Penilaian </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
  <div class="kt-portlet__head kt-portlet__head--lg">
    <div class="kt-portlet__head-label">
        <h3 class="kt-portlet__head-title">Penilaian</h3>
    </div>
    <div class="kt-portlet__head-toolbar">
        <div class="kt-portlet__head-wrapper">
            <a href="{{ route('score.index') }}" class="btn btn-default btn-bold btn-upper btn-font-sm">
            <i class="fa fa-arrow-left"></i>
            Kembali
            </a>
        </div>
    </div>
  </div>

  <div class="kt-portlet__body">
    <form action="{{ route('score.store') }}" method="post">
        @csrf
        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                <label for="">Pilih Jurusan</label>
                <select name="jurusan" id="select_jurusan" class="kt_selectpicker form-control">
                    <option value="" selected="">Pilih Jurusan</option>
                    @foreach($jurusan as $field)
                    <option value="{{ $field->id }}">{{ $field->name }}</option>
                    @endforeach
                </select>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                <label for="">Pilih Rombel</label>
                <select name="" id="select_rombel" class="form-control">
                    <option value=""></option>
                </select>
                </div>
            </div>
        </div>
        <div class="row">    
            <div class="col-md-5">
                <div class="form-group">
                    <label for="">Type Penilaian</label>
                    <select name="type" id="type" class="form-control">
                        <option value="tugas">Tugas</option>
                        <option value="remedial">Remedial</option>
                    </select>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label for="">Judul Penilaian</label>
                    <input type="text" name="title" class="form-control">
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
            <div id="detail_siswa"></div>
        </div>
        </div>
    </form>    
  </div>
</div>
@endsection
@section('script')
    <script>
        $('#select_rombel').attr("disabled","disabled");
        $('#select_jurusan').change(function(){
            $('#select_rombel').html("");
            let value = $(this).val();
            $('#select_rombel').removeAttr("disabled");
            if(value != null)
            {
                let options = '<option value="">Pilih Rombel</option>';
                $.get("{{ url('api/ajax_score/rombel') }}" + "/" + value , function(data) {
                    console.log(data);
                    $(data).each(function(index,value) {
                        options += '<option value=" ' + value.id + ' ">'+ value.name +'</option>'
                    });
                    $('#select_rombel').html(options);
                });
            }
        });
        $('#select_rombel').change(function () {
            var value = $('#select_rombel').val();
            $.get("{{ url('api/ajax_score/detail_siswa') }}" + "/" + value, function(data) {
                console.log(value);
                $('#detail_siswa').html(data);
            });
        });
    </script>    
@endsection
