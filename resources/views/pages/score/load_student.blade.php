@if($count == (null))
<table class="table table-bordered">
    <thead>
        <tr>
            <th>No</th>
            <th>NIS</th>
            <th>Nama</th>
            <th>Nilai</th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td colspan="4" align="center">Tidak Ada Data Siswa</td>
        </tr>
    </tbody>
</table>
@else
    <div class="col-md-12">
        <div class="col-md-6"></div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>No</th>
                    <th>NIS</th>
                    <th>Nama</th>
                    <th>Nilai</th>
                </tr>
            </thead>

            <tbody>
                @foreach($siswa as $field)
                <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $field->nis }}</td>
                    <td>{{ $field->name }}</td>
                    <td>
                        <input type="text" name="score[]" class="form-control" value="0">
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @foreach ($rombel as $data)
            <input type="hidden" name="rombel_id" value="{{ $data->id }}">    
        @endforeach
        
        <button class="btn btn-primary">Simpan</button>
    </div> 
@endif