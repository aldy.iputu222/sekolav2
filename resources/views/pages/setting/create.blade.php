@extends('templates.temp')

@section('title','Formulir Sekolah')
@section('page_name','Formulir Sekolah')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="" class="kt-subheader__breadcrumbs-link">
    Formulir Sekolah </a>
@endsection

@section('content')

<div class="row">
    <div class="col-lg-12">
        <!--begin::Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">Formulir Sekolah</h3>
                </div>
            </div>
            <!--begin::Form-->
            <form class="kt-form kt-form--label-right" action="{{ route('setting.store') }}" method="post">
                @csrf
                <div class="kt-portlet__body">
                    <div class="form-group row">
                        <label for="example-text-input" class="col-3 col-form-label">Nama :</label>
                        <div class="col-6">
                            <input type="text" class="form-control" name="name" placeholder="Masukkan nama">
                            <span class="form-text text-muted">Please enter your full name</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-3">Tingkat</label>
                        <div class="col-6">
                            <select name="grade" id="" class="form-control">
                                <option value="sd">SD</option>
                                <option value="smp">SMP</option>
                                <option value="sma">SMA</option>
                                <option value="smk">SMK</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-3 col-form-label">No. Telepon</label>
                        <div class="col-6">
                            <input type="number" name="phone" class="form-control" placeholder="Masukkan nomor">
                            <span class="form-text text-muted">Please enter your full name</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-3 col-form-label">No. Telepon</label>
                        <div class="col-6">
                            <input type="number" name="phone" class="form-control" placeholder="Masukkan nomor">
                            <span class="form-text text-muted">Please enter your full phone number</span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="example-text-input" class="col-3 col-form-label">Logo</label>
                        <div class="col-6">
                            <input type="file" name="logo" class="dropify">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="example-text-input" class="col-3 col-form-label">Alamat</label>
                        <div class="col-6">
                            <textarea name="address" id="" class="form-control"></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="example-text-input" class="col-3 col-form-label">Info</label>
                        <div class="col-6">
                            <textarea name="about" id="" class="summernote"></textarea>
                        </div>
                    </div> 
                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions kt-form__actions--right">
                        <div class="row">
                            <div class="col-lg-9 offset-lg-3">
                                <div class="kt-align-left">
                                    <button type="submit" class="btn btn-brand">Submit</button>
                                    <button type="reset" class="btn btn-secondary">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!--end::Form-->
        </div>
        <!--end::Portlet-->
    </div>
</div>
@endsection
