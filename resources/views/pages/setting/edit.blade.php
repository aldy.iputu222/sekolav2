@extends('templates.temp')

@section('title','Ubah Data Sekolah')
@section('page_name','Ubah Data Sekolah')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="">Data Master</a></li>
<li class="breadcrumb-item"><a href="">Sekolah</a></li>
<li class="breadcrumb-item"><a href="">Ubah Data Sekolah</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header"><h5>Ubah Data Sekolah</h5><a href="" class="btn btn-dark text-right"><i class="fas fa-arrow-left"></i>Kembali</a></div>
            <div class="card-body">
                <form action="{{ route('setting.update',$data) }}" method="POST" enctype="multipart/form-data">
                    @csrf @method('patch')
                    <div class="form-group">
                        <label for="">Nama</label>
                        <input type="text" class="form-control" name="name" value="{{ old('name',$data->grade) }}">
                    </div>

                    <div class="form-group">
                        <label for="">Tingkat</label>
                        <select id="" class="select2" name="grade">
                            @if($data->grade == "sd")
                            <option value="sd" selected>SD</option>
                            <option value="smp">SMP</option>
                            <option value="sma">SMA</option>
                            <option value="smk">SMK</option>
                            @elseif($data->grade == "smp")
                            <option value="sd">SD</option>
                            <option value="smp" selected>SMP</option>
                            <option value="sma">SMA</option>
                            <option value="smk">SMK</option>
                            @elseif($data->grade == "sma")
                            <option value="sd">SD</option>
                            <option value="smp">SMP</option>
                            <option value="sma" selected>SMA</option>
                            <option value="smk">SMK</option>
                            @elseif($data->grade == "smk")
                            <option value="sd">SD</option>
                            <option value="smp">SMP</option>
                            <option value="sma">SMA</option>
                            <option value="smk" selected>SMK</option>
                            @endif
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">Phone</label>
                        <input type="number" class="form-control" name="phone" value="{{ old('phone',$data->phone) }}">
                    </div>

                    <div class="form-group">
                        <label for="">Logo</label>
                        <input type="file" class="dropify" name="logo" data-default-file="{{ asset('app/'.$data->logo) }}">
                    </div>
                    
                    <div class="form-group">
                        <label for="">Address</label>
                        <textarea name="address" id="" class="form-control" rows="10">{{ $data->address }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="">About</label>
                        <textarea name="about" id="" class="ck-editor" rows="10">{{ $data->about }}</textarea>
                    </div>

                    <button class="btn btn-primary">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
