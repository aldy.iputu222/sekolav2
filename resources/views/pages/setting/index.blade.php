@extends('templates.temp')

@section('title','Sekolah')
@section('page_name','Sekolah')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="">Data Master</a></li>
<li class="breadcrumb-item"><a href="">Sekolah</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5>Sekolah</h5><a href="{{ route('setting.edit',$data) }}" class="btn btn-primary text-right"><i class="fa fa-edit"></i>Edit</a>
            </div>
            <div class="card-body">
               <div class="form-group">
                    <h4>Nama</h4>
                    <p>{{ $data->name }}</p>
               </div>
               <hr>
               <div class="form-group">
                    <h4>Tingkat</h4>
                    <p>{{ strtoupper($data->grade) }}</p>
                </div>
                <hr>
               <div class="form-group">
                    <h4>Phone</h4>
                    <p>{{ $data->phone }}</p>
               </div>
               <hr>
               <div class="form-group">
                    <h4>Address</h4>
                    <p>{{ $data->address }}</p>
                </div>
                <hr>
                <div class="form-group">
                    <h4>About</h4>
                    <p>{!! $data->about !!}</p>
                </div>
                <hr>
                <div class="form-group">
                    <h4>Logo</h4>
                    <input type="file" disabled class="dropify" data-default-file="{{ asset('app/'.$data->logo) }}">
                </div>
                <hr>
            </div>
        </div>
    </div>
</div>
@endsection
