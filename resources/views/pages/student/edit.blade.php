@extends('templates.temp')

@section('title','Ubah Siswa')
@section('page_name','Ubah Siswa')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('student.index') }}" class="kt-subheader__breadcrumbs-link">
    Siswa </a>
<a href="" class="kt-subheader__breadcrumbs-link">
Ubah Siswa </a>
@endsection

@section('content')
@php
$religion = ['islam','kristen','hindu','buddha'];
@endphp
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Ubah Siswa</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('teacher.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>

    <form class="kt-form kt-form--label-right" method="POST" action="{{ route('student.update',$data) }}" enctype="multipart/form-data">
        @csrf @method('put')
        <div class="kt-portlet__body">
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">NIS</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="number" name="nis" class="form-control"  required placeholder="Contoh : 11605524" value="{{ old('nis',$data->nis) }}" disabled />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Nama</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                        <input type="text" class="form-control" name="name" id="nama" required placeholder="Contoh : Ujang Sutisna" value="{{ old('name',$data->name) }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Foto</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="file" name="photo" id="photo" data-allowed-file-extensions="png jpg jpeg" class="dropify" data-height="250" data-default-file="{{ asset('app/'.$data->photo) }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Alamat</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                        <textarea name="address" class="form-control" id="alamat" cols="30" rows="10" placeholder="Contoh : Jln. Suharso No. 968, Manado 46715, Riau">{{ old('address',$data->address) }}</textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Jenis Kelamin</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <select name="gender" class="form-control kt_selectpicker">
                        @if($data->gender == "L")
                        <option value="L" selected>Laki-Laki</option>
                        <option value="P">Perempuan</option>
                        @else
                        <option value="L">Laki-Laki</option>
                        <option value="P" selected>Perempuan</option>
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Tanggal Lahir</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="date" class="form-control" placeholder="Select date" name="date_of_birth" value="{{ old('date_of_birth',$data->date_of_birth) }}" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Agama</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <select name="region" id="" class="kt_selectpicker form-control">
                        @foreach($religion as $religion_field)
                        @if($data->religion == $religion_field)
                        <option value="{{ $religion_field }}" selected>{{ $religion_field }}</option>
                        @else
                        <option value="{{ $religion_field }}">{{ $religion_field }}</option>
                        @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">No.Telp</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="number" class="form-control" name="phone" id="no_tlp" placeholder="08971225785" value="{{ old('phone',$data->phone) }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Tahun Ajar</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <select name="school_year_id" id="school_year_select" class="form-control kt_selectpicker">
                        <option value="">Pilih Tahun Ajar</option>
                        @foreach($school_year as $field)
                        @if($field->id == $data->school_year_id)
                        <option value="{{ $field->id }}" selected>{{ $field->first_year }} - {{ $field->last_year }}</option>
                        @else
                        <option value="{{ $field->id }}">{{ $field->first_year }} - {{ $field->last_year }}</option>
                        @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Kelas</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <select name="class_group_id" id="classgroup_select" class="form-control" data-live-search="true">
                        <option value="">Pilih Kelas</option>
                    </select>
                </div>
            </div>
            
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-4 col-sm-12"></div>
                    <div class="col-lg-8 col-sm-12">
                        <button type="submit" class="btn btn-brand">Simpan</button>
                        <button type="reset" class="btn btn-secondary">Batal</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function(){
        $('#school_year_select').change(function(){
            var school_year = $(this).val();
            $.post("{{ url('api/school_year/get_class') }}",{
                'school_year_id': school_year,
            },function(data){
                var options = '<option value="">Pilih Kelas</option>';
                $(data).each(function(index,value){
                    options += '<option value="'+value.id+'">'+value.name+'</option>';
                });
                $('#classgroup_select').html(options);
            });
        });

        prepare();

        function prepare()
        {
            $.post("{{ url('api/school_year/get_class') }}",{
                'school_year_id': "{{ $data->school_year_id }}",
            },function(data){
                var options = '<option value="">Pilih Kelas</option>';
                $(data).each(function(index,value){
                    if(value.id == "{{ $data->class_group_id }}")
                    {
                        options += '<option value="'+value.id+'" selected>'+value.name+'</option>';
                    }else{
                        options += '<option value="'+value.id+'">'+value.name+'</option>';
                    }
                });
                $('#classgroup_select').html(options);
            });
        }
    });
</script>
@endsection
