@extends('templates.temp')

@section('title','Siswa')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('student.index') }}" class="kt-subheader__breadcrumbs-link">
    Siswa </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Siswa</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('student.create') }}" class="btn btn-brand btn-bold btn-upper btn-font-sm">
                    <i class="flaticon2-add-1"></i>
                    Buat Baru
                </a>
            </div>
        </div>
    </div>

    <div class="kt-portlet__body">
        <!--begin: Datatable -->
        <table class="table  data-table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nis</th>
                    <th>Name</th>
                    <th>Jenis Kelamin</th>
                    <th>Kelas</th>
                    <th>Tahun Ajar</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $field)
                <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $field->nis }}</td>
                    <td>{{ $field->name }}</td>
                    <td>{{ $field->gender }}</td>
                    <td>{{ $field->classgroup->name }}</td>
                    <td>{{ $field->school_year->first_year }} - {{ $field->school_year->last_year }}</td>
                    <td class="text-center">
                        <a href="{{ route('student.show',$field) }}"
                            class="btn btn-secondary btn-elevate btn-circle btn-icon"> <i class="la la-eye"></i> </a>
                        <a href="{{ route('student.destroy',$field) }}"
                            class="btn btn-secondary btn-elevate btn-circle btn-icon delete-btn"> <i
                                class="la la-trash"></i> </a>
                        <a href="{{ route('student.edit',$field) }}"
                            class="btn btn-secondary btn-elevate btn-circle btn-icon"> <i class="la la-edit"></i> </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <!--end: Datatable -->
    </div>
</div>
@endsection