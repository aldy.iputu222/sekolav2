@extends('templates.temp')

@section('title','Mata Pelajaran')
@section('breadcrumb')
<a href="#" class="kt-subheader__breadcrumbs-link">
    Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Mata Pelajaran</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('subject.create') }}" class="btn btn-default btn-bold btn-upper btn-font-sm">
                    <i class="flaticon2-add-1"></i>
                    Buat Baru
                </a>
            </div>
        </div>
    </div>

    <div class="kt-portlet__body">
        <!--begin: Datatable -->
        <table class="table data-table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Pelajaran</th>
                    <th>KKM</th>
                    <th>Alias</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach( $data as $field )
                <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{$field->name}}</td>
                    <td>{{$field->kkm}}</td>
                    <td>{{$field->alias}}</td>
                    <td nowrap class="text-center">
                        <a href="{{ route('subject.destroy',$field) }}"
                            class="btn btn-secondary btn-elevate btn-circle btn-icon delete-btn"> <i
                                class="la la-trash"></i> </a>
                        <a href="{{ route('subject.edit',$field) }}"
                            class="btn btn-secondary btn-elevate btn-circle btn-icon"> <i class="la la-edit"></i>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <!--end: Datatable -->
    </div>
</div>
@endsection