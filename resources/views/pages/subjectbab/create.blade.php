@extends('templates.temp')

@section('title','Buat Jumlah Bab')
@section('page_name','Buat Jumlah Bab')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
CMS Sekolah </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('subjectbab.index') }}" class="kt-subheader__breadcrumbs-link">
    Bab Mata Pelajaran </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('subjectbab.create') }}" class="kt-subheader__breadcrumbs-link">
    Buat Jumlah Bab </a>
@endsection


@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Buat Jumlah Bab</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('subjectbab.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>

    <form class="kt-form kt-form--label-right" action="{{route('subjectbab.store')}}" method="POST" enctype="multipart/form-data">
        <div class="kt-portlet__body">
            @csrf
                    <div class="form-group row">
                        <label class="col-form-label col-lg-4 col-sm-12">Mata Pelajaran</label>
                        <div class="col-lg-4 col-md-9 col-sm-12">
                            <select name="subject_id" class="form-control kt_selectpicker">
                            @foreach($subject as $show)
                                <option value="{{ $show->id }}">{{ $show->name }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-4 col-sm-12">Jumlah Bab</label>
                        <div class="col-lg-4 col-md-9 col-sm-12">
                                <input type="text" class="form-control" onkeypress="return isNumber(event)" name="total" id="nama" required value="{{ old('total') }}">
                        </div>
                    </div>
                    <div class="kt-portlet__foot text-center">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-12 col-sm-12"></div>
                                <div class="col-lg-12 col-sm-12">
                                    <button type="submit" class="btn btn-brand">Lanjutkan</button>
                                    <button type="reset" class="btn btn-secondary">Batal</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
@endsection
