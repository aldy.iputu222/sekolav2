@extends('templates.main')

@section('title','Subject Bab Edit')
@section('page_name','Subject Bab Edit')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="">Dashboard</a></li>
<li class="breadcrumb-item"><a href="">Subject</a></li>
<li class="breadcrumb-item"><a href="">Subject Edit</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header"><h5>Subject Bab Edit</h5><a href="{{route('subjectbab.index')}}" class="btn btn-dark float-right"><i class="fas fa-arrow-left"></i>Kembali</a></div>
            <div class="card-body">
                <form action="{{route('subjectbab.store')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="">Mata Pelajaran</label>
                        <input type="text" class="form-control" disabled value="{{ $data->subject->name }}">
                    </div>
                     <div class="form-group">
                        <label for="">Jumlah Bab</label>
                        <input type="text" name="total" value="{{ $data->total }}" class="form-control" required="">
                    </div>
                    <button class="btn btn-primary">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
