@extends('templates.temp')

@section('title','Bab Mata Pelajaran')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Manajemen Ulangan </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('subjectbab.index') }}" class="kt-subheader__breadcrumbs-link">
    Bab Mata Pelajaran </a>
@endsection
@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">Bab Mata Pelajaran</h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <a href="{{ route('subjectbab.create') }}"
                        class="btn btn-default btn-bold btn-upper btn-font-sm">
                        <i class="flaticon2-add-1"></i>
                        Buat Baru
                    </a>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
                <table class="table data-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Pelajaran</th>
                            <th>Jumlah Bab</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($subjectbab as $all)
                        <tr>
                            <td>{{ $loop->index + 1 }}</td>
                            <td>{{$all->subject->name}}</td>
                            <td>{{$all->total}}</td>
                            <td class="text-center">
                            <td nowrap class="text-center">
                                    <a href="{{ route('subjectbab.destroy',$all->id) }}" class="btn btn-secondary btn-elevate btn-circle btn-icon delete-btn"><i class="la la-trash"></i></a>
                                    <a href="{{ route('subjectbab.edit',$all->id) }}" class="btn btn-secondary btn-elevate btn-circle btn-icon"><i class="la la-edit"></i></a>
                                    <a href="{{ route('subjectbab.show',$all->id) }}" class="btn btn-secondary btn-elevate btn-circle btn-icon"><i class="la la-eye"></i></a>
                            </td>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
@endsection
