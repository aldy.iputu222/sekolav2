@extends('templates.temp')
@section('title','Detail Bab Mata Pelajaran')
@section('page_name','Detail Bab Mata Pelajaran')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
CMS Sekolah </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('subjectbab.index') }}" class="kt-subheader__breadcrumbs-link">
    Bab Mata Pelajaran </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="" class="kt-subheader__breadcrumbs-link">
    Detail Bab Mata Pelajaran </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Detail Bab Mata Pelajaran</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('subjectbab.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>
            <div class="card-body">
                <form action="" enctype="multipart/form-data" method="POST">
                    @csrf @method('patch')
                    <div class="form-group">
                            <h5>Mata Pelajaran</h5>
                            <p>{{ $data->subject->name }}</p>
                    </div>
                    <hr>
                     <div class="form-group">
                            <h5>Jumlah Bab</h5>
                            <p>{{ $data->total }}</p>
                     </div>
                    <hr>
                     <div class="form-group">
                            <h5>Daftar Bab</h5>
                            @foreach($bab as $ba)
                                <ul>
                                    <li>{{ $ba->bab }}</li>
                                </ul>
                            @endforeach
                     </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
