@extends('templates.temp')

@section('title','Ubah data Guru')
@section('page_name','Ubah data Guru')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('teacher.index') }}" class="kt-subheader__breadcrumbs-link">
    Guru </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="" class="kt-subheader__breadcrumbs-link">
    Ubah data Guru </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Guru</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('teacher.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>

    <form class="kt-form kt-form--label-right" method="POST" action="{{ route('teacher.update',$data) }}" enctype="multipart/form-data">
        @csrf @method('put')
        <div class="kt-portlet__body">
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">NIP</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="number" name="nip" class="form-control"  required placeholder="Contoh : 11605524" value="{{ old('nip',$data->nip) }}" disabled />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Nama</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                        <input type="text" class="form-control" name="name" id="nama" required placeholder="Contoh : Ujang Sutisna" value="{{ old('name',$data->name) }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Email</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="email" class="form-control" name="email" id="email" required placeholder="Contoh : mang_ujang@gmail.com" value="{{ old('email',$data->email) }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Foto</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="file" name="photo" id="photo" data-allowed-file-extensions="png jpg jpeg" class="dropify" data-height="250" data-default-file="{{ asset('app/'.$data->photo) }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Alamat</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                        <textarea name="address" class="form-control" id="alamat" cols="30" rows="10" placeholder="Contoh : Jln. Suharso No. 968, Manado 46715, Riau">{{ old('address',$data->address) }}</textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Jenis Kelamin</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <select name="gender" class="form-control kt_selectpicker">
                        @if($data->gender == "L")
                        <option value="L" selected>Laki-Laki</option>
                        <option value="P">Perempuan</option>
                        @else
                        <option value="L">Laki-Laki</option>
                        <option value="P" selected>Perempuan</option>
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Tanggal Lahir</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="date" class="form-control" placeholder="Select date" name="date_of_birth" value="{{ old('date_of_birth',$data->date_of_birth) }}" />
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">No.Telp</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <input type="number" class="form-control" name="phone" id="no_tlp" placeholder="08971225785" value="{{ old('phone',$data->phone) }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-4 col-sm-12">Pelajaran yang di ajar</label>
                <div class="col-lg-4 col-md-9 col-sm-12">
                    <select name="subject_id" class="form-control kt_selectpicker" data-live-search="true">
                        @foreach($subject as $field)
                        @if($field->id == $data->subject_id)
                        <option value="{{ $field->id }}" selected>{{ $field->name }}</option>
                        @else
                        <option value="{{ $field->id }}">{{ $field->name }}</option>
                        @endif
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-4 col-sm-12"></div>
                    <div class="col-lg-8 col-sm-12">
                        <button type="submit" class="btn btn-brand">Simpan</button>
                        <button type="reset" class="btn btn-secondary">Batal</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection