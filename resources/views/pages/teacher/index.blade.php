@extends('templates.temp')

@section('title','Guru')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('teacher.index') }}" class="kt-subheader__breadcrumbs-link">
    Guru </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">Guru</h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <a href="{{ route('teacher.create') }}"
                        class="btn btn-brand btn-bold btn-upper btn-font-sm">
                        <i class="flaticon2-add-1"></i>
                        Buat Baru
                    </a>
                </div>
            </div>
        </div>

    <div class="kt-portlet__body">
        <!--begin: Datatable -->
        <table class="table data-table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>NIP</th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>Tanggal Lahir</th>
                    <th class="text-center">Aksi</th>
                </tr>
            </thead>

            <tbody>
                @foreach($teacher as $field)
                <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $field->nip }}</td>
                    <td>{{ $field->name }}</td>
                    <td>{{ $field->address }}</td>
                    <td>{{ $field->date_of_birth }}</td>
                    <td nowrap class="text-center">
                        <a href="{{ route('teacher.show',$field) }}" class="btn btn-secondary btn-elevate btn-circle btn-icon"> <i
                            class="la la-eye"></i> </a>
                        <a href="{{ route('teacher.destroy',$field) }}" class="btn btn-secondary btn-elevate btn-circle btn-icon delete-btn"> <i
                            class="la la-trash"></i> </a>
                        <a href="{{ route('teacher.edit',$field) }}" class="btn btn-secondary btn-elevate btn-circle btn-icon"> <i
                            class="la la-edit"></i> </a>
                    </td>
                </tr>
                @endforeach
            </tbody>

        </table>
        <!--end: Datatable -->
    </div>
</div>
@endsection