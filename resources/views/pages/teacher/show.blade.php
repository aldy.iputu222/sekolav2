@extends('templates.temp')

@section('title','Detail Guru')
@section('page_name','Detail Guru')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    Data Master </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('teacher.index') }}" class="kt-subheader__breadcrumbs-link">
    Guru </a>
<a href="" class="kt-subheader__breadcrumbs-link">
    Detail Guru </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Guru</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('teacher.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>

    <div class="kt-portlet__body">
        <div class="row">
            <div class="col-md-6">
                <div class="kt-section kt-section--first">
                    <h3 class="kt-section__title">Biodata</h3>
                    <div class="form-group">
                        <label for="">NIP</label>
                        <p>{{ $data->nip }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Nama</label>
                        <p>{{ $data->name }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Jenis Kelamin</label>
                        @if($data->gender == "L")
                        <p>Laki - Laki</p>
                        @else
                        <p>Perempuan</p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="">Tanggal Lahir</label>
                        <p>{{ $data->date_of_birth }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="kt-section kt-section--first">
                    <h3 class="kt-section__title">Foto</h3>
                    <img src="{{ asset('app/'.$data->photo) }}" alt="" class="img-fluid">
                </div>
            </div>
        </div>
        <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg kt-separator--portlet-fit">
            </div>
        <div class="row">
            <div class="col-md-6">
                <div class="kt-section kt-section--first">
                    <h3 class="kt-section__title">Kontak</h3>
                    <div class="form-group">
                        <label for="">No. Hp</label>
                        <p>{{ $data->name }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Email</label>
                        <p>{{ $data->email }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Alamat</label>
                        <p>{{ $data->address }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg kt-separator--portlet-fit">
            </div>
        <div class="row">
            <div class="col-md-6">
                <div class="kt-section kt-section--first">
                    <h3 class="kt-section__title">Qrcode</h3>
                    <img src="{{ asset('app/qrcode/'.$data->qrcode) }}" alt="" class="img-fluid">
                </div>
            </div>
            <div class="col-md-6">
                <div class="kt-section kt-section--first">
                    <h3 class="kt-section__title">Barcode</h3>
                    <img src="{{ asset('app/barcode/'.$data->barcode) }}" alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection