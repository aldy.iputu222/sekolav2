@extends('templates.main')

@section('title','UKS Create')
@section('page_name','UKS Create')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="">Dashboard</a></li>
<li class="breadcrumb-item"><a href="{{ route('uks.index') }}">UKS</a></li>
<li class="breadcrumb-item"><a href="{{ route('uks.create') }}">UKS Create</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header"><h5>Title</h5><a href="{{route('uks.index')}}" class="btn btn-dark text-right"><i class="fas fa-arrow-left"></i>Kembali</a></div>
            <div class="card-body">
                <form action="{{ route('uks.store') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="">Tahun Ajar</label>
                        <select name="school_year_id" id="school_year_select" class="select2">
                            <option value="">Pilih Tahun Ajar</option>
                            @foreach($school_year as $field)
                            <option value="{{ $field->id }}">{{ $field->first_year }} - {{ $field->last_year }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">Kelas</label>
                        <select name="class_group_id" id="classgroup_select" class="select2">
                            <option value="">Pilih Kelas</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">Siswa</label>
                        <select name="student_id" id="student" class="select2">
                            <option>Belum ada data</option>    
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">Description</label>
                        <textarea name="description" id="" class="ck-editor" rows="10"></textarea>
                    </div>
                    <button class="btn btn-primary">Simpan</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script>
    $(document).ready(function(){
        $('#school_year_select').change(function(){
            var school_year = $(this).val();
            $.post("{{ url('api/school_year/get_class') }}",{
                'school_year_id': school_year,
            },function(data){
                var options = '<option value="">Pilih Kelas</option>';
                $(data).each(function(index,value){
                    options += '<option value="'+value.id+'">'+value.name+'</option>';
                });
                $('#classgroup_select').html(options);
            });
        });

        $('#classgroup_select, #school_year_select').on('change', function(){
            $("#student option").remove();
            var class_group_id = $('#classgroup_select').val();
            var school_year_id = $('#school_year_select').val();

            $.post("{{ url('api/student/do_borrow') }}",{
                'school_year_id': school_year_id,
                'class_group_id': class_group_id
            },function(data){
                var options;
                $(data).each(function(index,value){
                    options += '<option value="'+value.id+'">'+value.name+'</option>';
                });
                $('#student').html(options);
            });
        });
    });
</script>
@endsection
