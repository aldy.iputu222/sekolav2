@extends('templates.main')

@section('title','UKS')
@section('page_name','UKS')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="">Dashboard</a></li>
<li class="breadcrumb-item"><a href="">UKS</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header"><h5>Title</h5><a href="{{ route('uks.create') }}" class="btn btn-primary"><i class="fas fa-plus"></i>Tambah Data </a></div>
            <div class="card-body">
                <table class="table table-striped table-bordered data-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Siswa</th>
                            <th>Kelas</th>
                            <th>Deskripsi Sakit</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $uks)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td>{{ $uks->student->name }}</td>
                                <td>{{ $uks->student->classgroup->name }}</td>
                                <td>{!! $uks->description !!}</td>
                                <td class="text-center">
                                    <div class="btn-group text-center">
                                        <a href="" class="btn btn-sm btn-primary text-center">
                                            <i class="mr-0 fas fa-edit"></i>
                                        </a>
                                        <a href="{{ route('uks.destroy', $uks->id) }}" class="btn btn-sm btn-danger delete-btn">
                                            <i class="mr-0 fas fa-trash"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
