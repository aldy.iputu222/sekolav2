@extends('templates.main')

@section('title','UKS Create')
@section('page_name','UKS Create')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="">Dashboard</a></li>
<li class="breadcrumb-item"><a href="{{ route('uks.index') }}">UKS</a></li>
<li class="breadcrumb-item"><a href="{{ route('uks.create') }}">UKS Create</a></li>
@endsection

@section('content')
{{$data}}
@endsection