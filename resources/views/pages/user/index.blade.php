@extends('templates.main')

@section('title', 'Page User')
@section('page_name', 'Pengguna')
@section('breadcrumb')

    <li class="breadcrumb-item"><a href="">Dashboard</a></li>
    <li class="breadcrumb-item"><a href="">Pengguna</a></li>
    
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header"><h5>Tabel Pengguna</h5><a href="{{ url('user/create') }}" class="btn btn-primary"><i class="fas fa-plus"></i>Tambah Data </a></div>
            <div class="card-body">
                <table class="table table-striped table-bordered data-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Level</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Value 1</td>
                            <td>Value 1</td>
                            <td>Value 1</td>
                            <td class="text-center">
                                <div class="btn-group text-center">
                                    <a href="" class="btn btn-sm btn-primary text-center">
                                        <i class="mr-0 fas fa-edit"></i></a>
                                    <a href="google.com" class="btn btn-sm btn-primary delete-btn">
                                        <i class="fas fa-trash"></i></a>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
