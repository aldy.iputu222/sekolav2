@extends('templates.temp')

@section('title','Buat Artikel')
@section('page_name','Buat Artikel')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
CMS Sekolah </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('article.index') }}" class="kt-subheader__breadcrumbs-link">
    Artikel </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="" class="kt-subheader__breadcrumbs-link">
    Buat Artikel </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Artikel</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('article.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>

    <form class="kt-form kt-form--label-right" action="{{route('article.store')}}" method="POST" enctype="multipart/form-data">
        <div class="kt-portlet__body">
        @csrf
        <div class="form-group row">
            <label class="col-form-label col-lg-3 col-sm-12">Judul Artikel</label>
            <div class="col-lg-6 col-md-9 col-sm-12">
                <input type="text" class="form-control" name="title" id="nama" required value="{{ old('title') }}">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-form-label col-lg-3 col-sm-12">Konten</label>
            <div class="col-lg-6 col-md-9 col-sm-12">
                 <textarea name="content" class="summernote" id="m_summernote_1" cols="30" rows="10">{{ old('content') }}</textarea>
            </div>
        </div>
        <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Gambar</label>
                <div class="col-lg-6 col-md-9 col-sm-12">
                    <input type="file" name="image" id="photo" data-allowed-file-extensions="png jpg jpeg" class="dropify" data-height="250">
                </div>
        </div>
        <div class="form-group row">
                <label class="col-form-label col-lg-3 col-sm-12">Kategori</label>
                <div class="col-lg-6 col-md-9 col-sm-12">
                    <select name="category_content_id" class="form-control kt_selectpicker">
                    @foreach($kategori as $show)
                        <option value="{{ $show->id }}">{{ $show->name }}</option>
                    @endforeach
                    </select>
                </div>
        </div>
        <div class="kt-portlet__foot text-center">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-3 col-sm-12"></div>
                    <div class="col-lg-8 col-sm-12">
                        <button type="submit" class="btn btn-brand">Simpan</button>
                        <button type="reset" class="btn btn-secondary">Batal</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
