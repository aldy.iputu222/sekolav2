@extends('templates.temp')

@section('title','Edit Artikel')
@section('page_name','Edit Artikel')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
CMS Sekolah </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('article.index') }}" class="kt-subheader__breadcrumbs-link">
    Artikel </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="" class="kt-subheader__breadcrumbs-link">
    Edit Artikel </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Artikel</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('article.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>

    <form class="kt-form kt-form--label-right" action="{{route('article.update',$data)}}" method="POST" enctype="multipart/form-data">
        <div class="kt-portlet__body">
        @csrf @method('patch')
        <div class="form-group row">
            <label class="col-form-label col-lg-2 col-sm-12">Judul Artikel</label>
            <div class="col-lg-8 col-md-9 col-sm-12">
                    <input type="text" class="form-control" name="title" id="nama" value="{{ $data->title }}" required value="{{ old('title') }}">
            </div>
        </div>
        <div class="form-group row">
            <label class="col-form-label col-lg-2 col-sm-12">Konten</label>
            <div class="col-lg-8 col-md-9 col-sm-12">
                    <textarea name="content" id="" class="summernote" cols="30" rows="10">{{ $data->content }}</textarea>
            </div>
        </div>
        <div class="form-group row">
                <label class="col-form-label col-lg-2 col-sm-12">Gambar</label>
                <div class="col-lg-8 col-md-9 col-sm-12">
                    <input type="file" class="dropify" name="image" id="photo" data-default-file="{{ asset('app/'.$data->image) }}" data-allowed-file-extensions="png jpg jpeg" data-height="250">
                </div>
        </div>
        <div class="form-group row">
                <label class="col-form-label col-lg-2 col-sm-12">Kategori</label>
                <div class="col-lg-8 col-md-9 col-sm-12">
                    <select name="category_content_id" class="form-control kt_selectpicker">
                    @foreach($kategori as $show)
                        @if($show->id == $data->category_content_id)
                            <option value="{{ $show->id }}" selected>{{ $show->name }}</option>
                        @else
                            <option value="{{ $show->id }}">{{ $show->name }}</option>
                        @endif
                    @endforeach
                    </select>
                </div>
        </div>
        <div class="kt-portlet__foot text-center">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-12 col-sm-12"></div>
                    <div class="col-lg-12 col-sm-12">
                        <button type="submit" class="btn btn-brand">Update</button>
                        <button type="reset" class="btn btn-secondary">Batal</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
