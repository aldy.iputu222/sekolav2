@extends('templates.temp')

@section('title','Artikel')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    CMS Sekolah </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('article.index') }}" class="kt-subheader__breadcrumbs-link">
    Artikel </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">Artikel</h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <a href="{{ route('article.create') }}"
                        class="btn btn-default btn-bold btn-upper btn-font-sm">
                        <i class="flaticon2-add-1"></i>
                        Buat Baru
                    </a>
                </div>
            </div>
        </div>

        <div class="kt-portlet__body">
                    <table class="table data-table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Judul</th>
                                <th>Kategori</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($article as $show)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td>{{ $show->title }}</td>
                                <td>{{ $show->category->name }}</td>
                                <td nowrap class="text-center">
                                    <a href="{{ route('article.destroy',$show->id) }}" class="btn btn-secondary btn-elevate btn-circle btn-icon delete-btn"><i class="la la-trash"></i></a>
                                    <a href="{{ route('article.edit',$show) }}" class="btn btn-secondary btn-elevate btn-circle btn-icon"><i class="la la-edit"></i></a>
                                    <a href="{{ route('article.show',$show) }}" class="btn btn-secondary btn-elevate btn-circle btn-icon"><i class="la la-eye"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>    
            </div>
@endsection
