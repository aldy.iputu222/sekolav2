@extends('templates.temp')

@section('title','Buat Artikel')
@section('page_name','Buat Artikel')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
CMS Sekolah </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('article.index') }}" class="kt-subheader__breadcrumbs-link">
    Artikel </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="" class="kt-subheader__breadcrumbs-link">
    Detail Artikel </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Detail Artikel</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('article.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>
            <div class="card-body">
                    @csrf @method('patch')
                    <div class="form-group">
                            <h5>Title</h5>
                            <p>{{ $data->title }}</p>
                    </div>
                    <hr>
                     <div class="form-group">
                            <h5>Content</h5>
                            <p>{!! $data->content !!}</p>
                     </div>
                     <hr>
                     <div class="form-group">
                        <label for="photo">Image / Video</label>
                        @if($ext == "jpg" || $ext == "JPG" || $ext == "jpeg" || $ext == "JPEG" || $ext == "png" || $ext == "PNG")
                              <input type="file" name="image" disabled id="photo" data-default-file="{{ asset('app/'.$data->image) }}" data-allowed-file-extensions="png jpg jpeg" class="dropify" >
                        @elseif($ext == "mp4" || $ext == "MP4")
                             <video width="100%" height="100%" controls="">
                                <source src="{{ asset('app/'.$data->image) }}" type="video/mp4">
                            </video>
                        @endif
                    </div>
                    <hr>
                     <div class="form-group">
                            <h5>Category</h5>
                            <p>{!! $data->category->name !!}</p>
                     </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
