@extends('templates.temp')

@section('title','Edit Ektrakulikuler')
@section('page_name','Edit Ektrakulikuler')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
CMS Sekolah </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('eskul.index') }}" class="kt-subheader__breadcrumbs-link">
    Ektrakulikuler </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="" class="kt-subheader__breadcrumbs-link">
    Edit Ektrakulikuler </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Edit Eskul</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('eskul.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>
    <form action="{{ route('eskul.update',$data) }}" method="POST" enctype="multipart/form-data">
        {{ method_field('put') }}
        @csrf<div class="row">
        <div class="kt-portlet__body">
            <div class="form-group row">
                <label class="col-form-label col-lg-2 col-sm-12">Nama Ekstrakulikuler</label>
                <div class="col-lg-8 col-md-9 col-sm-12">
                        <input type="text" class="form-control" name="name" id="nama" required value="{{ $data->name }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2 col-sm-12">Gambar</label>
                <div class="col-lg-8 col-md-9 col-sm-12">
                        <input type="file" name="image" id="photo" data-allowed-file-extensions="png jpg jpeg" data-default-file="{{ asset('app/'.$data->image) }}" class="dropify">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2 col-sm-12">Deskripsi</label>
                <div class="col-lg-8 col-md-9 col-sm-12">
                        <textarea class="summernote" cols="60" rows="10" name="description" id="nama" required>{{ $data->description }}</textarea>
                </div>
            </div>
            <div class="kt-portlet__foot text-center">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12"></div>
                        <div class="col-lg-12 col-sm-12">
                            <button type="submit" class="btn btn-brand">Update</button>
                            <button type="reset" class="btn btn-secondary">Batal</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
