@extends('templates.temp')

@section('title','Detail Prestasi')
@section('page_name','Detail Prestasi')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
CMS Sekolah </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('prestation.index') }}" class="kt-subheader__breadcrumbs-link">
    Prestasi </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="" class="kt-subheader__breadcrumbs-link">
    Detail Prestasi </a>
@endsection


@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Detail Prestasi</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('prestation.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <h5>Judul</h5>
                            <p>{{ $data->name }}</p>
                        </div>
                        <hr>
                        <div class="form-group">
                            <h5>Deskripsi</h5>
                            <p>{{ strip_tags($data->description) }}</p>
                        </div>
                        <hr>
                        <div class="form-group">
                            <h5>Photo</h5>
                            <input type="file" disabled class="dropify"
                                data-default-file="{{ asset('app/'.$data->image) }}">
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection