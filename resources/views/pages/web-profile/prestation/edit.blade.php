@extends('templates.temp')

@section('title','Edit Prestasi')
@section('page_name','Edit Prestasi')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
CMS Sekolah </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('prestation.index') }}" class="kt-subheader__breadcrumbs-link">
    Prestasi </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="" class="kt-subheader__breadcrumbs-link">
    Edit Prestasi </a>
@endsection


@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Edit Prestasi</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('prestation.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>
    <form action="{{ route('prestation.update',$data) }}" method="POST" enctype="multipart/form-data">
        {{ method_field('put') }}
        @csrf
        <div class="kt-portlet__body">
            <div class="form-group row">
                <label class="col-form-label col-lg-2 col-sm-12">Judul</label>
                <div class="col-lg-8 col-md-9 col-sm-12">
                        <input type="text" class="form-control" name="name" id="nama" required value="{{ $data->name }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2 col-sm-12">Foto</label>
                <div class="col-lg-8 col-md-9 col-sm-12">
                        <input type="file" class="dropify" name="image" id="image" data-default-file="{{ asset('app/'.$data->image) }}" data-allowed-file-extensions="png jpg jpeg">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2 col-sm-12">Deskripsi</label>
                <div class="col-lg-8 col-md-9 col-sm-12">
                        <textarea name="description" id="description" class="summernote" cols="30" rows="10">{{ $data->description }}</textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2 col-sm-12">Juara Ke</label>
                <div class="col-lg-8 col-md-9 col-sm-12">
                    <input type="number" class="form-control" value="{{ $data->position }}" name="position">
                </div>
            </div>
            <div class="kt-portlet__foot text-center">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12">
                            <button type="submit" class="btn btn-brand">Update</button>
                            <button type="reset" class="btn btn-secondary">Batal</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
     </div>
@endsection
