@extends('templates.temp')

@section('title','Buat Video')
@section('page_name','Buat Video')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
CMS Sekolah </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('video.index') }}" class="kt-subheader__breadcrumbs-link">
    Video </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('video.create') }}" class="kt-subheader__breadcrumbs-link">
    Buat Video </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Video</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('video.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>

    <form class="kt-form kt-form--label-right" action="{{route('video.store')}}" method="POST" enctype="multipart/form-data">
        <div class="kt-portlet__body">
            @csrf
            <div class="form-group row">
                <label class="col-form-label col-lg-2 col-sm-12">Judul</label>
                <div class="col-lg-8 col-md-9 col-sm-12">
                        <input type="text" class="form-control" name="title" id="nama" required value="{{ old('title') }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2 col-sm-12">Konten</label>
                <div class="col-lg-8 col-md-9 col-sm-12">
                        <textarea name="content" id="" class="summernote" cols="30" rows="10">{{ old('content') }}</textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2 col-sm-12">Gambar</label>
                <div class="col-lg-8 col-md-9 col-sm-12">
                    <input type="file" name="image" id="photo" data-allowed-file-extensions="png jpg jpeg" class="dropify" data-height="250">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2 col-sm-12">Tipe</label>
                <div class="col-lg-8 col-md-9 col-sm-12">
                    <select name="type" id="type" class="form-control kt_selectpicker">
                        <option value="">Pilih Tipe</option>
                        <option value="local">Local</option>
                        <option value="embed">Embed</option>
                    </select>
                </div>
            </div>
                    <div class="form-group form-file">
                        <label for="">Local</label>
                        <input type="file" class="dropify" data-allowed-file-extensions="3gp mp4 mkv" name="video">
                    </div>
                    <div class="form-group form-embed">
                        <label for="">Embed</label>
                        <input type="text" class="form-control" placeholder="https://www.youtube.com/embed/CacPltdpmrI" name="video">
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-lg-2 col-sm-12">Kategori Konten</label>
                            <div class="col-lg-8 col-md-9 col-sm-12">
                                <select name="category_content_id" class="form-control kt_selectpicker">
                                @foreach($category as $val)
                                    <option value="{{ $val->id }}">{{ $val->name }}</option>
                                @endforeach
                                </select>
                            </div>
                    </div>
                        <!-- <input type="text" name="category_content_id" id="category_content_id" class="form-control"> -->
                    </div>
                    <div class="kt-portlet__foot text-center">
                        <div class="kt-form__actions">
                            <div class="row">
                                <div class="col-lg-12 col-sm-12"></div>
                                <div class="col-lg-12 col-sm-12">
                                    <button type="submit" class="btn btn-brand">Simpan</button>
                                    <button type="reset" class="btn btn-secondary">Batal</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
@endsection
@section('script')
<script>
    $(document).ready(function(){
        $('.form-embed').hide();
        $('.form-file').hide();
        $('#type').change(function(data){
            if($(this).val()=="embed")
            {
                $('.form-embed').show();
                $('.form-file').hide();
            }else if($(this).val()=="local"){
                $('.form-embed').hide();
                $('.form-file').show();
            }else{
                $('.form-embed').hide();
                $('.form-file').hide();
            }
        });
    });
</script>
@endsection
    