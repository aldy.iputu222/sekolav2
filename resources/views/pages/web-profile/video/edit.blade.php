@extends('templates.temp')

@section('title','Edit Video')
@section('page_name','Edit Video')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
CMS Sekolah </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('video.index') }}" class="kt-subheader__breadcrumbs-link">
    Video </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="" class="kt-subheader__breadcrumbs-link">
    Edit Video </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Edit Video</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('video.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>
    <form action="{{ route('video.update',$edit) }}" method="POST" enctype="multipart/form-data">
        {{ method_field('put') }}
        @csrf
        <div class="kt-portlet__body">
            <div class="form-group row">
                <label class="col-form-label col-lg-2 col-sm-12">Judul</label>
                <div class="col-lg-8 col-md-9 col-sm-12">
                        <input type="text" class="form-control" name="title" id="nama" required value="{{ $edit->title }}">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2 col-sm-12">Konten</label>
                <div class="col-lg-8 col-md-9 col-sm-12">
                        <textarea class="summernote" cols="60" rows="10" name="content" id="nama" required>{{ $edit->content }}</textarea>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-form-label col-lg-2 col-sm-12">Gambar</label>
                <div class="col-lg-8 col-md-9 col-sm-12">
                        <input type="file" name="photo" id="photo" data-allowed-file-extensions="png jpg jpeg" data-default-file="{{ asset('app/'.$edit->image) }}" class="dropify">
                </div>
            </div>
             @if($edit->type == "local")
             <div class="form-group row">
                <label class="col-form-label col-lg-2 col-sm-12">Video Local</label>
                <div class="col-lg-8 col-md-9 col-sm-12">
                <input type="hidden" name="type" value="{{ $edit->type }}">
                        <input type="file" name="video" id="photo" data-allowed-file-extensions="3gp mkv mp4" data-default-file="{{ asset('app/'.$edit->video) }}" class="dropify">
                </div>
            </div>
            @elseif($edit->type == "embed")
            <div class="form-group row">
                <label class="col-form-label col-lg-2 col-sm-12">Embed</label>
                <div class="col-lg-8 col-md-9 col-sm-12">
                        <input type="hidden" name="type" value="{{ $edit->type }}">
                        <input type="text" name="video" value="{{ $edit->video }}" id="photo" class="form-control">
                </div>
            </div>
            @endif
            <div class="form-group row">
                <label class="col-form-label col-lg-2 col-sm-12">Kategori Konten</label>
                <div class="col-lg-8 col-md-9 col-sm-12">
                    <select name="status" class="form-control kt_selectpicker">
                    @foreach($kategori as $val)
                        @if($edit->category_content_id == $val->id)
                            <option value="{{ $val->id }}" selected>{{ $val->name }}</option>
                        @else
                            <option value="{{ $val->id }}" checked>{{ $val->name }}</option>
                        @endif
                    @endforeach
                    </select>
                </div>
             </div>
             <div class="kt-portlet__foot text-center">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-lg-12 col-sm-12"></div>
                        <div class="col-lg-12 col-sm-12">
                            <button type="submit" class="btn btn-brand">Update</button>
                            <button type="reset" class="btn btn-secondary">Batal</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
    