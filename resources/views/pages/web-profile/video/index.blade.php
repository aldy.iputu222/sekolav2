@extends('templates.temp')

@section('title','Video')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
    CMS Sekolah </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('video.index') }}" class="kt-subheader__breadcrumbs-link">
    Video </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">Video</h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <a href="{{ route('video.create') }}"
                        class="btn btn-default btn-bold btn-upper btn-font-sm">
                        <i class="flaticon2-add-1"></i>
                        Buat Baru
                    </a>
                </div>
            </div>
        </div>

        <div class="kt-portlet__body">
                <table class="table data-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Judul</th>
                            <th>Type</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($video as $show)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td>{{ $show->title }}</td>
                                @if($show->type == 'local')
                                <td>
                                    <div class="badge badge-primary">Local</div>
                                </td>
                                @else
                                <td>
                                    <div class="badge badge-warning">Embed</div>
                                </td>
                                @endif
                                <td nowrap class="text-center">
                                    <a href="{{ route('video.destroy',$show->id) }}" class="btn btn-secondary btn-elevate btn-circle btn-icon delete-btn"><i class="la la-trash"></i></a>
                                    <a href="{{ route('video.edit',$show) }}" class="btn btn-secondary btn-elevate btn-circle btn-icon"><i class="la la-edit"></i></a>
                                    <a href="{{ route('video.show',$show) }}" class="btn btn-secondary btn-elevate btn-circle btn-icon"><i class="la la-eye"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
@endsection
