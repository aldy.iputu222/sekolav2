@extends('templates.temp')

@section('title','Detail Video')
@section('page_name','Detail Video')
@section('breadcrumb')
<a href="" class="kt-subheader__breadcrumbs-link">
CMS Sekolah </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="{{ route('video.index') }}" class="kt-subheader__breadcrumbs-link">
    Video </a>
<span class="kt-subheader__breadcrumbs-separator"></span>
<a href="" class="kt-subheader__breadcrumbs-link">
    Detail Video </a>
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">Detail Video</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="{{ route('video.index') }}" class="btn btn-clean btn-bold btn-upper btn-font-sm">
                    <i class="la la-long-arrow-left"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <h5>Title</h5>
                            <p>{{ $video->title }}</p>
                        </div>
                        <hr>
                        <div class="form-group">
                            <h5>Content</h5>
                            <p>{!! $video->content !!}</p>
                        </div>
                        <hr>
                        <div class="form-group">
                            <h5>Photo</h5>
                            <input type="file" disabled class="dropify"
                                data-default-file="{{ asset('app/'.$video->image) }}">
                        </div>
                        <hr>
                        <div class="form-group">
                            <h5>Type</h5>
                            @if($video->type == 'local')
                                <div class="badge badge-primary">Local</div>
                            @else
                                <div class="badge badge-warning">Embed</div>
                            @endif
                        </div>
                        <hr>
                        <div class="form-group">
                            <h5>Video</h5>
                            @if($video->type == "embed")
                                <div class="plyr__video-embed" id="player">
                                    <iframe
                                        src="{{ $video->video }}"
                                        allowfullscreen
                                        allowtransparency
                                        allow="autoplay"
                                    ></iframe>
                                </div>
                            @elseif($video->type == "local")
                                <video poster="{{ asset('app/'.$video->image )}}" id="player" playsinline controls width="975" height="400">
                                    <source src="{{ asset('app/'.$video->video) }}" type="video/mp4" />
                                </video>
                            @endif
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection