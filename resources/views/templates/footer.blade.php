<script type="text/javascript">
   (function(d, m){var s, h;       
   s = document.createElement("script");
   s.type = "text/javascript";
   s.async=true;
   s.src="https://apps.applozic.com/sidebox.app";
   h=document.getElementsByTagName('head')[0];
   h.appendChild(s);
   window.applozic=m;
   m.init=function(t){m._globals=t;}})(document, window.applozic || {});
</script>

<script>
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#3699ff",
                    "metal": "#c4c5d6",
                    "light": "#ffffff",
                    "accent": "#00c5dc",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995",
                    "focus": "#9816f4"
                },
                "base": {
                    "label": [
                        "#c5cbe3",
                        "#a1a8c3",
                        "#3d4465",
                        "#3e4466"
                    ],
                    "shape": [
                        "#f0f3ff",
                        "#d9dffa",
                        "#afb4d4",
                        "#646c9a"
                    ]
                }
            }
        };
    </script>
    <!-- end::Global Config -->

    <!--begin::Global Theme Bundle(used by all pages) -->
    <script src="{{ asset('assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/demo6/base/scripts.bundle.js')}}" type="text/javascript"></script>
    <!--end::Global Theme Bundle -->


    <!--begin::Page Vendors(used by this page) -->
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/app/custom/general/components/forms/widgets/bootstrap-datepicker.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/app/custom/general/components/forms/widgets/bootstrap-select.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/dist/js/dropify.min.js') }}"></script>
    <script src="{{ asset('assets/dist/js/image-slider.js') }}"></script>
    <script src="{{ asset('assets/app/custom/general/components/extended/sweetalert2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/app/custom/general/components/forms/widgets/summernote.js') }}" type="text/javascript"></script>
    <!--end::Page Vendors -->



    <!--begin::Page Scripts(used by this page) -->
    <script src="{{ asset('assets/app/custom/general/dashboard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/dist/js/iziToast.min.js') }}"></script>
    <script src="{{ asset('js/peer.js') }}"></script>
    <!--end::Page Scripts -->


    <!--begin::Global App Bundle(used by all pages) -->
    <script src="{{ asset('assets/app/bundle/app.bundle.js') }}" type="text/javascript"></script>
    <!--end::Global App Bundle -->



    <script>
        $(document).ready(function(){
            $('.data-table').DataTable();
            $('.dropify').dropify();
        });
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
    </script>
    @include('alerts.main')
    @yield('script')
    @yield('js')
</body>
<!-- end::Body -->

</html>