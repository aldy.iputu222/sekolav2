<!DOCTYPE html>

<html lang="en" class="kt-sweetalert2--nopadding">
<!-- begin::Head -->

<head>
    <meta charset="utf-8" />

    <title>@yield('title')</title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!--begin::Fonts -->
    
    <!--end::Fonts -->

    <!--begin::Page Vendors Styles(used by this page) -->
    <link href="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/app/custom/blog/post.demo6.css') }}" rel="stylesheet" type="text/css" />
    <!--end::Page Vendors Styles -->


    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="{{ asset('assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/demo/demo6/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/dist/css/iziToast.min.css') }}">
    {{-- <link href="https://keenthemes.com/keen/preview/demo6/custom/blog/assets/demo/demo6/base/style.bundle.css"
		rel="stylesheet" type="text/css" /> --}}
    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <link rel="stylesheet" href="{{ asset('assets/dist/css/dropify.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/dist/css/image-slider.css') }}">
    <script src="{{ asset('assets/dist/js/jquery-3.2.1.min.js') }}"></script>
    <!--end::Layout Skins -->
    @yield('css')
    <style>
        th{
            border-bottom: none !important;
        }
        /* td{
            border-top: none !important;
        } */
        .form-control[readonly] {
            background-color: #efefef !important;
        }

        /* .kt-aside .kt-aside__primary{
            background-color : #0052d4 !important;
        } */

        .btn-brand{
            background-color : #0052d4 !important;
            border-color: #0052d4 !important;
        }

        #chatter_hero{
            display:none;
        }
    </style>
</head>
<!-- end::Head -->

<!-- begin::Body -->

<body class="kt-sweetalert2--nopadding kt-header-mobile--fixed kt-page-content-white kt-subheader--enabled kt-aside--secondary-enabled kt-offcanvas-panel--left kt-aside--left kt-page--loading">