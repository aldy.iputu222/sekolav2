<?php 
    $route = Route::currentRouteName();
?>

<div class="kt-aside  kt-grid__item kt-grid kt-grid--ver" id="kt_aside">
    <!-- begin::Aside Primary -->
    <div class="kt-aside__primary">
        <!-- begin::Aside Top -->
        <div class="kt-aside__top">
            <a class="kt-aside__brand" href="index.html">
                <img alt="Logo" src="{{ asset('assets/media/logos/logo-9.png') }}">
            </a>
        </div>
        <!-- end:: Aside Top -->

        <!-- begin::Aside Middle -->
        <div class="kt-aside__middle">
            <ul class="kt-aside__nav">
                <li class="kt-aside__nav-item">
                    <a class="kt-aside__nav-link active" href="index.html">
                        <i class="flaticon2-protection"></i>
                    </a>
                </li>
            </ul>
        </div>
        <!-- end::Aside Middle -->

        <!-- begin::Aside Bottom -->
        <div class="kt-aside__bottom">
            <ul class="kt-aside__nav">
                <li class="kt-aside__nav-item">
                    <a href="index.html#" class="kt-aside__nav-link" id="kt_offcanvas_toolbar_search_toggler_btn">
                        <i class="flaticon2-search-1"></i>
                    </a>
                </li>

                <li class="kt-aside__nav-item">
                    <a href="index.html#" class="kt-aside__nav-link"
                        id="kt_offcanvas_toolbar_notifications_toggler_btn">
                        <i class="flaticon2-bell-alarm-symbol"></i>
                    </a>
                </li>

                <li class="kt-aside__nav-item">
                    <a href="index.html#" class="kt-aside__nav-link" id="kt_quick_panel_toggler_btn">
                        <i class="flaticon2-grids"></i>
                    </a>
                </li>

                <li class="kt-aside__nav-item">
                    <a href="index.html#" class="kt-aside__nav-link" id="kt_offcanvas_toolbar_profile_toggler_btn">
                        <i class="flaticon2-hourglass-1 kt-hidden"></i>
                        <img class="kt-hidden-" alt="" src="{{ asset('assets/media/users/300_21.jpg') }}">
                        <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                        <span class="kt-aside__nav-username kt-bg-brand kt-hidden">S</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- end::Aside Bottom -->
    </div>
    <!-- end::Aside Primary -->

    <!-- begin::Aside Secondary -->
    <div class="kt-aside__secondary">
        <div class="kt-aside__secondary-top">
            <h3 class="kt-aside__secondary-title">
                Menu
            </h3>
        </div>
        <!-- To hide the secondary menu remove "kt-aside--secondary-enabled" class from the body tag and remove below "kt-aside__secondary-bottom" block-->
        <div class="kt-aside__secondary-bottom">
            <!-- begin:: Aside Menu -->
            <div id="kt_aside_menu" class="kt-aside-menu  aziko " data-ktmenu-vertical="1" data-ktmenu-scroll="1">
                <ul class="kt-menu__nav ">
                    <li class="kt-menu__section kt-menu__section--first">
                        <h4 class="kt-menu__section-text">Apps</h4>
                        <i class="kt-menu__section-icon flaticon-more-v2"></i>
                    </li>
                    <!-- =======================  ADMIN  ======================== -->
                    @if(Auth::guard('web')->check())
                    <li class="kt-menu__item {{ $route == 'admin' ? 'kt-menu__item--active' : '' }}"
                        aria-haspopup="true"><a href="{{ route('admin') }}" class="kt-menu__link "><span
                                class="kt-menu__link-text">Dashboard</span></a></li>
                    <li class="kt-menu__section ">
                        <h4 class="kt-menu__section-text">Content</h4>
                        <i class="kt-menu__section-icon flaticon-more-v2"></i>
                    </li>
                    <li class="kt-menu__item  kt-menu__item--submenu 
                    @switch($route)
                        @case('teacher.index') @case('teacher.create') @case('teacher.edit') @case('setting.index') @case('setting.create') @case('setting.edit') @case('classgroup.index') @case('classgroup.create') @case('classgroup.edit') @case('student.index') @case('student.create') @case('student.edit') @case('major.index') @case('major.create') @case('major.edit') @case('school_year.index') @case('school_year.create') @case('school_year.edit') @case('attitude.index') @case('attitude.create') @case('attitude.edit') @case('subject.index') @case('subject.create') @case('subject.edit') @case('employee.index')
                        kt-menu__item--open
                        @break
                    @endswitch
                    " aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;"
                            class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text">Data
                                Master</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                            <ul class="kt-menu__subnav">
                                <li class="kt-menu__item {{ $route == 'setting.index' || $route == 'setting.create' || $route == 'setting.edit' ? 'kt-menu__item--active' : '' }}"
                                    aria-haspopup="true"><a href="{{ route('setting.index') }}"
                                        class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Profil Sekolah</span></a></li>
                                <li class="kt-menu__item {{ $route == 'school_year.index' || $route == 'school_year.create' || $route == 'school_year.edit' ? 'kt-menu__item--active' : '' }}"
                                    aria-haspopup="true"><a href="{{ route('school_year.index')}}"
                                        class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Tahun Ajar</span></a></li>
                                <li class="kt-menu__item {{ $route == 'major.index' || $route == 'major.create' || $route == 'major.edit' ? 'kt-menu__item--active' : '' }}"
                                    aria-haspopup="true"><a href="{{ route('major.index') }}" class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Jurusan</span></a>
                                </li>
                                <li class="kt-menu__item {{ $route == 'classgroup.index' || $route == 'classgroup.create' || $route == 'classgroup.edit' ? 'kt-menu__item--active' : '' }}"
                                    aria-haspopup="true"><a href="{{ route('classgroup.index') }}"
                                        class="kt-menu__link"><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Kelas</span></a></li>
                                <li class="kt-menu__item {{ $route == 'subject.index' || $route == 'subject.create' ? 'kt-menu__item--active' : '' }}"
                                    aria-haspopup="true"><a href="{{ route('subject.index') }}"
                                        class="kt-menu__link {{ $route == 'subject.index' || $route == 'subject.create' || $route == 'subject.edit' ? 'kt-menu__item--active' : '' }}"><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Mata Pelajaran</span></a></li>
                                <li class="kt-menu__item {{ $route == 'teacher.index' || $route == 'teacher.create' || $route == 'teacher.edit' ? 'kt-menu__item--active' : '' }}"
                                    aria-haspopup="true"><a href="{{ route('teacher.index') }}"
                                        class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Guru</span></a></li>
                                <li class="kt-menu__item {{ $route == 'student.index' || $route == 'student.create' || $route == 'student.edit' ? 'kt-menu__item--active' : '' }}"
                                    aria-haspopup="true"><a href="{{ route('student.index') }}" class="kt-menu__link"><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Siswa</span></a></li>
                                <li class="kt-menu__item {{ $route == 'attitude.index' || $route == 'attitude.create' || $route == 'attitude.edit' ? 'kt-menu__item--active' : '' }}"
                                    aria-haspopup="true"><a href="{{ route('attitude.index') }}"
                                        class="kt-menu__link"><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Reward & Punishment</span></a></li>
                                <li class="kt-menu__item {{ $route == 'employee.index' || $route == 'employee.create' || $route == 'employee.edit' ? 'kt-menu__item--active' : '' }}"
                                    aria-haspopup="true"><a href="{{ route('employee.index') }}"
                                        class="kt-menu__link"><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Pegawai</span></a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="kt-menu__item  kt-menu__item--submenu
                    @switch($route)
                        @case('absent.index') @case('absent.create') 
                        kt-menu__item--open
                        @break
                    @endswitch
                    " aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;"
                            class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text">Absensi</span><i
                                class="kt-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                            <ul class="kt-menu__subnav">
                                <li class="kt-menu__item {{ $route == 'absent.create' ? 'kt-menu__item--active' : '' }}"
                                    aria-haspopup="true"><a href="{{ route('absent.create') }}"
                                        class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Pendataan</span></a></li>
                                <li class="kt-menu__item {{ $route == 'absent.index' ? 'kt-menu__item--active' : '' }}"
                                    aria-haspopup="true"><a href="{{ route('absent.index') }}" class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Rekapan</span></a></li>
                                <li class="kt-menu__item " aria-haspopup="true"><a href="" class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Laporan</span></a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="kt-menu__item  kt-menu__item--submenu
                    @switch($route)
                        @case('author.index') @case('author.create') @case('author.edit') @case('publisher.index') @case('publisher.create') @case('publisher.edit') @case('book_category.index') @case('book_category.create') @case('book_category.edit') @case('book_borrow.create') @case('book_borrow.next') @case('book.index') @case('book.edit') @case('book.create')
                        kt-menu__item--open
                        @break
                    @endswitch" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;"
                            class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text">Perpustakaan</span><i
                                class="kt-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                            <ul class="kt-menu__subnav">
                                <li class="kt-menu__item {{ $route == 'author.index' || $route == 'author.create' || $route == 'author.edit' ? 'kt-menu__item--active' : '' }}"
                                    aria-haspopup="true"><a href="{{ route('author.index') }}" class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Penulis</span></a></li>

                                <li class="kt-menu__item {{ $route == 'publisher.index' || $route == 'publisher.create' || $route == 'publisher.edit' ? 'kt-menu__item--active' : '' }}"
                                    aria-haspopup="true"><a href="{{ route('publisher.index') }}"
                                        class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Penerbit</span></a></li>

                                <li class="kt-menu__item {{ $route == 'book_category.index' || $route == 'book_category.create' || $route == 'book_category.edit' ? 'kt-menu__item--active' : '' }}"
                                    aria-haspopup="true"><a href="{{ route('book_category.index') }}"
                                        class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Kategori Buku</span></a></li>
                                <li class="kt-menu__item {{ $route == 'book.index' || $route == 'book.create' || $route == 'book.edit' ? 'kt-menu__item--active' : '' }}" aria-haspopup="true"><a href="{{route('book.index')}}"
                                        class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Buku</span></a></li>
                                <li class="kt-menu__item {{ $route == 'book_borrow.index' || $route == 'book_borrow.create' || $route == 'book_borrow.edit' ? 'kt-menu__item--active' : '' }}" aria-haspopup="true">
                                <a href="{{route('book_borrow.index')}}" aria-haspopup="true"><a href="{{route('book_borrow.index')}}" class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Transaksi</span></a></li>
                                <li class="kt-menu__item " aria-haspopup="true"><a href="" class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Laporan</span></a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="kt-menu__item {{ $route == 'attention.index' || $route == 'attention.create' || $route == 'attention.edit' ? 'kt-menu__item--active' : '' }}" aria-haspopup="true"><a href="{{ route('attention.index') }}"
                            class="kt-menu__link "><span class="kt-menu__link-text">Pengumuman</span></a></li>
                    <li class="kt-menu__item {{ $route == 'schedule.index' || $route == 'schedule.create' || $route == 'schedule.edit' ? 'kt-menu__item--active' : '' }}" aria-haspopup="true"><a href="{{ route('schedule.index') }}"
                            class="kt-menu__link "><span class="kt-menu__link-text">Penjadwalan</span></a></li>
                    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
                        data-ktmenu-submenu-toggle="hover"><a href="javascript:;"
                            class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text">Keuangan</span><i
                                class="kt-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                            <ul class="kt-menu__subnav">
                                <li class="kt-menu__item " aria-haspopup="true"><a href="{{ route('bill.index') }}"
                                        class="kt-menu__link ">
                                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                        <span class="kt-menu__link-text">Bayaran</span></a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
                        data-ktmenu-submenu-toggle="hover"><a href="javascript:;"
                            class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text">CMS Sekolah</span><i
                                class="kt-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                            <ul class="kt-menu__subnav">
                                <li class="kt-menu__item " aria-haspopup="true"><a href="{{ route('banner.index') }}"
                                        class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Banner</span></a></li>
                                <li class="kt-menu__item " aria-haspopup="true"><a
                                        href="{{ route('categorytype.index') }}" class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Kategori Konten</span></a></li>
                                <li class="kt-menu__item " aria-haspopup="true"><a href="{{ route('article.index') }}"
                                        class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Artikel</span></a></li>
                                <li class="kt-menu__item " aria-haspopup="true"><a href="{{ route('video.index') }}"
                                        class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Video</span></a></li>
                                <li class="kt-menu__item " aria-haspopup="true"><a
                                        href="{{ route('prestation.index') }}" class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Prestasi</span></a></li>
                                <li class="kt-menu__item " aria-haspopup="true"><a href="{{ route('eskul.index') }}"
                                        class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Ekstrakulikuler</span></a></li>
                            </ul>
                        </div>
                    </li>
                    @endif
                    <!-- =======================  TEACHER  ======================== -->
                    @if(Auth::guard('web_teacher')->check())
                    <li class="kt-menu__item {{ $route == 'teacher' ? 'kt-menu__item--active' : '' }}"
                        aria-haspopup="true"><a href="{{ route('admin') }}" class="kt-menu__link "><span
                                class="kt-menu__link-text">Dashboard</span></a></li>

                    <li class="kt-menu__section ">
                        <h4 class="kt-menu__section-text">Konten</h4>
                        <i class="kt-menu__section-icon flaticon-more-v2"></i>
                    </li>

                    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
                        data-ktmenu-submenu-toggle="hover"><a href="javascript:;"
                            class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text">Media
                                Pembelajaran</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                            <ul class="kt-menu__subnav">
                                <li class="kt-menu__item " aria-haspopup="true"><a href="{{ route('setting.index') }}"
                                        class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Video</span></a></li>
                                <li class="kt-menu__item " aria-haspopup="true"><a href="{{ route('document.index') }}"
                                        class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Dokumen</span></a></li>
                                <li class="kt-menu__item " aria-haspopup="true"><a href="" class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Artikel</span></a></li>
                            </ul>
                        </div>
                    <li class="kt-menu__item" aria-haspopup="true"><a href="{{ route('task.index') }}"
                            class="kt-menu__link "><span class="kt-menu__link-text">Penugasan</span></a></li>
                    <li class="kt-menu__item" aria-haspopup="true"><a href="{{ route('score.index') }}"
                            class="kt-menu__link "><span class="kt-menu__link-text">Penilaian</span></a></li>
                    <li class="kt-menu__item" aria-haspopup="true"><a href="" class="kt-menu__link "><span
                                class="kt-menu__link-text">Ulangan</span></a></li>
                    <li class="kt-menu__item" aria-haspopup="true"><a href="" class="kt-menu__link "><span
                                class="kt-menu__link-text">Siswa</span></a></li>
                    <li class="kt-menu__item" aria-haspopup="true"><a href="{{route('attitudeTransaction.index')}}"
                            class="kt-menu__link "><span class="kt-menu__link-text">Rekap Sikap Siswa</span></a></li>
                    <li class="kt-menu__item" aria-haspopup="true"><a href="" class="kt-menu__link "><span
                                class="kt-menu__link-text">Pengumuman</span></a></li>
                    <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
                        data-ktmenu-submenu-toggle="hover"><a href="javascript:;"
                            class="kt-menu__link kt-menu__toggle"><span class="kt-menu__link-text">Manajemen
                                Ulangan</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                            <ul class="kt-menu__subnav">
                                <li class="kt-menu__item " aria-haspopup="true"><a
                                        href="{{ route('subjectbab.index') }}" class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Bab Mata Pelajaran</span></a></li>
                                <li class="kt-menu__item " aria-haspopup="true"><a href="{{ route('qbank.index') }}"
                                        class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Bank Soal</span></a></li>
                                <li class="kt-menu__item " aria-haspopup="true"><a
                                        href="{{ route('exam_manage.index') }}" class="kt-menu__link "><i
                                            class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span
                                            class="kt-menu__link-text">Setting Ulangan</span></a></li>
                            </ul>
                        </div>
                    </li>
                    @endif

                    <!-- =======================  STUDENT  ======================== -->
                    @if(Auth::guard('web_student')->check())
                    <li class="kt-menu__item {{ $route == 'teacher' ? 'kt-menu__item--active' : '' }}"
                        aria-haspopup="true"><a href="{{ route('admin') }}" class="kt-menu__link "><span
                                class="kt-menu__link-text">Dashboard</span></a></li>

                    <li class="kt-menu__section ">
                        <h4 class="kt-menu__section-text">Absensi</h4>
                        <i class="kt-menu__section-icon flaticon-more-v2"></i>
                    </li>

                    <li class="kt-menu__item" aria-haspopup="true"><a href="{{ route('student.absent') }}"
                            class="kt-menu__link "><span class="kt-menu__link-text">Absenku</span></a></li>

                    <li class="kt-menu__section ">
                        <h4 class="kt-menu__section-text">Forum</h4>
                        <i class="kt-menu__section-icon flaticon-more-v2"></i>
                    </li>

                    <li class="kt-menu__item" aria-haspopup="true"><a href="{{ url('/forums') }}"
                        class="kt-menu__link "><span class="kt-menu__link-text">Diskusi</span></a></li>

                    <li class="kt-menu__section ">
                        <h4 class="kt-menu__section-text">Media Pembelajaran</h4>
                        <i class="kt-menu__section-icon flaticon-more-v2"></i>
                    </li>

                    <li class="kt-menu__item" aria-haspopup="true"><a href="" class="kt-menu__link "><span
                                class="kt-menu__link-text">Materi</span></a></li>

                    <li class="kt-menu__item" aria-haspopup="true"><a href="{{ route('student.task') }}"
                            class="kt-menu__link "><span class="kt-menu__link-text">Tugas</span></a></li>

                    <li class="kt-menu__item" aria-haspopup="true"><a href="" class="kt-menu__link "><span
                                class="kt-menu__link-text">Ulangan</span></a></li>

                    <li class="kt-menu__item" aria-haspopup="true"><a href="" class="kt-menu__link "><span
                                class="kt-menu__link-text">Pengumuman</span></a></li>

                    <li class="kt-menu__item" aria-haspopup="true"><a href="" class="kt-menu__link "><span
                                class="kt-menu__link-text">Keuangan</span></a></li>

                    <li class="kt-menu__item" aria-haspopup="true"><a href="" class="kt-menu__link "><span
                                class="kt-menu__link-text">Perpustakaan</span></a></li>
                    @endif
                </ul>
            </div>
            <!-- end:: Aside Menu -->
        </div>
    </div>
    <!-- end::Aside Secondary -->
</div>