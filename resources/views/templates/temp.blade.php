@include('templates.header')
	<!-- begin::Page loader -->

	<!-- end::Page Loader -->
	<!-- begin:: Page -->
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
		<div class="kt-header-mobile__logo">
			<a href="index.html">
				<img alt="Logo" src="{{ asset('assets/media/logos/logo-6.png') }}" />
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toggler kt-header-mobile__toggler--left"
				id="kt_aside_mobile_toggler"><span></span></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->
	<!-- begin:: Root -->
	<div class="kt-grid kt-grid--hor kt-grid--root">
		<!-- begin:: Page -->
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<!-- begin:: Aside -->
			<button class="kt-aside-close kt-hidden " id="kt_aside_close_btn"><i class="la la-close"></i></button>

			
			@include('templates.sidebar')
			<!-- end:: Aside -->
			<!-- begin:: Wrapper -->
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

					<!-- begin:: Subheader -->
					<div class="kt-subheader   kt-grid__item" id="kt_subheader">
						<div class="kt-subheader__main">
							<h3 class="kt-subheader__title">@yield('title')</h3>
							<span class="kt-subheader__separator kt-hidden"></span>
							<div class="kt-subheader__breadcrumbs">
								@yield('breadcrumb')
							</div>

						</div>
						<div class="kt-subheader__toolbar">

						</div>
					</div>
					<!-- end:: Subheader -->
					<!-- begin:: Content -->
					<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
						@yield('content')
					</div>
					<!-- end:: Content -->
				</div>

				<!-- begin:: Footer -->
				<div class="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop">
					<div class="kt-footer__copyright">
						2018&nbsp;&copy;&nbsp;<a href="../../index.html" target="_blank" class="kt-link">Kodein
							Inc
						</a>
					</div>
					<div class="kt-footer__menu">
						<a href="../../index.html" target="_blank" class="kt-footer__menu-link kt-link">About</a>
						<a href="../../index.html" target="_blank" class="kt-footer__menu-link kt-link">Team</a>
						<a href="../../index.html" target="_blank" class="kt-footer__menu-link kt-link">Contact</a>
					</div>
				</div>
				<!-- end:: Footer -->
			</div>
			<!-- end:: Wrapper -->

		</div>
		<!-- end:: Page -->
	</div>
	<!-- end:: Root -->

	<!-- end:: Page -->

	<!-- utilities -->
	@include('templates.utilities')
	
@include('templates.footer')
	