<!-- begin::Offcanvas Toolbar Search -->
	<div id="kt_offcanvas_toolbar_search" class="kt-offcanvas-panel">
		<div class="kt-offcanvas-panel__head" kt-hidden-height="88" style="">
			<h3 class="kt-offcanvas-panel__title">
				Search
			</h3>
			<a href="builder.html#" class="kt-offcanvas-panel__close" id="kt_offcanvas_toolbar_search_close"><i class="flaticon2-delete"></i></a>
		</div>
		<div class="kt-offcanvas-panel__body">
			<!-- begin:: Quick Search -->
			<div class="kt-quick-search kt-quick-search--offcanvas kt-quick-search--has-result" id="kt_quick_search_offcanvas">
				<form method="get" action="" class="kt-quick-search__form" kt-hidden-height="39" style="">
					<div class="input-group">
						<div class="input-group-prepend"><span class="input-group-text"><i class="flaticon2-search-1"></i></span></div>
						<input type="text" class="form-control kt-quick-search__input" placeholder="Type here..." name="query">
						<div class="input-group-append"><span class="input-group-text"><i class="la la-close kt-quick-search__close" style="display: none;"></i></span></div>
					</div>
				</form>
				<div class="kt-quick-search__wrapper kt-scroll ps ps--active-y" style="height: 584px; overflow: hidden;"><div class="kt-quick-search__result">
		<div class="kt-quick-search__message kt-hidden">
			No record found
		</div>

	    <div class="kt-quick-search__category">
	        Documents
	    </div>
	    <div class="kt-quick-search__section">
	        <div class="kt-quick-search__item">
	            <div class="kt-quick-search__item-img kt-quick-search__item-img--file">
	                <img src="{{ asset('assets/media/files/doc.svg') }}" alt="">
	            </div>
	            <div class="kt-quick-search__item-wrapper">
	                <a href="#" class="kt-quick-search__item-title">
						AirPlus Requirements
					</a>
	                <div class="kt-quick-search__item-desc">
	                    by Grog John
	                </div>
	            </div>
	        </div>

	        <div class="kt-quick-search__item">
	            <div class="kt-quick-search__item-img kt-quick-search__item-img--file">
	                <img src="{{ asset('assets/media/files/pdf.svg') }}" alt="">
	            </div>
	            <div class="kt-quick-search__item-wrapper">
	                <a href="#" class="kt-quick-search__item-title">
					    TechNav Documentation
					</a>
	                <div class="kt-quick-search__item-desc">
	                    by Mary Broun
	                </div>
	            </div>
	        </div>

	        <div class="kt-quick-search__item">
	            <div class="kt-quick-search__item-img kt-quick-search__item-img--file">
	                <img src="{{ asset('assets/media/files/zip.svg') }}" alt="">
	            </div>
	            <div class="kt-quick-search__item-wrapper">
	                <a href="#" class="kt-quick-search__item-title">
						All Framework Docs
					</a>
	                <div class="kt-quick-search__item-desc">
	                    by Nick Stone
	                </div>
	            </div>
	        </div>

	        <div class="kt-quick-search__item">
	            <div class="kt-quick-search__item-img kt-quick-search__item-img--file">
	                <img src="{{ asset('assets/media/files/xml.svg') }}" alt="">
	            </div>
	            <div class="kt-quick-search__item-wrapper">
	                <a href="#" class="kt-quick-search__item-title">
						AirPlus Requirements
					</a>
	                <div class="kt-quick-search__item-desc">
	                    by Tim Hardy
	                </div>
	            </div>
	        </div>
	    </div>

	    <div class="kt-quick-search__category">
	        Members
	    </div>
	    <div class="kt-quick-search__section">
	        <div class="kt-quick-search__item">
	            <div class="kt-quick-search__item-img">
	                <img src="{{ asset('assets/media/users/300_11.jpg') }}" alt="">
	            </div>
	            <div class="kt-quick-search__item-wrapper">
	                <a href="#" class="kt-quick-search__item-title">
										Jimmy Curry
									</a>
	                <div class="kt-quick-search__item-desc">
	                    Software Developer
	                </div>
	            </div>
	        </div>

	        <div class="kt-quick-search__item">
	            <div class="kt-quick-search__item-img">
	                <img src="{{ asset('assets/media/users/300_16.jpg') }}" alt="">
	            </div>
	            <div class="kt-quick-search__item-wrapper">
	                <a href="#" class="kt-quick-search__item-title">
										Milena Gibson
									</a>
	                <div class="kt-quick-search__item-desc">
	                    UI Designer
	                </div>
	            </div>
	        </div>

	        <div class="kt-quick-search__item">
	            <div class="kt-quick-search__item-img">
	                <img src="{{ asset('assets/media/users/300_21.jpg') }}" alt="">
	            </div>
	            <div class="kt-quick-search__item-wrapper">
	                <a href="#" class="kt-quick-search__item-title">
										Stefan JohnStefan  
									</a>
	                <div class="kt-quick-search__item-desc">
	                    Marketing Manager
	                </div>
	            </div>
	        </div>

	        <div class="kt-quick-search__item">
	            <div class="kt-quick-search__item-img">
	                <img src="{{ asset('assets/media/users/300_5.jpg') }}" alt="">
	            </div>
	            <div class="kt-quick-search__item-wrapper">
	                <a href="#" class="kt-quick-search__item-title">
										Anna Strong
									</a>
	                <div class="kt-quick-search__item-desc">
	                    Software Developer
	                </div>
	            </div>
	        </div>
	    </div>

	    <div class="kt-quick-search__category">
	        Files
	    </div>
	    <div class="kt-quick-search__section">
	        <div class="kt-quick-search__item">
	            <div class="kt-quick-search__item-icon">
	                <i class="flaticon2-box kt-font-danger"></i>
	            </div>
	            <div class="kt-quick-search__item-wrapper">
	                <a href="#" class="kt-quick-search__item-title">
										2 New items submitted
									</a>
	                <div class="kt-quick-search__item-desc">
	                    Marketing Manager
	                </div>
	            </div>
	        </div>

	        <div class="kt-quick-search__item">
	            <div class="kt-quick-search__item-icon">
	                <i class="flaticon-psd kt-font-brand"></i>
	            </div>
	            <div class="kt-quick-search__item-wrapper">
	                <a href="#" class="kt-quick-search__item-title">
										79 PSD files generated
									</a>
	                <div class="kt-quick-search__item-desc">
	                    by Grog John
	                </div>
	            </div>
	        </div>

	        <div class="kt-quick-search__item">
	            <div class="kt-quick-search__item-icon">
	                <i class="flaticon2-supermarket kt-font-warning"></i>
	            </div>
	            <div class="kt-quick-search__item-wrapper">
	                <a href="#" class="kt-quick-search__item-title">
										$2900 worth products sold
									</a>
	                <div class="kt-quick-search__item-desc">
	                    Total 234 items
	                </div>
	            </div>
	        </div>

	        <div class="kt-quick-search__item">
	            <div class="kt-quick-search__item-icon">
	                <i class="flaticon-safe-shield-protection kt-font-info"></i>
	            </div>
	            <div class="kt-quick-search__item-wrapper">
	                <a href="#" class="kt-quick-search__item-title">
										4 New items submitted
									</a>
	                <div class="kt-quick-search__item-desc">
	                    Marketing Manager
	                </div>
	            </div>
	        </div>
	    </div>
	</div><div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; height: 584px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 300px;"></div></div></div>
			</div>
			<!-- end:: Quick Search -->
		</div>
	</div>
	<!-- end::Offcanvas Toolbar Search -->


	<!-- begin::Offcanvas Toolbar Notifications -->
	<div id="kt_offcanvas_toolbar_notifications" class="kt-offcanvas-panel">
		<div class="kt-offcanvas-panel__head">
			<h3 class="kt-offcanvas-panel__title">
				Notifications
				<small>24 New</small>
			</h3>
			<a href="index.html#" class="kt-offcanvas-panel__close" id="kt_offcanvas_toolbar_notifications_close"><i
					class="flaticon2-delete"></i></a>
		</div>
		<div class="kt-offcanvas-panel__body">
			<div class="kt-notification-v2">
				<a href="index.html#" class="kt-notification-v2__item">
					<div class="kt-notification-v2__item-icon">
						<i class="flaticon-bell kt-font-focus"></i>
					</div>
					<div class="kt-notification-v2__item-wrapper">
						<div class="kt-notification-v2__item-title">
							5 new user generated report
						</div>
						<div class="kt-notification-v2__item-desc">
							Reports based on sales
						</div>
					</div>
				</a>
			</div>
		</div>
	</div>
	<!-- end::Offcanvas Toolbar Notifications -->
	<!-- begin::Offcanvas Toolbar Profile -->
	<div id="kt_offcanvas_toolbar_profile" class="kt-offcanvas-panel">
		<div class="kt-offcanvas-panel__head">
			<h3 class="kt-offcanvas-panel__title">
				Profile
			</h3>
			<a href="index.html#" class="kt-offcanvas-panel__close" id="kt_offcanvas_toolbar_profile_close"><i
					class="flaticon2-delete"></i></a>
		</div>
		<div class="kt-offcanvas-panel__body">
			<div class="kt-user-card-v3 kt-margin-b-30">
				<div class="kt-user-card-v3__avatar">
					<img src="{{ asset('app/teacher/default.png') }}" alt="" />
				</div>
				@if(Auth::guard('web_teacher')->check())
				<div class="kt-user-card-v3__detalis">
	                <a href="#" class="kt-user-card-v3__name">
						{{ Auth::guard('web_teacher')->user()->name }}
					</a>
	                <div class="kt-user-card-v3__desc">
	                    Teacher Of Kodein
	                </div>
	                <div class="kt-user-card-v3__info">
	                    <a href="#" class="kt-user-card-v3__item">
	                        <i class="flaticon-email-black-circular-button kt-font-brand"></i>
	                        <span class="kt-user-card-v3__tag">{{ Auth::guard('web_teacher')->user()->email }}</span>
	                    </a>
	                    <a href="#" class="kt-user-card-v3__item">
	                        <i class="flaticon-twitter-logo-button kt-font-success"></i>
	                        <span class="kt-user-card-v3__tag">@kodein</span>
	                    </a>
	                </div>
				</div>
				@endif
				@if(Auth::guard('web')->check())
				<div class="kt-user-card-v3__detalis">
	                <a href="#" class="kt-user-card-v3__name">
						@if(Auth::guard('web')->check())
							{{ Auth::user()->name }}
						@elseif(Auth::guard('web_teacher')->check())
							{{ Auth::guard('web_teacher')->user()->name }}
						@endif
					</a>
	                <div class="kt-user-card-v3__desc">
	                    Kodein Developer
	                </div>
	                <div class="kt-user-card-v3__info">
	                    <a href="#" class="kt-user-card-v3__item">
							<i class="flaticon-email-black-circular-button kt-font-brand"></i>
							@if(Auth::guard('web')->check())
								<span class="kt-user-card-v3__tag">{{ Auth::user()->email }}</span>
							@elseif(Auth::guard('web_teacher')->check())
								<span class="kt-user-card-v3__tag">{{ Auth::guard('web_teacher')->user()->email }}</span>
							@endif
	                    </a>
	                    <a href="#" class="kt-user-card-v3__item">
	                        <i class="flaticon-twitter-logo-button kt-font-success"></i>
	                        <span class="kt-user-card-v3__tag">@kodein</span>
	                    </a>
	                </div>
	            </div>
				@endif
				@if(Auth::guard('web_student')->check())
				<div class="kt-user-card-v3__detalis">
	                <a href="#" class="kt-user-card-v3__name">
						{{ Auth::guard('web_student')->user()->name }}
					</a>
	                <div class="kt-user-card-v3__desc">
	                    Student Of Kodein
	                </div>
	                <div class="kt-user-card-v3__info">
	                    <a href="#" class="kt-user-card-v3__item">
	                        <i class="flaticon-email-black-circular-button kt-font-brand"></i>
	                        <span class="kt-user-card-v3__tag">{{ Auth::guard('web_student')->user()->nis }}</span>
						</a>
						<a href="#" class="kt-user-card-v3__item">
	                        <i class="flaticon-email-black-circular-button kt-font-brand"></i>
	                        <span class="kt-user-card-v3__tag">{{ Auth::guard('web_student')->user()->classgroup->name }}</span>
						</a>
						<a href="#" class="kt-user-card-v3__item">
	                        <i class="flaticon-email-black-circular-button kt-font-brand"></i>
	                        <span class="kt-user-card-v3__tag">{{ Auth::guard('web_student')->user()->phone }}</span>
	                    </a>
	                    <a href="#" class="kt-user-card-v3__item">
	                        <i class="flaticon-twitter-logo-button kt-font-success"></i>
	                        <span class="kt-user-card-v3__tag">@kodein</span>
	                    </a>
	                </div>
				</div>
				@endif
			</div>

			<div class="kt-margin-t-40">
				<a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-danger btn-font-sm btn-upper btn-bold">Logout</a>

				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
			</div>

		</div>
	</div>
	<!-- end::Offcanvas Toolbar Profile -->
	<!-- begin:: Quick Panel -->
	<div id="kt_quick_panel" class="kt-offcanvas-panel">
		<div class="kt-offcanvas-panel__nav" kt-hidden-height="77" style="">
			<ul class="nav nav-pills" role="tablist">
		        <li class="nav-item active">
		            <a class="nav-link active" data-toggle="tab" href="builder.html#kt_quick_panel_tab_notifications" role="tab">Notifications</a>
		        </li>
		        <li class="nav-item">
		            <a class="nav-link" data-toggle="tab" href="builder.html#kt_quick_panel_tab_actions" role="tab">Actions</a>
		        </li>
		        <li class="nav-item">
		            <a class="nav-link" data-toggle="tab" href="builder.html#kt_quick_panel_tab_settings" role="tab">Settings</a>
		        </li>
		    </ul>

		    <button class="kt-offcanvas-panel__close" id="kt_quick_panel_close_btn"><i class="flaticon2-delete"></i></button>
		</div>

		<div class="kt-offcanvas-panel__body">
		    <div class="tab-content">
		        <div class="tab-pane fade show kt-offcanvas-panel__content kt-scroll active ps ps--active-y" id="kt_quick_panel_tab_notifications" role="tabpanel" style="height: 586px; overflow: hidden;">
	 				<!--Begin::Timeline -->
	                <div class="kt-timeline">
	                    <!--Begin::Item -->                      
	                    <div class="kt-timeline__item kt-timeline__item--success">
	                        <div class="kt-timeline__item-section">
	                            <div class="kt-timeline__item-section-border">
	                                <div class="kt-timeline__item-section-icon">
	                                    <i class="flaticon-feed kt-font-success"></i>
	                                </div>
	                            </div>
	                            <span class="kt-timeline__item-datetime">02:30 PM</span>
	                        </div>

	                        <a href="" class="kt-timeline__item-text">
	                            KeenThemes created new layout whith tens of new options for Keen Admin panel                                                                                             
	                        </a>
	                        <div class="kt-timeline__item-info">
	                            HTML,CSS,VueJS                                                                                            
	                        </div>
	                    </div>
	                    <!--End::Item -->  

	                    <!--Begin::Item --> 
	                    <div class="kt-timeline__item kt-timeline__item--danger">
	                        <div class="kt-timeline__item-section">
	                            <div class="kt-timeline__item-section-border">
	                                <div class="kt-timeline__item-section-icon">
	                                    <i class="flaticon-safe-shield-protection kt-font-danger"></i>
	                                </div>
	                            </div>
	                            <span class="kt-timeline__item-datetime">01:20 AM</span>
	                        </div>

	                        <a href="" class="kt-timeline__item-text">
	                            New secyrity alert by Firewall &amp; order to take aktion on User Preferences                                                                                             
	                        </a>
	                        <div class="kt-timeline__item-info">
	                            Security, Fieewall                                                                                         
	                        </div>
	                    </div>  
	                    <!--End::Item --> 

	                    <!--Begin::Item --> 
	                    <div class="kt-timeline__item kt-timeline__item--brand">
	                        <div class="kt-timeline__item-section">
	                            <div class="kt-timeline__item-section-border">
	                                <div class="kt-timeline__item-section-icon">
	                                    <i class="flaticon2-box kt-font-brand"></i>
	                                </div>
	                            </div>
	                            <span class="kt-timeline__item-datetime">Yestardey</span>
	                        </div>

	                        <a href="" class="kt-timeline__item-text">
	                            FlyMore design mock-ups been uploadet by designers Bob, Naomi, Richard                                                                                            
	                        </a>
	                        <div class="kt-timeline__item-info">
	                            PSD, Sketch, AJ                                                                                       
	                        </div>
	                    </div>  
	                    <!--End::Item --> 

	                    <!--Begin::Item --> 
	                    <div class="kt-timeline__item kt-timeline__item--warning">
	                        <div class="kt-timeline__item-section">
	                            <div class="kt-timeline__item-section-border">
	                                <div class="kt-timeline__item-section-icon">
	                                    <i class="flaticon-pie-chart-1 kt-font-warning"></i>
	                                </div>
	                            </div>
	                            <span class="kt-timeline__item-datetime">Aug 13,2018</span>
	                        </div>

	                        <a href="" class="kt-timeline__item-text">
	                            Meeting with Ken Digital Corp ot Unit14, 3 Edigor Buildings, George Street, Loondon<br> England, BA12FJ                                                                                           
	                        </a>
	                        <div class="kt-timeline__item-info">
	                            Meeting, Customer                                                                                          
	                        </div>
	                    </div> 
	                    <!--End::Item --> 

	                    <!--Begin::Item --> 
	                    <div class="kt-timeline__item kt-timeline__item--info">
	                        <div class="kt-timeline__item-section">
	                            <div class="kt-timeline__item-section-border">
	                                <div class="kt-timeline__item-section-icon">
	                                    <i class="flaticon-notepad kt-font-info"></i>
	                                </div>
	                            </div>
	                            <span class="kt-timeline__item-datetime">May 09, 2018</span>
	                        </div>

	                        <a href="" class="kt-timeline__item-text">
	                            KeenThemes created new layout whith tens of new options for Keen Admin panel                                                                                                
	                        </a>
	                        <div class="kt-timeline__item-info">
	                            HTML,CSS,VueJS                                                                                            
	                        </div>
	                    </div> 
	                    <!--End::Item --> 

	                    <!--Begin::Item --> 
	                    <div class="kt-timeline__item kt-timeline__item--accent">
	                        <div class="kt-timeline__item-section">
	                            <div class="kt-timeline__item-section-border">
	                                <div class="kt-timeline__item-section-icon">
	                                    <i class="flaticon-bell kt-font-success"></i>
	                                </div>
	                            </div>
	                            <span class="kt-timeline__item-datetime">01:20 AM</span>
	                        </div>

	                        <a href="" class="kt-timeline__item-text">
	                            New secyrity alert by Firewall &amp; order to take aktion on User Preferences                                                                                             
	                        </a>
	                        <div class="kt-timeline__item-info">
	                            Security, Fieewall                                                                                         
	                        </div>
	                    </div>   
	                    <!--End::Item -->                    

	                </div> 
	                <!--End::Timeline --> 
		        <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; height: 586px; right: 5px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 300px;"></div></div></div>
		        <div class="tab-pane fade kt-offcanvas-panel__content kt-scroll ps" id="kt_quick_panel_tab_actions" role="tabpanel" style="height: 586px; overflow: hidden;">
		        	<!--begin::Portlet-->
					<div class="kt-portlet kt-portlet--solid-success">
						<div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<span class="kt-portlet__head-icon kt-hide"><i class="flaticon-stopwatch"></i></span>
								<h3 class="kt-portlet__head-title">Recent Bills</h3>
							</div>
							<div class="kt-portlet__head-toolbar">
								<div class="kt-portlet__head-group">
									<div class="dropdown dropdown-inline">
										<button type="button" class="btn btn-sm btn-font-light btn-outline-hover-light btn-circle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
											<i class="flaticon-more"></i>
										</button>
										<div class="dropdown-menu dropdown-menu-right">
											<a class="dropdown-item" href="builder.html#">Action</a>
											<a class="dropdown-item" href="builder.html#">Another action</a>
											<a class="dropdown-item" href="builder.html#">Something else here</a>
											<div class="dropdown-divider"></div>
											<a class="dropdown-item" href="builder.html#">Separated link</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="kt-portlet__body">
							<div class="kt-portlet__content">
								Lorem Ipsum is simply dummy text of the printing and typesetting simply dummy text of the printing industry. 
							</div>
						</div>	
						<div class="kt-portlet__foot kt-portlet__foot--sm kt-align-right">
							<a href="builder.html#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">Dismiss</a>
							<a href="builder.html#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">View</a>
						</div>
					</div>
					<!--end::Portlet-->

					<!--begin::Portlet-->
					<div class="kt-portlet kt-portlet--solid-focus">
						<div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<span class="kt-portlet__head-icon kt-hide"><i class="flaticon-stopwatch"></i></span>
								<h3 class="kt-portlet__head-title">Latest Orders</h3>
							</div>
							<div class="kt-portlet__head-toolbar">
								<div class="kt-portlet__head-group">
									<div class="dropdown dropdown-inline">
										<button type="button" class="btn btn-sm btn-font-light btn-outline-hover-light btn-circle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
											<i class="flaticon-more"></i>
										</button>
										<div class="dropdown-menu dropdown-menu-right">
											<a class="dropdown-item" href="builder.html#">Action</a>
											<a class="dropdown-item" href="builder.html#">Another action</a>
											<a class="dropdown-item" href="builder.html#">Something else here</a>
											<div class="dropdown-divider"></div>
											<a class="dropdown-item" href="builder.html#">Separated link</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="kt-portlet__body">
							<div class="kt-portlet__content">
								Lorem Ipsum is simply dummy text of the printing and typesetting simply dummy text of the printing industry. 
							</div>
						</div>	
						<div class="kt-portlet__foot kt-portlet__foot--sm kt-align-right">
							<a href="builder.html#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">Dismiss</a>
							<a href="builder.html#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">View</a>
						</div>
					</div>
					<!--end::Portlet-->

					<!--begin::Portlet-->
					<div class="kt-portlet kt-portlet--solid-info">
						<div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<h3 class="kt-portlet__head-title">Latest Invoices</h3>
							</div>
							<div class="kt-portlet__head-toolbar">
								<div class="kt-portlet__head-group">
									<div class="dropdown dropdown-inline">
										<button type="button" class="btn btn-sm btn-font-light btn-outline-hover-light btn-circle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
											<i class="flaticon-more"></i>
										</button>
										<div class="dropdown-menu dropdown-menu-right">
											<a class="dropdown-item" href="builder.html#">Action</a>
											<a class="dropdown-item" href="builder.html#">Another action</a>
											<a class="dropdown-item" href="builder.html#">Something else here</a>
											<div class="dropdown-divider"></div>
											<a class="dropdown-item" href="builder.html#">Separated link</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="kt-portlet__body">
							<div class="kt-portlet__content">
								Lorem Ipsum is simply dummy text of the printing and typesetting simply dummy text of the printing industry. 
							</div>
						</div>	
						<div class="kt-portlet__foot kt-portlet__foot--sm kt-align-right">
							<a href="builder.html#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">Dismiss</a>
							<a href="builder.html#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">View</a>
						</div>
					</div>
					<!--end::Portlet-->

					<!--begin::Portlet-->
					<div class="kt-portlet kt-portlet--solid-warning">
						<div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<h3 class="kt-portlet__head-title">New Comments</h3>
							</div>
							<div class="kt-portlet__head-toolbar">
								<div class="kt-portlet__head-group">
									<div class="dropdown dropdown-inline">
										<button type="button" class="btn btn-sm btn-font-light btn-outline-hover-light btn-circle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
											<i class="flaticon-more"></i>
										</button>
										<div class="dropdown-menu dropdown-menu-right">
											<a class="dropdown-item" href="builder.html#">Action</a>
											<a class="dropdown-item" href="builder.html#">Another action</a>
											<a class="dropdown-item" href="builder.html#">Something else here</a>
											<div class="dropdown-divider"></div>
											<a class="dropdown-item" href="builder.html#">Separated link</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="kt-portlet__body">
							<div class="kt-portlet__content">
								Lorem Ipsum is simply dummy text of the printing and typesetting simply dummy text of the printing industry. 
							</div>
						</div>	
						<div class="kt-portlet__foot kt-portlet__foot--sm kt-align-right">
							<a href="builder.html#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">Dismiss</a>
							<a href="builder.html#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">View</a>
						</div>
					</div>
					<!--end::Portlet-->

					<!--begin::Portlet-->
					<div class="kt-portlet kt-portlet--solid-brand">
						<div class="kt-portlet__head">
							<div class="kt-portlet__head-label">
								<h3 class="kt-portlet__head-title">Recent Posts</h3>
							</div>
							<div class="kt-portlet__head-toolbar">
								<div class="kt-portlet__head-group">
									<div class="dropdown dropdown-inline">
										<button type="button" class="btn btn-sm btn-font-light btn-outline-hover-light btn-circle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
											<i class="flaticon-more"></i>
										</button>
										<div class="dropdown-menu dropdown-menu-right">
											<a class="dropdown-item" href="builder.html#">Action</a>
											<a class="dropdown-item" href="builder.html#">Another action</a>
											<a class="dropdown-item" href="builder.html#">Something else here</a>
											<div class="dropdown-divider"></div>
											<a class="dropdown-item" href="builder.html#">Separated link</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="kt-portlet__body">
							<div class="kt-portlet__content">
								Lorem Ipsum is simply dummy text of the printing and typesetting simply dummy text of the printing industry. 
							</div>
						</div>	
						<div class="kt-portlet__foot kt-portlet__foot--sm kt-align-right">
							<a href="builder.html#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">Dismiss</a>
							<a href="builder.html#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">View</a>
						</div>
					</div>
					<!--end::Portlet-->
		        <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 5px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
		        <div class="tab-pane fade kt-offcanvas-panel__content kt-scroll ps" id="kt_quick_panel_tab_settings" role="tabpanel" style="height: 586px; overflow: hidden;">
		        	<form class="kt-form">
						<div class="kt-heading kt-heading--space-sm">Notifications</div>

						<div class="form-group form-group-xs row">
							<label class="col-8 col-form-label">Enable notifications:</label>
							<div class="col-4 kt-align-right">
								<span class="kt-switch kt-switch--sm">
									<label>
										<input type="checkbox" checked="checked" name="quick_panel_notifications_1">
										<span></span>
									</label>
								</span>
							</div>
						</div>
						<div class="form-group form-group-xs row">
							<label class="col-8 col-form-label">Enable audit log:</label>
							<div class="col-4 kt-align-right">
								<span class="kt-switch kt-switch--sm">
									<label>
										<input type="checkbox" name="quick_panel_notifications_2">
										<span></span>
									</label>
								</span>
							</div>
						</div>
						<div class="form-group form-group-last form-group-xs row">
							<label class="col-8 col-form-label">Notify on new orders:</label>
							<div class="col-4 kt-align-right">
								<span class="kt-switch kt-switch--sm">
									<label>
										<input type="checkbox" checked="checked" name="quick_panel_notifications_2">
										<span></span>
									</label>
								</span>
							</div>
						</div>
						
						<div class="kt-separator kt-separator--space-md kt-separator--border-dashed"></div>

						<div class="kt-heading kt-heading--space-sm">Orders</div>

						<div class="form-group form-group-xs row">
							<label class="col-8 col-form-label">Enable order tracking:</label>
							<div class="col-4 kt-align-right">
								<span class="kt-switch kt-switch--sm kt-switch--danger">
									<label>
										<input type="checkbox" checked="checked" name="quick_panel_notifications_3">
										<span></span>
									</label>
								</span>
							</div>
						</div>
						<div class="form-group form-group-xs row">
							<label class="col-8 col-form-label">Enable orders reports:</label>
							<div class="col-4 kt-align-right">
								<span class="kt-switch kt-switch--sm kt-switch--danger">
									<label>
										<input type="checkbox" name="quick_panel_notifications_3">
										<span></span>
									</label>
								</span>
							</div>
						</div>
						<div class="form-group form-group-last form-group-xs row">
							<label class="col-8 col-form-label">Allow order status auto update:</label>
							<div class="col-4 kt-align-right">
								<span class="kt-switch kt-switch--sm kt-switch--danger">
									<label>
										<input type="checkbox" checked="checked" name="quick_panel_notifications_4">
										<span></span>
									</label>
								</span>
							</div>
						</div>

						<div class="kt-separator kt-separator--space-md kt-separator--border-dashed"></div>

						<div class="kt-heading kt-heading--space-sm">Customers</div>

						<div class="form-group form-group-xs row">
							<label class="col-8 col-form-label">Enable customer singup:</label>
							<div class="col-4 kt-align-right">
								<span class="kt-switch kt-switch--sm kt-switch--success">
									<label>
										<input type="checkbox" checked="checked" name="quick_panel_notifications_5">
										<span></span>
									</label>
								</span>
							</div>
						</div>
						<div class="form-group form-group-xs row">
							<label class="col-8 col-form-label">Enable customers reporting:</label>
							<div class="col-4 kt-align-right">
								<span class="kt-switch kt-switch--sm kt-switch--success">
									<label>
										<input type="checkbox" name="quick_panel_notifications_5">
										<span></span>
									</label>
								</span>
							</div>
						</div>
						<div class="form-group form-group-last form-group-xs row">
							<label class="col-8 col-form-label">Notifiy on new customer registration:</label>
							<div class="col-4 kt-align-right">
								<span class="kt-switch kt-switch--sm kt-switch--success">
									<label>
										<input type="checkbox" checked="checked" name="quick_panel_notifications_6">
										<span></span>
									</label>
								</span>
							</div>
						</div>

					</form>
		        <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 5px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
		    </div>
		</div>
	</div>
	<!-- end:: Quick Panel -->


	<!-- begin:: Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="la la-arrow-up"></i>
	</div>