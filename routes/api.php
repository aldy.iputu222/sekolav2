<?php

use Illuminate\Http\Request;


// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::apiResource('book','API\BookController');
Route::get('book_status','API\BookController@book_status');
Route::get('major_and_class/{id_year}/{id_class}','API\AbsentController@major_class');
Route::post('school_year/get_class/','API\ClassGroupController@by_schoolyear');
Route::post('book_temporary/store','API\TemporaryBorrowController@store');
Route::get('book_temporary/{id}','API\BookController@data');
Route::get('book_temporary/load/{id}','API\BookController@load_current_temporary');
Route::post('subject_bab/get_bab','API\QuestionSettingController@by_subjectid');

Route::post('student/login','API\StudentController@login');
Route::get('student/by_nis/{nis}','API\StudentController@by_nis');
Route::post('student/do_borrow/','API\StudentController@do_borrow');
Route::get('attention/{for}/{id}','API\AttentionController@attention_data');
Route::post('attention/parent','API\AttentionController@attention_parent');
Route::post('student/check','API\StudentController@check');
Route::get('student/friends/{id}','API\StudentController@friends');

//GuestBook
Route::get('guest/today','API\GuestBookController@today');
Route::get('guest/all','API\GuestBookController@index');
Route::post('guest/store','API\GuestBookController@store');

//Article
Route::get('article','API\ArticleController@index');

//Video
Route::get('video','API\VideoController@index');

// Uks
Route::post('uks/store','API\UksController@store');

// Absent
Route::post('absent/store','API\AbsentController@store');
Route::get('absent/student/{student_id}','API\AbsentController@student_absent');
Route::post('absent/month','API\AbsentController@absent_month');
Route::get('absent/test','API\AbsentController@test');

//Attitude
Route::get('attitude/student/{student_id}','API\AttitudeController@student');

// Teacher
Route::get('teacher','API\TeacherController@index');
Route::post('teacher/login','API\TeacherController@login');

// Notification
Route::apiResource('notification','API\NotificationController');

// Parent
Route::post('parent/register','StudentParentController@register');
Route::post('parent/relate','API\ParentRelateController@relate');

//Schedule
Route::get('schedule/{student_id}','API\ScheduleController@detail');

//Bill
Route::get('bill/student/{id}','API\BillController@student_bill');

Route::post('/charge/','API\BillController@charge');
Route::post('/charge/handler','API\BIllController@charge_notification');

// Exam
Route::post('exam/start','ExamStartController@store');

// Login Employee
Route::post('employee/login','EmployeeController@login');


// Schedule
Route::get('ajax/rombel/{id}','ScheduleController@ajax_jurusan');
Route::get('ajax/jadwal_detail/{id}','ScheduleController@ajax_jadwal_detail');

// Score
Route::get('ajax_score/rombel/{id}', 'ScoreController@ajax_rombel');
Route::get('ajax_score/detail_siswa/{id}', 'ScoreController@ajax_siswa');

Route::get('student/borrow/{student_id}','API\BorrowController@get_books');

