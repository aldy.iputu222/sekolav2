<?php

Route::view('example','example.index');
Route::view('example/create','example.create');

// ROUTE WEB-PROFILE
Route::view('/','welcome');
Route::resource('attention','AttentionController');
Route::prefix('admin')->middleware('auth')->group(function(){
    Route::view('/','pages.other.dashboard')->name('admin');
    Route::resource('setting','SettingController');
    Route::resource('major','MajorController');
    Route::resource('school_year', 'SchoolYearController');
    Route::resource('subject','SubjectController');
    Route::resource('bill', 'BillController');
    Route::resource('bill_filter', 'BillFilterController');
    Route::resource('classgroup','ClassGroupController');
    Route::resource('video','VideoController');
    Route::resource('teacher', 'TeacherController');
    Route::resource('eskul', 'EskulController');
    Route::resource('student','StudentController');
    Route::resource('banner','BannerController');
    Route::resource('categorytype','CategoryTypeController');
    Route::resource('publisher','PublisherController');
    Route::resource('author','AuthorController');
    Route::resource('student','StudentController');
    Route::resource('book','BookController');
    Route::resource('book_category','BookCategoryController');

    Route::resource('employee','EmployeeController');
    Route::resource('attitude','AttitudeController');

    Route::resource('absent','AbsentController');
    Route::post('/input_absen','AbsentController@input_absen')->name('input_absen');
    Route::post('/data_absen','AbsentController@data_absen')->name('data_absen');

    Route::resource('schedule','ScheduleController');

    Route::resource('article','ArticleController');
    Route::resource('prestation','PrestationController');
    Route::resource('borrow_type','BorrowTypeController');
    Route::resource('book_borrow','BookBorrowController');
    Route::resource('uks','UksController');
    Route::resource('task', 'TaskController');
    Route::resource('examtype', 'ExamTypeController');
    Route::group(['prefix' => 'user'], function() {
        Route::view('/', 'pages.user.index');
    });
    Route::group(['prefix'=>'book_borrow'],function() {
        Route::get('next/{nis}','BookBorrowController@next')->name('book_borrow.next');
        Route::get('delete/{id}','BookBorrowController@destroy')->name('book_borrow.deletes');
    });
    Route::resource('attitude','AttitudeController');
});


Route::prefix('student')->middleware('student')->group(function(){
    Route::get('logout','StudentAuth\LoginController@logout')->name('student.logout');
    Route::middleware('student')->group(function(){
        Route::get('/','StudentController@dashboard')->name('student.dashboard');
        Route::resource('notes','NotesController');
        Route::get('attention','AttentionController@student_attention')->name('student.attention');
        Route::get('task','TaskController@student_task')->name('student.task');
        Route::get('absent','AbsentController@student_index')->name('student.absent');
        Route::post('absent/filter','AbsentController@student_filter')->name(
            'student.absent.filter');
    });
});

Route::prefix('teacher')->middleware('teacher')->group(function(){
    Route::view('/','pages.teacher.dashboard');
    // SUBJECT BAB
    Route::resource('subjectbab','SubjectBabController');
    Route::get('storebab','SubjectBabController@storeBab')->name('teacher.storebab');
    Route::post('insertbab','SubjectBabController@insertbab')->name('teacher.insertbab');

    // QUESTION BANK
    Route::resource('qbank','QuestionBankController');

    Route::resource('document','DocumentController');

    Route::resource('attitudeTransaction','attitudeTransactionController');

    Route::get('setting','QuestionBankController@addSetting')->name('teacher.setting');
    Route::post('addquestion','QuestionBankController@addQuestion')->name('teacher.addquestion');

    // EXAM MANAGE
    Route::resource('exam_manage','ExamManageController');
    Route::get('choose_question/{id}/{subject_id}','ExamManageController@choose_question')->name('teacher.choose_question');

    // Task
    Route::resource('task', 'TaskController');

    // Score
    Route::resource('score', 'ScoreController');
});



Auth::routes();
//Credentials
Route::get('student/login','StudentAuth\LoginController@showLoginForm')->name('student.login.form');
Route::get('teacher/login','TeacherAuth\LoginController@showLoginForm')->name('teacher.login.form');
Route::post('student/login','StudentAuth\LoginController@login')->name('student.login.store');
Route::post('teacher/login','TeacherAuth\LoginController@login')->name('teacher.login.store');
Route::post('teacher/logout','TeacherAuth\LoginController@logout')->name('teacher.logout');
Route::post('student/logout','StudentAuth\LoginController@logout')->name('student.logout');
Route::get('/home', 'HomeController@index')->name('home');
